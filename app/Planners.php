<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planners extends Model
{
    protected $table = 'planners';

    protected $fillable = ['name', 'address', 'city', 'state', 'zip', 'phone', 'website', 'user_id'];

    protected $casts = [
        'favorites' => 'json',
    ];
}
