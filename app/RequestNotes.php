<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestNotes extends Model
{
    protected $table = 'requestnotes';
}
