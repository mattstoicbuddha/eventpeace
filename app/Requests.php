<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requests extends Model
{

    protected $table = 'requests';

    protected $fillable = ['name', 'price', 'description', 'is_custom', 'service_id', 'event_id', 'quantity', 'price_diff', 'price_diff_description'];
}
