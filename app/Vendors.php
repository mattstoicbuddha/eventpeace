<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use AlgoliaSearch\Laravel\AlgoliaEloquentTrait;

class Vendors extends Model
{
	use AlgoliaEloquentTrait;
    protected $table = 'vendors';

    protected $fillable = ['name', 'address', 'city', 'state', 'zip', 'description', 'phone', 'website', 'email', 'business_contact', 'business_contact_title', 'price_range_low', 'price_range_high', 'profile_pic', 'stripe_customer_id', 'user_id', '_geoloc'];

    protected $casts = [
        '_geoloc' => 'array',
    ];

    public $algoliaSettings = [
        'attributesToIndex' => [
            'id', 'name', 'address', 'city', 'state', 'zip', 'description', 'phone', 'website', 'email', 'business_contact', 'business_contact_title', 'price_range_low', 'price_range_high', 'profile_pic', '_geoloc',
        ],
    ];

}
