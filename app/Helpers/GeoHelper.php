<?php 

class GeoHelper{
	
	public function address_info($address){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address)."&key=AIzaSyASfFNUH52i82R6uReEsn4qF6RbRXx-oVQ",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_CUSTOMREQUEST => "GET",
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return false;
		} else {
		  return $response;
		}

	}
}