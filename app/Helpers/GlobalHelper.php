<?php

function request_status_encoder($user, $status){
	return md5($user->id . Session::getId() . $status);
}

function request_text($r = 0){
	/*

	The statuses for requests:

	1 - Just opened, sent to vendor.
	2 - Vendor declines.
	3 - Vendor requires changes before accepting.
	4 - Planner has accepted that an edit needs to happen and is making one.
	5 - Vendor edit denied, request cancelled.
	6 - Vendor requires a custom extra.
	7 - Planner has submitted a custom extra.
	8 - Planner has submitted an edit.
	9 - Vendor has accepted (at any point in the process).
	10 - Planner has paid.
	11 - Planner has cancelled pre-payment.
	12 - Planner has cancelled post-payment. (Trigger Refund)
	13 - Vendor has cancelled pre-payment.
	14 - Vendor has cancelled post-payment. (Trigger Refund)
	15 - A site admin has cancelled the transaction pre-payment.
	16 - A site admin has cancelled the transaction post-payment. (Trigger Refund)
	17 - A site admin has paid out a planner the amount owed for services.

	*/

	switch($r)
		{
		    case 1:
		    return 'Pending Vendor Review';
			break;

		    case 2:
		    return 'Vendor Declined';
			break;

			case 3:
		    return 'Edit Required';
			break;

		    case 4:
		    return 'Pending Edit';
			break;

		    case 5:
		    return 'Planner Cancelled';
			break;

			case 6:
		    return 'Pending Custom Extra';
			break;

		    case 7:
		    return 'Pending Vendor Review';
			break;

		    case 8:
		    return 'Pending Vendor Review';
			break;

			case 9:
		    return 'Accepted/Pending Payment';
			break;

		    case 10:
		    return 'Paid';
			break;

		    case 11:
		    return 'Planner Cancelled';
			break;

			case 12:
		    return 'Planner Cancelled';
			break;

		    case 13:
		    return 'Vendor Cancelled';
			break;

		    case 14:
		    return 'Vendor Cancelled';
			break;

			case 15:
		    return 'Site Admin Cancelled';
			break;

		    case 16:
		    return 'Site Admin Cancelled';
			break;

			case 17:
		    return 'Paid Out';
			break;

		    default:
			return 'New/Untouched';
			break;
		}
}

function request_text_class($r = 0){
	/*

	The statuses for requests:

	1 - Just opened, sent to vendor.
	2 - Vendor declines.
	3 - Vendor requires changes before accepting.
	4 - Vendor edit accepted.
	5 - Vendor edit denied, request cancelled.
	6 - Vendor requires a custom extra.
	7 - Planner has submitted a custom extra.
	8 - Planner has submitted an edit.
	9 - Vendor has accepted (at any point in the process).
	10 - Planner has paid.
	11 - Planner has cancelled pre-payment.
	12 - Planner has cancelled post-payment. (Trigger Refund)
	13 - Vendor has cancelled pre-payment.
	14 - Vendor has cancelled post-payment. (Trigger Refund)
	15 - A site admin has cancelled the transaction pre-payment.
	16 - A site admin has cancelled the transaction post-payment. (Trigger Refund)

	*/

	switch($r)
		{
		    case 1:
		    return 'text-warning';
			break;

		    case 2:
		    return 'text-danger';
			break;

			case 3:
		    return 'text-warning';
			break;

		    case 4:
		    return 'text-warning';
			break;

		    case 5:
		    return 'text-danger';
			break;

			case 6:
		    return 'text-warning';
			break;

		    case 7:
		    return 'text-warning';
			break;

		    case 8:
		    return 'text-warning';
			break;

			case 9:
		    return 'text-success';
			break;

		    case 10:
		    return 'text-success';
			break;

		    case 11:
		    return 'text-danger';
			break;

			case 12:
		    return 'text-danger';
			break;

		    case 13:
		    return 'text-danger';
			break;

		    case 14:
		    return 'text-danger';
			break;

			case 15:
		    return 'text-danger';
			break;

		    case 16:
		    return 'text-danger';
			break;

			case 17:
		    return 'text-success';
			break;

		    default:
			return 'text-info';
			break;
		}
}

function request_icon($r = 0){
	/*

	The statuses for requests:

	1 - Just opened, sent to vendor.
	2 - Vendor declines.
	3 - Vendor requires changes before accepting.
	4 - Vendor edit accepted.
	5 - Vendor edit denied, request cancelled.
	6 - Vendor requires a custom extra.
	7 - Planner has submitted a custom extra.
	8 - Planner has submitted an edit.
	9 - Vendor has accepted (at any point in the process).
	10 - Planner has paid.
	11 - Planner has cancelled pre-payment.
	12 - Planner has cancelled post-payment. (Trigger Refund)
	13 - Vendor has cancelled pre-payment.
	14 - Vendor has cancelled post-payment. (Trigger Refund)
	15 - A site admin has cancelled the transaction pre-payment.
	16 - A site admin has cancelled the transaction post-payment. (Trigger Refund)

	*/

	switch($r)
		{
		    case 1:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/yellow-light-40.png" height="40px" width="40px">';
			break;

		    case 2:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/red-light-40.png" height="40px" width="40px">';
			break;

			case 3:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/yellow-light-40.png" height="40px" width="40px">';
			break;

		    case 4:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/yellow-light-40.png" height="40px" width="40px">';
			break;

		    case 5:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/red-light-40.png" height="40px" width="40px">';
			break;

			case 6:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/yellow-light-40.png" height="40px" width="40px">';
			break;

		    case 7:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/yellow-light-40.png" height="40px" width="40px">';
			break;

		    case 8:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/yellow-light-40.png" height="40px" width="40px">';
			break;

			case 9:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/yellow-green-light-40.png" height="40px" width="40px">';
			break;

		    case 10:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/green-light-40.png" height="40px" width="40px">';
			break;

		    case 11:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/red-light-40.png" height="40px" width="40px">';
			break;

			case 12:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/red-light-40.png" height="40px" width="40px">';
			break;

		    case 13:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/red-light-40.png" height="40px" width="40px">';
			break;

		    case 14:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/red-light-40.png" height="40px" width="40px">';
			break;

			case 15:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/red-light-40.png" height="40px" width="40px">';
			break;

		    case 16:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/red-light-40.png" height="40px" width="40px">';
			break;

			case 17:
		    return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/green-light-40.png" height="40px" width="40px">';
			break;

		    default:
			return '<img src="'.env('CDN_URL', 'http://cdn.todcandev.com/eventpeace').'/grey-light-40.png" height="40px" width="40px">';
			break;
		}
}