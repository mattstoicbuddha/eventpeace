<?php

namespace App\Helpers;

use App\Vendors;
use App\Planners;
use App\Services;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use App\Mail\ActivateAccount;

//Account Helper functionality

class AccountHelper
{

	public function vendor_grant_access($user, $vid){

		// NEEDS TO BE BROKEN OUT SOMEHOW!
        $result = (object) array('success'=>false, 'redirect'=>false);

        if(!empty($user)){

            if(!empty($vid)){

                if($user->id !== Vendors::find($vid)->user_id && $user->is_admin !== 1){

                    return $result;

                }

                $result->redirect = false;
                $result->success = true;
                return $result;

            } else {

            	return $result;

            }
        } else {

            $result->redirect = true;

        }

        return $result;

	}

    public function planner_grant_access($user, $pid){

        // NEEDS TO BE BROKEN OUT SOMEHOW!
        $result = (object) array('success'=>false, 'redirect'=>false);

        if(!empty($user)){

            if(!empty($pid)){

                if($user->id !== Planners::find($pid)->user_id && $user->is_admin !== 1){

                    return $result;

                }

                $result->redirect = false;
                $result->success = true;
                return $result;

            } else {

                return $result;

            }
        } else {

            $result->redirect = true;

        }

        return $result;

    }

    public function services_grant_access($user, $sid){

        // NEEDS TO BE BROKEN OUT SOMEHOW!
        $result = (object) array('success'=>false, 'redirect'=>false);

        if(!empty($user)){

            if(!empty($sid)){

                if($user->id !== Services::find($sid)->vendor_id && $user->is_admin !== 1){

                    return $result;

                }

                $result->redirect = false;
                $result->success = true;
                return $result;

            } else {

                return $result;

            }
        } else {

            $result->redirect = true;

        }

        return $result;

    }

    public function registerUser($email, $password, $type, $showPass = false){

        $the_password = $showPass ? $password : '';
        
        $user = Sentinel::register(array(
            'email'    => $email,
            'password' => $password,
            'type'     => (int) $type
        ), false);
        $result = (object) array('success'=>true);
        if(!$user){
            $result->success = false;
        }
        $user->type = (int) $type;
        $user->save();
        if($result->success){
            if((int) $type === 1){

                $vendor = new Vendors;
                $vendor->user_id = $user->id;
                if(!$vendor->save()){
                    $result->success = false;
                }

            } else {

                $planner = new Planners;
                $planner->user_id = $user->id;
                if(!$planner->save()){
                    $result->success = false;
                }

            }

            \Mail::to($user->email)->send(new ActivateAccount($user, $the_password));
            return $result;
        }
    }

}
