<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\RequestNotes;
use App\Requests as EvpReq;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Support\Facades\Input;
use App\Services;
use App\Extras;
use App\Events;
use App\Vendors;
use App\Planners;
use App\Mail\NewRequestNote;
use App\Mail\NewRequest;
use Cartalyst\Stripe\Stripe;

class RequestsController extends Controller
{

	/*

	The statuses for requests:

	1 - Just opened, sent to vendor.
	2 - Vendor declines.
	3 - Vendor requires changes before accepting.
	4 - Vendor edit accepted.
	5 - Vendor edit denied, request cancelled.
	6 - Vendor requires a custom extra.
	7 - Planner has submitted a custom extra.
	8 - Planner has submitted an edit.
	9 - Vendor has accepted (at any point in the process).
	10 - Planner has paid.
	11 - Planner has cancelled pre-payment.
	12 - Planner has cancelled post-payment. (Trigger Refund)
	13 - Vendor has cancelled pre-payment.
	14 - Vendor has cancelled post-payment. (Trigger Refund)
	15 - A site admin has cancelled the transaction pre-payment.
	16 - A site admin has cancelled the transaction post-payment. (Trigger Refund)

	*/

    public function store(Request $request)
    {
        if(Sentinel::guest()) return json_encode( (object) array( 'success'=>false ) );
        $input = Input::all();
        $formdata = array();
        parse_str($input['formdata'], $formdata);
        $user = Sentinel::getUser();
        $event = Events::find($formdata['event_id']);
        if((int) $user->id !== (int) $event->user_id && (int) $user->is_admin === 0) return json_encode( (object) array( 'success'=>false ) );

        $service = Services::find($formdata['service_id']);

        $quant = !empty($formdata['requestQuantity']) ? $formdata['requestQuantity'] : 1;

        $price = (int) ( $service->price * $quant ) + (int) $service->delivery_charge;

        $extras =[];
        $extras_index = 0;
        if(!empty($formdata['extras'])){
            foreach($formdata['extras'] as $ex){
                if((int) $ex !== 0){
                    $extras[] = (int) $ex;
                    continue;
                }

                $new_extra = new Extras;
                $new_extra->name = $formdata['requestCustomExtrasName'][$extras_index];
                $new_extra->price = (int) $formdata['requestCustomExtrasPrice'][$extras_index] * 100;
                $new_extra->description = $formdata['requestCustomExtrasDescription'][$extras_index];
                $new_extra->is_custom = 1;
                $new_extra->event_id = $formdata['event_id'];
                $new_extra->service_id = $formdata['service_id'];
                if($new_extra->save()){
                    $extras[] = (int) $new_extra->id;
                    $price = (int) $price + (int) $new_extra->price;
                }
                $extras_index++;

            }
        }



        $request = new EvpReq;
        $request->event_id = $formdata['event_id'];
        $request->service_id = $formdata['service_id'];
        $request->extras = json_encode($extras);
        $request->status = !empty($service->auto_accept) ? 9 : 1;
        $request->paid = 0;
        $request->total = $price;
        $request->time_start = strtotime(urldecode( $formdata['startDate'] ). " " .urldecode( $formdata['startTime'] ) );
        $request->time_stop = strtotime(urldecode( $formdata['endDate'] ). " " .urldecode( $formdata['endTime'] ) );
        $request->request_history = !empty($service->auto_accept) ? json_encode( array( (object) array('status'=>9, 'timestamp'=>time() ) ) ) : json_encode( array( (object) array('status'=>1, 'timestamp'=>time() ) ) );
        $request->quantity = !empty($formdata['requestQuantity']) && (int) $formdata['requestQuantity'] > 0 ? $formdata['requestQuantity'] : 0;
        $request->address = !empty($formdata['address-for-service']) ? $formdata['address-for-service'] : '';
        $request->city = !empty($formdata['city-for-service']) ? $formdata['city-for-service'] : '';
        $request->state = !empty($formdata['state-for-service']) ? $formdata['state-for-service'] : '';
        $request->zip = !empty($formdata['zip-for-service']) ? $formdata['zip-for-service'] : '';

        if($request->save()){

            $request_vendor = Sentinel::findUserById($service->vendor_id);

            $vendor = Vendors::where('user_id', $service->vendor_id)->first();

            $planner = Planners::where('user_id', $user->id)->first();

            if($request_vendor) \Mail::to($request_vendor->email)->send(new NewRequest($request, $vendor, $planner));

            if(!empty($formdata['note'])){
                foreach($formdata['note'] as $note){
                    $newnote = new RequestNotes;
                    $newnote->event_id = $formdata['event_id'];
                    $newnote->request_id = $request->id;
                    $newnote->author = $user->id;
                    $newnote->note = $note;
                    if($newnote->save()){

                        $request->name = $service->name;
                        $request->note = $note;
                        if( (int) $user->type === 0 ){
                            $other = Vendors::where('user_id', $service->vendor_id)->first();
                            $other_user = Sentinel::findUserById($service->vendor_id);
                            $recipient = Planners::where('user_id', $user->id)->first();
                            $request->recipient_name = !empty($other->name) ? $other->name : $other_user->email;
                            $request->other_name = !empty($recipient->name) ? $recipient->name : "Planner";
                            if(empty($other_user)) return json_encode( (object) array( 'success'=>true ) );
                            $email = $request->recipient_email = $other_user->email;
                        } else {
                            $other = Planners::where('user_id', $event->user_id)->first();
                            $other_user = Sentinel::findUserById($event->user_id);
                            $recipient = Vendors::where('user_id', $service->vendor_id)->first();
                            $request->recipient_name = !empty($other->name) ? $other->name : $other_user->email;
                            $request->other_name = !empty($recipient->name) ? $recipient->name : "Vendor";
                            if(empty($other_user)) return json_encode( (object) array( 'success'=>true ) );
                            $email = $request->recipient_email = $other_user->email;
                        }

                        \Mail::to($request->recipient_email)->send(new NewRequestNote($request));
                    }
                }
            }
            return json_encode( (object) array( 'success'=>true, 'id'=>$request->id ) );
        }

        return json_encode( (object) array( 'success'=>false ) );
    }

    public function update(Request $request, $id)
    {

    	if(Sentinel::guest()) return json_encode( (object) array( 'success'=>false ) );

    	$user = Sentinel::getUser();
    	$request = EvpReq::find($id);
    	$event = Events::find($request->event_id);
    	$service = Services::find($request->service_id);
        $formdata = Input::all();

        if( array_search( $request->status, array( 2, 5, 11, 12, 13, 14, 15, 16 ) ) !== false ) return json_encode( (object) array( 'success'=>false, 'status'=>true ) );

        if(!empty($formdata['formdata'])){
            parse_str($formdata['formdata'], $input);
        } else {
            $input = $formdata;
        }

        if((int) $event->user_id !== (int) $user->id && (int) $service->vendor_id !== (int) $user->id && (int) $user->is_admin === 0){
            return json_encode( (object) array( 'success'=>false, 'user'=>true) );
        }

        $status = 0;

        $history = json_decode($request->request_history);

        /* Vendor Acceptance Codes */
        if((int) $user->type === 1 && !empty($input['status'])){
        	if($input['status'] === request_status_encoder($user, 2) ) {
        		// Vendor declined completely
        		$status = 2;
        	} else if($input['status'] === request_status_encoder($user, 3) ) {
        		// Vendor requires an edit
        		$status = 3;
        	} else if($input['status'] === request_status_encoder($user, 4) ) {
        		// Vendor accepted edit
        		$status = 4;
        	} else if($input['status'] === request_status_encoder($user, 6) ) {
        		// Vendor requires custom extra
        		$status = 6;
        	} else if($input['status'] === request_status_encoder($user, 9) ) {
        		// Vendor accepted
        		$status = 9;
        	} else if($input['status'] === request_status_encoder($user, 13) ) {
        		// Vendor cancelled

        		if((int) $request->paid === 0 ) $status = 13;

        		if((int) $request->paid === 1 ) $status = 14; // TODO: Figure out refund rules

        	}

        }

        /* Planner Acceptance Codes */
        if((int) $user->type === 0 && !empty($input['status'])){
        	if($input['status'] === request_status_encoder($user, 5) ) {
        		// Planner declines edit
        		$status = 5;
        	} else if($input['status'] === request_status_encoder($user, 7) ) {
        		// Planner sent custom extra
        		$status = 7;
        	} else if($input['status'] === request_status_encoder($user, 8) ) {
        		// Planner submitted an edit
        		$history[] = (object) array('status'=>8, 'timestamp'=>time() );
                $status = 1;
        	} else if($input['status'] === request_status_encoder($user, 11) ) {
        		// Planner cancelled

        		if((int) $request->paid === 0 ) $status = 11;

        		if((int) $request->paid === 1 ) $status = 12; // TODO: Figure out refund rules

        	}

        }

        /* Admin Codes */
        if((int) $user->is_admin === 1 && !empty($input['status'])){
        	if($input['status'] === request_status_encoder($user, 15) ) {
        		// Planner cancelled

        		if((int) $request->paid === 0 ) $status = 15;

        		if((int) $request->paid === 1 ) $status = 16; // TODO: Figure out refund rules

        	}

        }

        if($status === 0){
        	return json_encode( (object) array( 'success'=>false, 'status'=>$status ) ); // Do some form of redirect if we have no info on anything for a new status
        }

        $quant = !empty($input['requestQuantity']) ? $input['requestQuantity'] : 1;

        $price = (int) ( (int) $service->price * (int) $quant ) + (int) $service->delivery_charge;

        $extras_index = 0;

        $extras = [];

        if(!empty($input['extras'])){
            foreach($input['extras'] as $ex){
                if((int) $ex !== 0){
                    $extras[] = (int) $ex;
                    continue;
                }

                $new_extra = new Extras;
                $new_extra->name = $input['requestCustomExtrasName'][$extras_index];
                $new_extra->price = (int) $input['requestCustomExtrasPrice'][$extras_index] * 100;
                $new_extra->description = $input['requestCustomExtrasDescription'][$extras_index];
                $new_extra->is_custom = 1;
                $new_extra->event_id = $input['event_id'];
                $new_extra->service_id = $input['service_id'];
                if($new_extra->save()){
                    $extras[] = (int) $new_extra->id;
                    $price = (int) $price + (int) $new_extra->price;
                }
                $extras_index++;

            }

            if($extras_index > 0){
                $history[] = (object) array('status'=>7, 'timestamp'=>time() );
                $status = 1;
            }
        }

        $history[] = (object) array('status'=>$status, 'timestamp'=>time() );

        // $request->event_id = !empty($input['event_id']) ? $input['event_id'] : $request->event_id;
        // $request->service_id = $input['service_id'];
        $request->extras = (int) $service->vendor_id === (int) $user->id ? $request->extras : json_encode($extras);
        $request->status = !empty($status) ? $status : $request->status;
        $request->paid = !empty($request->paid) && (int) $request->paid === 1 ? 1 : 0;
        $request->total = $price;
        $request->time_start = !empty($input['startDate']) ? strtotime(urldecode( $input['startDate'] ). " " .urldecode( $input['startTime'] ) ) : $request->time_start;
        $request->time_stop = !empty($input['endDate']) ? strtotime(urldecode( $input['endDate'] ). " " .urldecode( $input['endTime'] ) ) : $request->time_stop;
        $request->request_history = json_encode($history);
        $request->quantity = !empty($input['requestQuantity']) && (int) $input['requestQuantity'] > 0 ? $input['requestQuantity'] : $request->quantity;
        $request->address = !empty($input['address-for-service']) ? $input['address-for-service'] : $request->address;
        $request->city = !empty($input['city-for-service']) ? $input['city-for-service'] : $request->city;
        $request->state = !empty($input['state-for-service']) ? $input['state-for-service'] : $request->state;
        $request->zip = !empty($input['zip-for-service']) ? $input['zip-for-service'] : $request->zip;
        if((int) $service->vendor_id === (int) $user->id && !empty($formdata['price_diff'])){
            $request->price_diff = !empty($formdata['price_diff']) ? $formdata['price_diff'] * 100 : 0;
            $request->price_diff_description = !empty($formdata['price_diff_description']) ? $formdata['price_diff_description'] : '';
        }

        if($request->save()){
            if(!empty($formdata['formdata'])){
                return json_encode( (object) array( 'success'=>true, 'status'=>$status, 'id'=>$request->id ) );
            } else {
                return redirect('/services/dashboard');
            }
        }

        if(!empty($formdata['formdata'])){
            return json_encode( (object) array( 'success'=>false, 'status'=>$status ) );
        } else {
            return redirect('/services/dashboard');
        }

    }

    public function payment_info($request_id){

        $user = Sentinel::getUser();
        $result = (object) array('success'=>false);
        if(empty($user)){
            return redirect()->action('UsersController@get_login');
        }

        $request = EvpReq::find($request_id);

        if(empty($request)){
            return "bad request";
        }

        $event = Events::find($request->event_id);

        if((int) $event->user_id !== (int) $user->id){
            return "wrong user";
        }

        if((int) $request->status !== 9){
            return "nope";
        }

        $service = Services::find($request->service_id);

        $total = 0;

        $extras = array();

        $quant = !empty($request->quantity) && $request->quantity > 0 ? $request->quantity : 1;

        $total = $total + $service->price * $quant;

        $total = $total + $service->delivery_charge;

        $extras_list = json_decode($request->extras);

        foreach($extras_list as $l){

            $extra = Extras::find($l);
            if(empty($extra)) continue;

            $extras[] = (object) array('name'=>$extra->name, 'price'=>$extra->price);

            $total = $total + ( ( !empty($request->quantity) ? $request->quantity : 1) * $extra->price );

        }

        $total = $total + (!empty($request->price_diff) ? (int)$request->price_diff : 0);

        if($total < 100) $total = 100;

        $stripe = new Stripe(env('STRIPE_SECRET', 'sk_test_95SgVueeUDeTZIPc5YnSaSMB'));
        $cards = new \stdClass;
        if(!empty($user->stripe_customer_id)){
            $cards = json_decode(json_encode(($stripe->cards()->all($user->stripe_customer_id))));
        }

        $result->success = true;
        $result->service_name = $service->name;
        $result->price = $service->price;
        $result->delivery_charge = $service->delivery_charge;
        $result->extras = $extras;
        $result->total = $total;
        $result->cards = $cards;
        $result->price_diff = !empty($request->price_diff) ? $request->price_diff : 0;
        $result->price_diff_description = !empty($request->price_diff_description) ? $request->price_diff_description : "";
        $result->quantity = !empty($request->quantity) && $request->quantity > 0 ? $request->quantity : 1;

        return json_encode($result);

    }

    public function create_charge($request_id){

        $user = Sentinel::getUser();
        $result = (object) array('success'=>false);
        if(empty($user)){
            return redirect()->action('UsersController@get_login');
        }

        $request = EvpReq::find($request_id);

        $event = Events::find($request->event_id);

        if((int) $event->user_id !== (int) $user->id){
             return json_encode($result);
        }

        if((int) $request->status !== 9){
             return json_encode($result);
        }

        $input = Input::all();

        if(!empty($input['card_id']) && !empty($user->stripe_customer_id)){

            $service = Services::find($request->service_id);

            $total = 0;

            $total = $total + $service->price;

            $total = $total + $service->delivery_charge;

            $extras_list = json_decode($request->extras);

            foreach($extras_list as $l){

                $extra = Extras::find($l);
                if(empty($extra)) continue;

                $total = $total + $extra->price;

            }

            $total = $total + (int) $request->price_diff;

            if($total < 100) $total = 100;

            $total = $total/100;

            $total = $total + ceil($total * 0.03);

            $stripe = new Stripe(env('STRIPE_SECRET', 'sk_test_95SgVueeUDeTZIPc5YnSaSMB'));

            try{

                $charge = $stripe->charges()->create([
                    'customer' => $user->stripe_customer_id,
                    'currency' => 'USD',
                    'amount'   => $total,
                    'card'     => $input['card_id'],
                ]);

                if(!empty($charge) && !empty($charge['id'])){
                    $request->status = 10;
                    $request->save();
                    $result->success = true;
                    $result->charge = $charge;
                }

                $request = EvpReq::find($request_id);
                $history = json_decode($request->request_history);
                $history[] = (object) array('status'=>10, 'timestamp'=>time() );
                $request->status = 10;
                $request->paid = 1;
                $request->request_history = json_encode($history);
                $request->save();

                // $purchaser = (object) array('user'=>$user, 'info'=>$request_info);
                // \Mail::to($user->email)->send(new PurchaseReceipt($purchaser));

                // $vendor = new Vendors::find($service->vendor_id);
                // $vendor_info = (object) array('user'=>$user, 'info'=>$request_info);
                // \Mail::to($vendor->email)->send(new VendorReceipt($vendor_info));

            } catch (\Guzzle\Service\Exception\ValidationException $e){
                $result->success = false;
                $result->error = $e->getMessage();
            } catch (\Cartalyst\Stripe\Exception\CardErrorException $e){
                $result->success = false;
                $result->error = $e->getMessage();
            } catch (\Cartalyst\Stripe\Exception\BadRequestException $e){
                $result->success = false;
                $result->error = $e->getMessage();
            } catch (\Cartalyst\Stripe\Exception\UnauthorizedException $e){
                $result->success = false;
                $result->error = $e->getMessage();
            } catch (\Cartalyst\Stripe\Exception\RequestFailedException $e){
                $result->success = false;
                $result->error = $e->getMessage();
            } catch (\Cartalyst\Stripe\Exception\NotFoundException $e){
                $result->success = false;
                $result->error = $e->getMessage();
            } catch (\Cartalyst\Stripe\Exception\ServerErrorException $e){
                $result->success = false;
                $result->error = $e->getMessage();
            }  catch (Exception $e) {
                $result->success = false;
                $result->error = $e->getMessage();
            }

            return json_encode($result);

        } else if(empty($input['card_id'])){
             return json_encode($result);
        } else if(empty($user->stripe_customer_id)){
             return json_encode($result);
        }
    }

    public function get_modify($id){

        if(Sentinel::guest()) return json_encode( (object) array( 'success'=>false ) );

        $user = Sentinel::getUser();
        $request = EvpReq::find($id);
        $event = Events::find($request->event_id);
        $service = Services::find($request->service_id);

        if((int) $event->user_id !== (int) $user->id && (int) $service->vendor_id !== (int) $user->id && (int) $user->is_admin === 0){
            return json_encode( (object) array( 'success'=>false, 'user'=>true) );
        }

        return json_encode( (object) array('success'=>true, 'price_diff'=>$request->price_diff/100, 'price_diff_description'=>$request->price_diff_description) );
    }

    public function post_modify($id){

        if(Sentinel::guest()) return json_encode( (object) array( 'success'=>false ) );

        $user = Sentinel::getUser();
        $request = EvpReq::find($id);
        $event = Events::find($request->event_id);
        $service = Services::find($request->service_id);
        $input = Input::all();

        if((int) $service->vendor_id !== (int) $user->id && (int) $user->is_admin === 0){
            return json_encode( (object) array( 'success'=>false, 'user'=>true) );
        }

        $request->price_diff = $input['price_diff']*100;
        $request->price_diff_description = $input['price_diff_description'];
        if($request->save()){
            return json_encode( (object) array('success'=>true, 'price_diff'=>$request->price_diff*100, 'price_diff_description'=>$request->price_diff_description) );
        }
        return json_encode( (object) array( 'success'=>false ) );

    }

}
