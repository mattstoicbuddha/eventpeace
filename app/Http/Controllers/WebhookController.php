<?php

//blah blah ddd blah

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;

class WebhookController extends Controller
{
    public function index()
    {

        $repo_dir     = '/var/www/eventpeace';
        $web_root_dir = '/var/www/eventpeace';

        // Full path to git binary is required if git is not in your PHP user's path. Otherwise just use 'git'.
        $git_bin_path = 'git';

        $update = true;

        // Parse data from Bitbucket hook payload
        $branch = "master";

        if ($update) {
            // Do a git checkout to the web root
            // Log::info("Checkout: " . shell_exec('cd ' . $repo_dir . ' && ' . $git_bin_path  . ' checkout master'));
            Log::info("Pull: " . shell_exec('cd ' . $repo_dir . ' && ' . $git_bin_path . ' pull'));

            // Log the deployment
            $commit_hash = shell_exec('cd ' . $repo_dir . ' && ' . $git_bin_path . ' rev-parse --short HEAD');
            Log::info(date('m/d/Y h:i:s a') . " Deployed branch: " . $branch . " Commit: " . $commit_hash . "\n");

            return date('m/d/Y h:i:s a') . " Deployed branch: " . $branch . " Commit: " . $commit_hash . "\n";
        }
    }
}
