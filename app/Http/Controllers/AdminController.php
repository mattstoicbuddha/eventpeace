<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Categories;
use App\Events as Events;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Support\Facades\Input;
use AccountHelper as AcctHelper;
use App\Vendors;
use App\Services;
use App\Requests as EvpReq;
use App\Extras;
use App\Planners;
use App\User as Users;
use App\Payments;
use Illuminate\Support\Facades\Response;

class AdminController extends Controller
{

	public function __construct(){
		$user = Sentinel::getUser();
    	if(empty($user) || (int) $user->is_admin !== 1){
    		abort(404); // If the user isn't an admin, we don't want them doing admin things.
    	}
	}

    public function dashboard(){
    	return view('admin.dashboard');
    }

    public function users(){
        $users = Users::select('id', 'email', 'type', 'suspended')->where('id', '>', 0)->limit(50)->get();
    	return view('admin.users', compact('users'));
    }

    public function get_more_users($page){
    	$start = $page * 50;
    	$users = Users::select('id', 'email', 'type', 'suspended')->where('id', '>', 0)->offset($page)->limit(50)->get();
    	return json_encode($users);
    }

    public function suspend_user($id){
    	$result = (object) array('success'=>false);
    	$user = Sentinel::findUserById($id);
    	$user->suspended = 1;
    	if($user->save()){
            Sentinel::logout($user, true);
    		$result->success = true;
    	}
    	return json_encode($result);
    }

    public function unsuspend_user($id){
        $result = (object) array('success'=>false);
        $user = Sentinel::findUserById($id);
        $user->suspended = 0;
        if($user->save()){
            Sentinel::logout($user, true);
            $result->success = true;
        }
        return json_encode($result);
    }

    public function payments(){
        $payments = $pendings = [];
        $payments_list = EvpReq::where('status', 17)->limit(50)->get();
        foreach($payments_list as $payment){
            $service = Services::find($payment->service_id);
            $payment->vendor_id = $service->vendor_id;
            $event = Events::find($payment->service_id);
            $payment->planner_id = $event->user_id;
            $payments[] = $payment;
        }
        $pending_list = EvpReq::where('status', 10)->limit(50)->get();
        foreach($pending_list as $pending){
            $service = Services::find($pending->service_id);
            $pending->vendor_id = $service->vendor_id;
            $event = Events::find($pending->service_id);
            $pending->planner_id = $event->user_id;
            $pendings[] = $pending;
        }
        $pending = $pendings;
    	return view('admin.payments', compact('payments', 'pending'));
    }

    public function get_more_payments($page){
    	$start = $page * 50;
        $payments = [];
    	$payments_list = EvpReq::where('status', 17)->offset($page)->limit(50)->get();
        foreach($payments_list as $payment){
            $service = Services::find($payment->service_id);
            $payment->vendor_id = $service->vendor_id;
            $event = Events::find($payment->service_id);
            $payment->planner_id = $event->user_id;
            $payments[] = $payment;
        }
    	return json_encode($payments);
    }

    public function get_more_pending($page){
        $start = $page * 50;
        $pendings = [];
        $pending_list = EvpReq::where('status', 10)->offset($page)->limit(50)->get();
        foreach($pending_list as $pending){
            $service = Services::find($pending->service_id);
            $pending->vendor_id = $service->vendor_id;
            $event = Events::find($pending->service_id);
            $pending->planner_id = $event->user_id;
            $pendings[] = $pending;
        }
        return json_encode($pendings);
    }

    public function pay_out($request_id){
    	$result = (object) array('success'=>false);
    	$request = EvpReq::find($request_id);
    	$history = json_decode($request->request_history);
    	$history[] = (object) array('status'=>17, 'timestamp'=>time() );
        $request->status = 17;
    	$request->request_history = json_encode($history);

    	if($request->save()){
    		$result->success = true;
    	}
    	return json_encode($result);
    }

    public function export_users_csv()
    {

        $user = Sentinel::getUser();

        $users_table = Users::all();
        $filename = storage_path() . "/app/public/" . $user->id.date('m-d-Y-h-i-s', time())."_registered_users.csv";
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('Email', 'Name', 'Phone Number', 'Address', 'City', 'State', 'Zip', 'Date Registered'));

        foreach($users_table as $row) {
            $this_user = (int)$row->type === 1 ? Vendors::where('user_id', $row->id)->first() : Planners::where('user_id', $row->id)->first();
            fputcsv($handle, array(
                !empty($row->email) ? $row->email : '',
                !empty($this_user->name) ? $this_user->name : '',
                !empty($this_user->phone) ? $this_user->phone : '',
                !empty($this_user->address) ? $this_user->address : '',
                !empty($this_user->city) ? $this_user->city : '',
                !empty($this_user->state) ? $this_user->state : '',
                !empty($this_user->zip) ? $this_user->zip : '',
                date('m/d/Y', strtotime($row->created_at))));
        }

        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Transfer-Encoding' => 'UTF-8'
        );

        return Response::download($filename, $user->id.date('m-d-Y-h-i-s', time())."_registered_users.csv", $headers);
    }

}
