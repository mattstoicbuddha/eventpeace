<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Categories;
use App\Events as Events;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Support\Facades\Input;
use AccountHelper as AcctHelper;
use App\Vendors;
use App\Services;
use App\Requests as EvpReq;
use App\Extras;
use App\Planners;
use App\SavedEvents;
use Illuminate\Support\Facades\Route;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $tag = '' )
    {
        $subcategories = array();

        $top_cats = array();

        $top = Categories::where("parent_category", 0)->get();

        foreach($top as $t){

            $top_cats[] = $t;

            $subcategories[$t->id] = Categories::where("parent_category", $t->id)->get();

        }

        $top_cats = json_decode(json_encode($top_cats));

        $events = [];

        $user = Sentinel::getUser();

        if(!empty($user->id)){
            $events = Events::where('user_id', $user->id)->get();
        }

        if(session('event')) session(['event'=>null]);

        return view('events.create', array("categories"=>$top_cats, "events"=>$events, "subcategories"=>$subcategories, "user"=>$user, 'tag'=>$tag));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return view('events.store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user = Sentinel::getUser();
        if(empty($user) || (int) $user->is_admin !== 1){
            abort(404); // If the user isn't an admin, they don't need to be here.
        }

        $event = Events::find($id);

        if(!$event) abort(404, "That event does not exist.");

        $services = Services::findMany(json_decode($event->services));

        $event->services = $services;

        return view('events.show', compact('event'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $input = (object) array();
        if(!empty($_POST)){
            $input = json_decode(json_encode(Input::all()));
            // $subcategories = Categories::where("parent_category", $input->EventServiceCat)->get();
        }

        $subcategories = array();

        $top_cats = array();

        $top = Categories::where("parent_category", 0)->get();

        foreach($top as $t){

            $top_cats[] = $t;

            $subcategories[$t->id] = Categories::where("parent_category", $t->id)->get();

        }

        $top_cats = json_decode(json_encode($top_cats));

        $user = Sentinel::getUser();

        if(!empty($user->id)){
            $events = Events::find($id);
        }

        return view('events.create', array("edit"=>$id, "categories"=>$top_cats, "events"=>$events, "input"=>$input, "subcategories"=>$subcategories));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return view('events.update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return view('events.destroy');
    }

    public function show_dashboard()
    {

        // Check if we're looking for expired or not.
        $expired = strpos(Route::getFacadeRoot()->current()->uri(), 'expired') !== false ? true : false;

        if(Sentinel::guest()) return redirect()->action('UsersController@get_login');
        $user = Sentinel::getUser();

        $planner = Planners::where('user_id', $user->id)->first();

        if($expired){
            $events = Events::where('user_id', $user->id)->where('end', '<', time())->orderBy('start', 'DESC')->get();
        } else {
            $events = Events::where('user_id', $user->id)->where('end', '>', time())->orderBy('start', 'ASC')->get();
        }

        $services_controller = new ServicesController; // Used for getting calendar info

        for($i=0;$i<count($events);$i++){

            $event_total = 0;

            $built_events = [];

            $services = json_decode($events[$i]->services);

            foreach($services as $s){
                $service_obj = Services::find($s);
                $extras = [];
                $req = EvpReq::where([ ['service_id', $s], ['event_id', $events[$i]->id] ])->first();
                $service_total = ( ( !empty($req->quantity) ? $req->quantity : 1 ) * (!empty($service_obj->price) ? $service_obj->price : 0) ) + ( !empty($service_obj->delivery_charge) ? $service_obj->delivery_charge : 0) + ( !empty($req->price_diff) ? $req->price_diff : 0 );
                if(!empty($req->id)){
                    if(!empty($req->extras)){
                        foreach(json_decode($req->extras) as $ex){
                            $extra_obj = Extras::find($ex);
                            $extras[] = $extra_obj;
                            $service_total += ( ( !empty($req->quantity) ? $req->quantity : 1 ) * $extra_obj->price );
                        }
                    }

                    $status = $req->status;

                    $text_status = !empty($req->status) ? request_text( $req->status ) : request_text();

                    $req->status = (object) array('id'=>$status, 'text'=>$text_status);
                }

                $calendar = !empty($service_obj->calendar_code) ? $services_controller->get_calendar_sources($s) : "";

                $event_total = $event_total + $service_total;

                $built_events[] = (object) array('service'=>$service_obj, 'request'=>$req, 'extras'=>$extras, 'service_total'=>$service_total, 'calendar' => $calendar);
            }

            $events[$i]->services = $built_events;
            $events[$i]->total = $event_total;

        }


        $all_requests = [];

        foreach($events as $e){
            $requests = EvpReq::where('event_id', $e->id)->get();
            foreach($requests as $r){
                $service = Services::find($r->service_id);
                if(empty($service->vendor_id)) continue;
                $vendor = Vendors::find($service->vendor_id);
                $all_requests[] = (object) array('request'=>$r, 'service'=>$service, 'vendor'=>$vendor);
            }
        }

        return view('events.dashboard', compact('user', 'events', 'all_requests', 'planner', 'expired'));
    }

    public function save_dashboard()
    {
        return view('events.dashboard');
    }

    public function get_services_from_event(Request $request, $id = ''){

        $event = $ev = array();
        $event_id = 0;

        if( is_numeric($id) ){
            $event = Events::find($id);
            $event_id = $id;
        } else {
            $event = session('event');
        }



        $services = [];
        foreach(json_decode($event->services) as $e){
            $ev[] = $e;
            $service = Services::find($e);
            $req = EvpReq::where( [ ['service_id', $e], ['event_id', session('event_id')] ] )->first();
            $status = -1;
            if(!empty($req)){
                $status = $req->status;
            }
            $services[] = array('id'=>(int) $e, 'name'=>$service->name, 'price'=>$service->price, 'status'=>$status);
        }

        session(['event'=>$ev, 'event_id'=>$event_id]);

        return json_encode($services);

    }

    public function add_service_to_event(Request $request, $id){
        if(empty($id)) return 'Error: No service ID received.';

        $id = (int) $id;
        $event = array();
        if(session('event')) $event = (array) session('event');

        if(!is_array($event)){
            $events_obj = json_decode($event);
            $event = [];
            foreach($events_obj as $key => $value) {
                $event[] = $value;
            }
        }

        foreach($event as $e){
            if($e === $id){
                return 'Error: This service is already part of the event.';
            }
        }

        $event[] = (int) $id;

        $services = [];
        foreach($event as $e){
            $service = Services::find($e);
            $services[] = array('id'=>(int) $e, 'name'=>$service->name, 'price'=>$service->price);
        }

        $event_in_session = $request->session()->pull('event', []);
        session(['event'=>$event]);

        return json_encode($services);

    }

    public function remove_service_from_event(Request $request, $id){
        if(empty($id)) return 'Error: No service ID received.';

        $id = (int) $id;
        $event = array();
        if(session('event')) $event = session('event');

        if(!is_array($event)) $event = (array) $event;

        foreach($event as $k => $e){
            if((int) $e === (int) $id){
                unset($event[$k]);
            }
        }

        $services = [];
        foreach($event as $e){
            $service = Services::find($e);
            $services[] = array('id'=>(int) $e, 'name'=>$service->name, 'price'=>$service->price);
        }

        if(session('event_id')){

            $req = EvpReq::where( [ ['service_id', $id], ['event_id', session('event_id')] ] )->first();

            if(!empty($req)){
                $req->delete();
            }

        }

        $event_in_session = $request->session()->pull('event', []);
        session(['event'=>$event]);

        return json_encode($services);
    }

    public function save_event(Request $request){

        $input = Input::all();

        if( Sentinel::guest() && empty($input['email']) ){
            return json_encode(array('success'=>false, 'login'=>false, 'events'=>false, 'save'=>false, 'noauth'=>true));
        }

        $id = $input['id'];

        $time = json_decode($input['time']);

        $user = Sentinel::getUser();

        $event = session('event_id') && session('event_id') !== 0 ? Events::find($request->session()->pull('event_id')) : new Events;

        if(empty($event) && empty($input['email']) || empty($user) && empty($input['email']) || !empty($user) && (int) $user->type === 1 || is_numeric($id) && !empty($user) && $user->id !== $event->user_id && $user->is_admin === 0){
                return json_encode(array('success'=>false, 'login'=>false, 'events'=>false, 'save'=>false, 'noauth'=>false));
        }

        if(!session('event')) return json_encode(array('success'=>false, 'login'=>false, 'events'=>true, 'save'=>false, 'noauth'=>false));

        if(!empty($input['email']) && empty($user)){

            $credentials = [
                'login' => $input['email'],
            ];

            $user = Sentinel::findByCredentials($credentials);

            if(!empty($user)){
                if( (int) $user->suspended === 1 ){
                    return json_encode(array('success'=>false, 'login'=>false, 'events'=>false, 'save'=>false, 'noauth'=>false, 'suspended'=>true));
                }
                return json_encode(array('success'=>false, 'login'=>true, 'events'=>true, 'save'=>false, 'noauth'=>false));
            }

            $event = new SavedEvents;
            $event->event_title = $id;
            $event->event_content = json_encode($request->session()->pull('event', []));
            $event->start = strtotime($time->start_time . " " . $time->start_date);
            $event->end = strtotime($time->end_time . " " . $time->end_date);
            $event->email = $input['email'];
            // $event->location = !empty($input['location']) ? ucwords($input['location']) : '';
            if($event->save()){
                $pass = substr(base64_encode(rand(0,99999) . time()), 0, 8);
                $acct = new AcctHelper;
                $result = $acct->registerUser($input['email'], $pass, 0, true);
                return json_encode(array('success'=>true, 'id'=>$event->id, 'email'=>true, 'result'=>$result));
            }
            return json_encode(array('success'=>false, 'login'=>false, 'saved_events'=>true, 'save'=>false, 'noauth'=>false));
        }

        $event_in_session = $request->session()->pull('event', []);

        if(!is_array($event_in_session)){
            $events_obj = json_decode($event_in_session);
            $event_in_session = [];
            foreach($events_obj as $key => $value) {
                $event_in_session[] = $value;
            }
        }

        $event->name = !empty($event->name) ? $event->name : $id;
        $event->services = json_encode($event_in_session);
        $event->start = strtotime($time->start_time . " " . $time->start_date);
        $event->end = strtotime($time->end_time . " " . $time->end_date);
        $event->annual = (int) $input['annual'];
        $event->user_id = $user->id;

        if($event->save()){
            return json_encode(array('success'=>true, 'id'=>$event->id));
        }
        session(['event'=>$event_in_session]);
        return json_encode(array('success'=>false, 'login'=>false, 'events'=>true, 'save'=>false, 'noauth'=>false));

    }

}
