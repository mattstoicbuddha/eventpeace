<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use App\Services as Services;
use App\Categories as Categories;
use App\Vendors as Vendors;
use App\Extras;
use App\Events;
use App\Requests as EvpReq;

class ExtrasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if(Sentinel::guest()) return json_encode((object) array('success'=>false));
        $user = Sentinel::getUser();
        $extra = Extras::find($id);
        $event = Events::find($extra->event_id);
        $service = Services::find($extra->service_id);
        
        if(empty($event->user_id) && $extra->event_id !== 0 || $service->vendor_id !== $user->id && $user->is_admin === 0 && $event->user_id !== $user->id ) return json_encode((object) array('success'=>false));
        $extra->delete();
        return json_encode((object) array('success'=>true));
    }
}
