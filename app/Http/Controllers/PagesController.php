<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Categories;

class PagesController extends Controller
{
	public function index(){

		$top_cats = array();

        $top = Categories::where("parent_category", 0)->get();

        foreach($top as $t){

            $top_cats[] = $t;

        }

        $top_cats = json_decode(json_encode($top_cats));

    	return view('pages.homepage', array('categories'=>$top_cats));
	}

	public function browsers(){
		return view('pages.browsers');
	}
	public function vendorfaq() {
        return view ('pages.vendorfaq');
    }
    public function plannerfaq() {
        return view ('pages.plannerfaq');
    }

    public function terms() {
        return view ('pages.terms');
    }

    public function privacy() {
        return view ('pages.privacy');
    }
}