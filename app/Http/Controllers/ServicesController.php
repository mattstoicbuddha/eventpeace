<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Services as Services;
use App\Categories as Categories;
use App\Vendors as Vendors;
use App\Extras;
use App\Requests as EvpReq;
use App\RequestNotes;
use App\Events;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use AccountHelper as AcctHelper;
use GeoHelper;
use Illuminate\Support\Facades\Storage;
use Cartalyst\Stripe\Stripe;
use Illuminate\Support\Facades\Input;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('services.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $user = Sentinel::getUser();
        if(empty($user)){
            return redirect()->action('UsersController@get_login');
        }

        $vendor = Vendors::where( 'user_id', $user->id )->first();

        if( empty($vendor) || empty($vendor->id) ){
            return redirect()->action('UsersController@get_login');
        }

        if(empty($vendor->_geoloc)){
            return view('errors.services.missing_address', compact('vendor'));
        }

        $top_cats = array();

        $top = Categories::where("parent_category", 0)->get();

        foreach($top as $t){

            $top_cats[] = $t;

        }

        $top_cats = json_decode(json_encode($top_cats));

        return view('services.create', array('categories'=>$top_cats));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = Sentinel::getUser();
        if(empty($user)){
            return redirect()->action('UsersController@get_login');
        }
        $vendor=Vendors::where("user_id", $user->id)->first();

        if(empty($vendor->_geoloc)){
            return view('errors.services.missing_address', compact('vendor'));
        }

        $top_cats = array();

        $top = Categories::where("parent_category", 0)->get();

        foreach($top as $t){

            $top_cats[] = array('category'=>$t);

        }

        $categories = json_decode(json_encode($top_cats));

        $input = Input::all();

        $result = (object) array('success'=>false);

        $geoloc = array();

        if(!empty($vendor->address)){

            $geograb = (new GeoHelper)->address_info( $vendor->address . " " . $vendor->city . " " . $vendor->state . " " . $vendor->zip);
            if($geograb){
                $geograb = json_decode($geograb);
                if(empty($geograb->results) || empty($geograb->results[0])){
                    $geoloc = array('lat'=>90, 'lng'=>-90);
                } else {
                    $geoloc = array('lat'=>$geograb->results[0]->geometry->location->lat, 'lng'=>$geograb->results[0]->geometry->location->lng);
                }
            } else {
                $geoloc = array('lat'=>90, 'lng'=>-90);
            }
        }

        if(!empty($input['servicePic'])){

            $img = $request->file('servicePic')->store('services');

            $imgexpl = explode("/", $img);

            $filename = end( $imgexpl );

            $disk = Storage::disk('gcs');

            // Put a private file on the 'gcs' disk which is a Google Cloud Storage bucket
            if($disk->put("/eventpeace/services/".$filename, file_get_contents(storage_path("app/public/".$img)))) $service_pic = $filename;

        }

        $tags = [];

        if(!empty($input['serviceTags'])){


            $tagsplit = explode(',', $input['serviceTags']);
            if(count($tagsplit) > 4) $tagsplit = array_slice($tagsplit, 0, 3);
            foreach($tagsplit as $tag){
                $tags[] = $tag;
            }

        }
        try{

            $service = !empty($input['service_id']) ? Services::where('id', $input['service_id']) : new Services;
            $service->name = $input['serviceTitle'];
            $service->description = $input['serviceDescription'];
            $service->price = (int) ( $input['servicePrice'] * 100 );
            $service->price_type = (int) $input['servicePriceType'];
            $service->price_details = $input['servicePriceDetails'];
            $service->vendor_id = $user->id;
            $service->category = (int) $input['selectServiceCategory'];
            $service->subcategory = (int) $input['selectServiceSubcat'];
            $service->delivery_time = $input['deliveryTime'];
            $service->delivery_charge = (int) $input['deliveryCharge'] * 100;
            $service->terms_conditions = $input['termsAndConditions'];
            $service->is_custom = !empty($input['is_custom']) ? $input['is_custom'] : 0;
            $service->auto_accept = !empty($input['auto_accept']) ? $input['auto_accept'] : 0;
            $service->image = !empty($service_pic) ? $service_pic : "";
            $service->_tags = !empty($input['serviceTags']) ? $tags : [];
            $service->calendar_code = !empty($input['calendarCode']) ? $input['calendarCode'] : "";
            if(!empty($geograb)){
                $service->_geoloc = array('lat'=>$geograb->results[0]->geometry->location->lat, 'lng'=>$geograb->results[0]->geometry->location->lng);
            }

            if($service->save()){

                if(!empty($input['extrafield'])){

                    for($a=0; $a<count($input['extrafield']); $a++){

                        $newextra = new Extras;
                        $newextra->name = $input['extraname'][$a];
                        $newextra->price = (int)$input['extraprice'][$a]*100;
                        $newextra->description = $input['extradetails'][$a];
                        $newextra->service_id = $service->id;
                        $newextra->save();
                    }

                }

                $result->success = true;
                return redirect()->action('ServicesController@show', ['id'=>$service->id]);
            } else {
                $result->false = true;
            }
        } catch(Exception $e){
            return view('errors.services.save');
        }

        return redirect()->action('ServicesController@create')->with('failed');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user = Sentinel::getUser();

        $service = Services::find($id);

        $category = Categories::find($service->category);

        $subcategory = Categories::find($service->subcategory);

        $extras = Extras::where('service_id', $service->id)->get();

        $calendar = $this->get_calendar_sources($id);

        return view('services.show', compact('user', 'service', 'category', 'subcategory', 'extras', 'calendar'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = Sentinel::getUser();

        $service = Services::find($id);

        if( (int) $user->id !== (int) $service->vendor_id && (int) $user->is_admin !== 1){
            return redirect()->action('UsersController@get_login');
        }

        $vendor = Vendors::where( 'user_id', $service->vendor_id )->first();

        if( empty($vendor) || empty($vendor->id) ){
            abort(404);
        }

        if(empty($vendor->_geoloc)){
            return view('errors.services.missing_address', compact('vendor'));
        }

        $service = Services::find($id);

        $top_cats = $top_subs = array();

        $top = Categories::where("parent_category", 0)->get();

        foreach($top as $t){

            $top_cats[] = $t;

        }

        $top_cats = json_decode(json_encode($top_cats));

        $subs = Categories::where("parent_category", $service->category)->get();

        $extras = Extras::where('service_id', $service->id)->get();

        return view('services.edit', array('categories'=>$top_cats, 'subcategories'=>$subs, 'services'=>$service, 'extras'=>$extras));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $service = Services::find($id);

        $user = Sentinel::getUser();
        if( (int) $user->id !== (int) $service->vendor_id && (int) $user->is_admin !== 1){
            return redirect()->action('UsersController@get_login');
        }

        $input = Input::all();

        $service = Services::find($id);

        $vendor = Vendors::where( 'user_id', $service->vendor_id )->first();

        if( empty($vendor) || empty($vendor->id) ){
            abort(404);
        }

        if(empty($vendor->_geoloc)){
            return view('errors.services.missing_address', compact('vendor'));
        }

        $geograb = (new GeoHelper)->address_info($vendor->address . " " . $vendor->city . " " . $vendor->state . " " . $vendor->zip);

        $geoloc = array();
        if($geograb){
            $geograb = json_decode($geograb);
            if(empty($geograb->results) || empty($geograb->results[0])){
                $geoloc = array('lat'=>90, 'lng'=>-90);
            } else {
               $geoloc = array('lat'=>$geograb->results[0]->geometry->location->lat, 'lng'=>$geograb->results[0]->geometry->location->lng);
            }
        } else {
            $geoloc = array('lat'=>90, 'lng'=>-90);
        }

        if(!empty($input['extrafield'])){

            for($a=0; $a<count($input['extrafield']); $a++){

                $newextra = !empty($input['extraid'][$a]) ? Extras::find($input['extraid'][$a]) : new Extras;
                $newextra->name = $input['extraname'][$a];
                $newextra->price = (int)$input['extraprice'][$a]*100;
                $newextra->description = $input['extradetails'][$a];
                $newextra->service_id = $service->id;
                $newextra->event_id = 0;
                $newextra->save();

            }
        }

        $tags = empty($input['serviceTags']) && !empty($service->tags) && is_array($service->tags) ? $service->tags : array(); // Define it here just in case we need tags to be empty.

        if(!empty($input['serviceTags'])){

            if(strpos($input['serviceTags'], ",") !== false){
                $tagsplit = explode(',', $input['serviceTags']);
                if(count($tagsplit) > 4) $tagsplit = array_slice($tagsplit, 0, 3);
                foreach($tagsplit as $tag){
                    $tags[] = $tag;
                }
            } else {
                $tags[] = $input['serviceTags'];
            }

        }

        if(!empty($input['servicePic'])){

            $img = $request->file('servicePic')->store('services');

            $imgexpl = explode("/", $img);

            $filename = end( $imgexpl );

            $disk = Storage::disk('gcs');

            // Put a private file on the 'gcs' disk which is a Google Cloud Storage bucket
            if($disk->put("/eventpeace/services/".$filename, file_get_contents(storage_path("app/public/".$img)))) $service_pic = $filename;

        }

        try{

            if($service->update(array(
                "name"=>$input['editServiceTitle'],
                "description"=>$input['editServiceDescription'],
                "price"=> (int) ( $input['editServicePrice']*100 ),
                "price_type"=> (int) $input['servicePriceType'],
                "price_details"=>$input['editServicePriceDetails'],
                "category"=>(int) $input['editSelectServiceCategory'],
                "subcategory"=>(int) $input['editSelectServiceSubcat'],
                "delivery_time"=>$input['editDeliveryTime'],
                "delivery_charge"=>$input['editDeliveryCharge']*100,
                "terms_conditions"=>$input['editTermsAndConditions'],
                "auto_accept"=> !empty($input['auto_accept']) ? $input['auto_accept'] : 0,
                "image"=>!empty($service_pic) ? $service_pic : $service->image,
                "is_custom"=>0,
                "_tags" => !empty( $input['serviceTags'] ) && !empty($tags) ? $tags : array(),
                "calendar_code" => !empty($input['calendarCode']) ? $input['calendarCode'] : "",
                "_geoloc"=>$geoloc,
                )
                )){
                    $result = (object) array('success'=>true);
                }


        } catch(Exception $e){
            return view('errors.services.save');
        }

        return redirect()->action('ServicesController@show', ['id'=>$service->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = Sentinel::getUser();
        if(empty($user)){
            return redirect()->action('UsersController@get_login');
        }
        $service = Services::find($id);
        if( (int) $user->id !== (int) $service->vendor_id && (int) $user->is_admin !== 1 ) abort(403);

        $service->delete();

        return redirect('/services/dashboard');
    }

    public function get_extras($event_id, $service_id){
        $extras = [];

        $qry = [ ['service_id', '=', $service_id], ['is_custom', '=', 0] ];

        $cust_qry = [ ['service_id', '=', $service_id], ['event_id', '=', $event_id], ['is_custom', '=', 1] ];

        $default_extras = Extras::where($qry)->get();

        $cust_extras = Extras::where($cust_qry)->get();

        $extras = array_merge($default_extras->toArray(), $cust_extras->toArray());

        $request = EvpReq::where([
            ['service_id', $service_id],
            ['event_id', $event_id],
            ])->first();

        $selected_extras = !empty($request->extras) ? json_decode($request->extras) : [];

        if(!empty($request->id)){

            for($i = 0; $i<count($extras); $i++){
                if( array_search($extras[$i]['id'], $selected_extras) !== false ){
                    $extras[$i]['selected'] = true;
                }
            }

        }

        return json_encode(json_decode( json_encode( $extras ) ) );
    }

    public function show_dashboard($expired = false)
    {
        $user = Sentinel::getUser();
        if(empty($user)){
            return redirect()->action('UsersController@get_login');
        }

        $services = Services::select('id', 'name')->where('vendor_id', $user->id)->orderBy('id', 'DESC')->get();

        $servicesArray = Services::select('id')->where('vendor_id', $user->id)->orderBy('id', 'DESC')->get();

        $servicesArray->toArray();

        $all_requests = [];

        // Expired vs current
        if($expired){
        $requests = EvpReq::whereIn('service_id', $servicesArray)->where('time_stop', '<', time())->orderBy('time_start', 'ASC')->get();
        } else {
        $requests = EvpReq::whereIn('service_id', $servicesArray)->where('time_stop', '>', time())->orderBy('time_start', 'ASC')->get();
        }
        foreach($requests as $r){
            $extras = [];
            $all_extras = !empty($r->extras) ? json_decode($r->extras) : [];
            foreach($all_extras as $allex){
                $extras[] = Extras::find($allex);
            }
            $r->extras = $extras;
            $all_notes = [];
            $notes = RequestNotes::where('request_id', $r->id)->orderBy('id', 'DESC')->get();
            foreach($notes as $note){
                $note->author_name = $note->author_name = (int) $note->author === $user->id ? "You" : "Planner";
                $all_notes[] = $note;
            }
            $r->notes = $all_notes;
            $service = Services::find($r->service_id);
            $total = 0;
            $quant = !empty($r->quantity) && $r->quantity > 0 ? $r->quantity : 1;
            $total = $total + $service->price * $quant;
            $total = $total + $service->delivery_charge;
            if( !is_array($r->extras) ) $r->extras = [];
            foreach($r->extras as $extra){
                if(empty($extra)) continue;
                $total = $total + ( $extra->price  * $quant );
            }
            $r->total = $total;
            $this_user = Sentinel::findById($r->planner_id);
            $all_requests[] = (object) array('request'=>$r, 'service'=>$service, 'user'=>$this_user);
        }

        return view('services.dashboard', compact('user', 'services', 'all_requests', 'expired'));
    }

    public function save_dashboard()
    {
        return view('services.dashboard');
    }

    public function show_modal($id)
    {
        $user = Sentinel::getUser();

        $service = Services::find($id);

        $category = Categories::find($service->category);

        $subcategory = Categories::find($service->subcategory);

        $extras = Extras::where('service_id', $service->id)->get();

        $calendar = $this->get_calendar_sources($id);

        return view('services.show_backbone', compact('user', 'service', 'category', 'subcategory', 'extras', 'calendar'));
    }

    public function tags_autocomplete(){

        $all_tags = Services::where( '_tags', 'like', "%" . $_GET['term'] . "%" )->get(['_tags']);

        $tags = [];

        foreach($all_tags as $a){
            foreach($a->_tags as $t){
                if(strpos($t, $_GET['term']) !== false){
                    $tags[] = $t;
                }
            }
        }

        return json_encode($tags);

    }

    public function get_calendar_sources($id){

        $service = Services::find($id);

        if(!$service) return "";

        $sources = "";

        if(!empty($service->calendar_code)){
            $srcstring = explode('"', explode("https://calendar.google.com/calendar/embed?", $service->calendar_code)[1])[0];
            if(strpos($srcstring, "&amp;") !== false){
                $props = explode("&amp;", $srcstring);
                foreach($props as $prop){
                    if(strpos($prop, "src=") === false && strpos($prop, "ctz=") === false) continue;
                    $sources .= $prop . "&amp;";
                }
                $sources = rtrim($sources, "&amp;");
            } else {
                $props = explode("&", $srcstring);
                foreach($props as $prop){
                    if(strpos($prop, "src=") === false && strpos($prop, "ctz=") === false && strpos($prop, "wkst=") === false && strpos($prop, "color=") === false) continue;
                    $sources .= $prop . "&";
                }
                $sources = rtrim($sources, "&");
            }
        }

        return $sources;
    }

}
