<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Vendors;
use App\Services;
use App\Requests as EvpReq;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Support\Facades\Input;
use AccountHelper as AcctHelper;
use GeoHelper;
use Illuminate\Support\Facades\Storage;
use Cartalyst\Stripe\Stripe;

class VendorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "lol";
        //return view('vendors.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->action('VendorsController@edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = Sentinel::getUser();
        if(empty($user)){
            return redirect()->action('UsersController@get_login');
        }

        $input = Input::all();

        // $this->validate($request, [
        // 'name' => 'bail|required|max:255',
        // 'address' => 'bail|required|max:255',
        // 'city' => 'bail|required|max:255',
        // 'state' => 'bail|required|max:255',
        // 'zip' => 'bail|required|max:10',
        // 'description' => 'bail|required',
        // 'phone' => 'bail|required|max:10',
        // 'website' => 'bail|required',
        // 'email' => 'bail|required|max:255',
        // 'business_contact' => 'bail|required|max:255',
        // 'name' => 'bail|required|max:255',
        // 'business_contact_title' => 'bail|required|max:255'
        //  ]);


        $vendor = new Vendors;
        $vendor->name = $input['name'];
        $vendor->address = $input['address'];
        $vendor->city = $input['city'];
        $vendor->state = $input['state'];
        $vendor->zip = $input['zip'];
        $vendor->description = $input['description'];
        $vendor->phone = $input['phone'];
        $vendor->website = $input['website'];
        $vendor->email = $input['email'];
        $vendor->business_contact = $input['business_contact'];
        $vendor->business_contact_title = $input['business_contact_title'];
        $vendor->price_range_low = !empty($input['price_range_low']) ? (int) $input['price_range_low'] : 0;
        $vendor->price_range_high = !empty($input['price_range_high']) ? (int) $input['price_range_high'] : 0;
        $vendor->user_id = $input['user_id'];

        if(!empty($input['profilePic'])){

            $img = $request->file('somefile')->store('vendors');

            $filename = end( ( explode("/", $path) ) );

            $disk = Storage::disk('gcs');

            // Put a private file on the 'gcs' disk which is a Google Cloud Storage bucket
            $disk->put("/eventpeace/vendors/profile/".$filename, file_get_contents(storage_path("app/".$img)));

            $vendor->profile_pic = $filename;

        }

        $vendor->_geoloc = array();

        $geograb = (new GeoHelper)->address_info($input['address'] . " " . $input['city'] . " " . $input['state'] . " " . $input['zip']);

        if($geograb){
            $vendor->_geoloc = array('lat'=>$geograb->geometry->location->lat, 'lng'=>$geograb->geometry->location->lng);
        }
        // GO THROUGH THE STRIPE BULLSHIT TO SAVE CREDIT CARD
        //$input['stripe_customer_id']

        if($vendor->save()){
            $result = (object) array('success'=>true);
        }

        return view('vendors.save', compact('result'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vendor = Vendors::find($id);
        if(empty($vendor)){
            return view('errors.404');
        }
        $services = Services::where('vendor_id', $vendor->user_id)->get();
        return view('vendors.show', compact('vendor', 'services'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $input = Input::all();
        $user = Sentinel::getUser();
        $vendors = Vendors::find($id);
        if( (int) $user->id !== (int) $vendors->user_id && (int) $user->is_admin !== 1){
            return redirect()->action('UsersController@get_login');
        }
        $stripe = new Stripe(env('STRIPE_SECRET', 'sk_test_95SgVueeUDeTZIPc5YnSaSMB'));
        $cards = new \stdClass;
        if(!empty($user->stripe_customer_id)){
            $cards = json_decode(json_encode(($stripe->cards()->all($user->stripe_customer_id))));
        }

        return view('vendors.edit', compact('vendors', 'cards'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $input = Input::all();
        $user = Sentinel::getUser();
        $vendor = Vendors::find($id);
        if( (int) $user->id !== (int) $vendor->user_id && (int) $user->is_admin !== 1){
            return redirect()->action('UsersController@get_login');
        }

        $geograb = (new GeoHelper)->address_info($input['vendorsAddress'] . " " . $input['vendorsCity'] . " " . $input['vendorsState'] . " " . $input['vendorsZip']);

        $geoloc = array();
        if($geograb){
            $geograb = json_decode($geograb);
            if(!empty($geograb->results[0])) $geoloc = array('lat'=>$geograb->results[0]->geometry->location->lat, 'lng'=>$geograb->results[0]->geometry->location->lng);
        }

        $profile_pic = !empty($vendor->profile_pic) ? $vendor->profile_pic : "";

        if(!empty($input['profilePic'])){

            $img = $request->file('profilePic')->store('vendors');

            $imgexpl = explode("/", $img);

            $filename = end( $imgexpl );

            $disk = Storage::disk('gcs');

            // Put a private file on the 'gcs' disk which is a Google Cloud Storage bucket
            $disk->put("/eventpeace/vendors/profile/".$filename, file_get_contents(storage_path("app/public/".$img)));

            $profile_pic = $filename;

        }
        $password = 1;
        if(!empty($input['newPassword'])){
            if($input['newPassword'] === $input['newPassword2']){
                $credentials = [
                    'password' => $input['newPassword'],
                ];
                Sentinel::update($user, $credentials);
                $password = 2;
            } else {
                $password = 0;
            }
        }

        if($vendor->update(array(
            "name"=>$input['vendorsName'],
            "address"=>$input['vendorsAddress'],
            "city"=>$input['vendorsCity'],
            "state"=>$input['vendorsState'],
            "zip"=>$input['vendorsZip'],
            "description"=>$input['vendorsDescription'],
            "phone"=>$input['vendorsPhone'],
            "website"=>$input['vendorsWebsite'],
            "email"=>$input['vendorsEmail'],
            "business_contact"=>$input['vendorsBusinessContact'],
            "business_contact_title"=>$input['vendorsBusinessContactTitle'],
            "price_range_low"=>!empty($input['priceRangeLow']) ? (int) $input['priceRangeLow'] : 0,
            "price_range_high"=>!empty($input['priceRangeHigh']) ?(int) $input['priceRangeHigh'] : 0,
            "profile_pic"=>$profile_pic,
            "_geoloc"=>$geoloc,
            )
            )){
                $result = (object) array('success'=>true);
            }
            return redirect()->action('VendorsController@edit', ['id'=>$id])->with(compact('result', 'password'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $grant = (new AcctHelper)->vendor_grant_access($input['vendor_id']);
        if(!$grant->success){
            if($grant->redirect){
                // redirect to login
            } else {
                // permissions issue, determine what to do
            }
        }
        $vendor = Vendors::find($id);
        $result = (object) array('success'=>true);

        if($vendor->delete()){
            $result = (object) array('success'=>true);
        }

        return view('vendors.deleted', compact('result'));
    }

    public function show_profile()
    {

        $user = Sentinel::getUser();
        if(empty($user)){
            return redirect()->action('UsersController@get_login');
        }
        $vendor = Vendors::where('user_id', $user->id)->first();

        if(empty($vendor)){
            return redirect()->action('UsersController@get_login');
        }

        $services = Services::where('vendor_id', $user->id)->get();

        $geograb = (new GeoHelper)->address_info($vendor->address . " " . $vendor->city . " " . $vendor->state . " " . $vendor->zip);

        $geoloc = array();
        if($geograb){
            $geograb = json_decode($geograb);
            if(empty($geograb->results) || empty($geograb->results[0])){
                $geoloc = false;
            } else {
                $geoloc = (object) array('lat'=>$geograb->results[0]->geometry->location->lat, 'lng'=>$geograb->results[0]->geometry->location->lng);
            }
        }

        return view('vendors.profile', compact('user', 'vendor', 'services', 'geoloc'));
    }

    public function landing() {
        return view ('vendors.vendor-landing');
    }

    public function tips() {
        return view ('vendors.vendor-tips');
    }
}
