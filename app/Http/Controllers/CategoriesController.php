<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Categories as Categories;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_top()
    {
        $top_cats = array();

        $top = Categories::where("parent_category", 0)->get();

        foreach($top as $t){

            $top_cats[] = $t; 

        }

        $top_cats = json_decode(json_encode($top_cats));

        return $top_cats;

    }

    public function get_children($parent){

        $cats = array();

        $cat = Categories::where("parent_category", $parent)->get();

        foreach($cat as $c){

            $cats[] = $c; 

        }

        $cats = json_decode(json_encode($cats));

        return $cats;

    }

    public function get_category($c){

        $cat = Categories::where("id", $c)->get();

        $cat = json_decode(json_encode($cat));

        return $cat;

    }

    
}
