<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Illuminate\Support\Facades\Input;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use App\Mail\ActivateAccount;
use App\Vendors;
use App\Planners;
use App\Services;
use App\Events;
use App\SavedEvents;
use Cartalyst\Stripe\Stripe;
use App\Helpers\AccountHelper;
use App\Mail\NewPassword;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = Input::all();

        $credentials = [
            'login' => $input['userEmailAddress'],
        ];

        $user = Sentinel::findByCredentials($credentials);

        if(!empty($user)){
            $exists = true;
            return redirect()->action('UsersController@get_login')->with('exists', $exists);
        }

        $acct = new AccountHelper;

        $result = $acct->registerUser($input['userEmailAddress'], $input['userPassword'], (int) $input['userType']);
        return view('users.store', compact('result'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = (int) $id; // I SHOULDN'T HAVE TO DO THIS GOD DAMMIT!
        $user = Sentinel::getUser();
        if(empty($user) || $user->id !== $id && $user->is_admin === 0){
            // Redirect to login
            return;
        }

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('users.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return view('users.update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return view('users.destroy');
    }

    public function activate($id, $code)
    {

        $user = Sentinel::findById($id);
        $result = (object) array("success"=>false);
        if($user){
            if((new \Cartalyst\Sentinel\Activations\IlluminateActivationRepository)->complete($user, $code)){
                $result->success = true;
                $stripe = new Stripe(env('STRIPE_SECRET', 'sk_test_95SgVueeUDeTZIPc5YnSaSMB'), env('STRIPE_VERSION', '2016-07-06'));
                $customer = $stripe->customers()->create([
                    'email' => $user->email,
                ]);
                $user->stripe_customer_id = $customer['id'];
                $user->save();
            }
        }

        if($result->success){
            return view('users.activation', compact('result'));
        } else {
            abort(403, "Your account could not be activated.  If it has already been activated, you're good to go.  If it has not, please contact us.");
        }

    }

    public function get_login()
    {
        return view('users.login');
    }

    public function post_login()
    {

        $input = Input::all();
        $onthefly = false;

        if(!empty($input['onthefly'])){ // Login done from modal
            $onthefly = true;
            parse_str($input['formdata'], $input);
        }

        $user = Sentinel::findByCredentials(['login'=>$input['userEmailAddress']]);

        if(empty($user)){
            if($onthefly){ // Login done from modal
                return json_encode( (object) array('success'=>false, 'checkmail'=>false, 'nouser'=>true, 'noauth'=>false));
            }
            return redirect()->action('UsersController@get_login')->with('nouser', true);
        }

        if(!(new \Cartalyst\Sentinel\Activations\IlluminateActivationRepository)->completed($user)){
            \Mail::to($user->email)->send(new ActivateAccount($user));
            if($onthefly){ // Login done from modal
                return json_encode( (object) array('success'=>false, 'checkmail'=>true, 'nouser'=>false, 'noauth'=>false));
            }
            return redirect()->action('UsersController@get_login')->with('checkemail', true);
        }

        $credentials = [
            'email'    => $input['userEmailAddress'],
            'password' => $input['userPassword'],
        ];

        $auth = Sentinel::authenticateAndRemember($credentials);

        if(!$auth || empty($auth)){

            if($onthefly){ // Login done from modal
                return json_encode( (object) array('success'=>false, 'checkmail'=>false, 'nouser'=>false, 'noauth'=>true));
            }

            return redirect()->action('UsersController@get_login')->with(['noauth'=>true, 'email'=>$input['userEmailAddress']]);

        }

        // If the user is suspended, give them the account suspension error page.
        if((int) $auth->suspended === 1) return view('errors.suspended');

        /* Set up saved events in case they made an event and registered */
        $saved = SavedEvents::where('email', $auth->email)->get();

        if(!empty($saved)){
            foreach($saved as $s){
                $event = new Events;
                $event->name = !empty($s->event_title) ? $s->event_title : "";
                $event->services = !empty($s->event_content) ? $s->event_content : "[]";
                $event->user_id = $auth->id;
                $event->start = $s->start;
                $event->end = $s->end;
                $event->save();
                $s->delete();
            }
        }

        if($onthefly){ // Login done from modal
            return json_encode( (object) array('success'=>true, 'acct'=>$auth->type));
        }

        if((int) $auth->type === 1){ // USER IS VENDOR
            return redirect('/services/dashboard');
        } else {
            return redirect('/events/dashboard');
        }
    }

    public function get_profile()
    {

        $user = Sentinel::getUser();
        if(empty($user)){
            return redirect()->action('UsersController@get_login');
        }
        return view('users.profile', compact('user'));
    }

    public function logout(){
        if(!Sentinel::guest()){
            Sentinel::logout();
        }
        return redirect()->action('UsersController@get_login');
    }

    public function add_card(){

        $user = Sentinel::getUser();
        $result = (object) array('success'=>false);
        if(empty($user)){
            if(!empty($input['ajaxPost']) && $input['ajax']){
                $result->login = false;
                return json_encode($result);
            }
            return redirect()->action('UsersController@get_login');
        }

        $input = Input::all();

        try{

            if(!empty($input['stripe_token']) && !empty($user->stripe_customer_id)){
                $stripe = new Stripe(env('STRIPE_SECRET', 'sk_test_95SgVueeUDeTZIPc5YnSaSMB'));
                $card = $stripe->cards()->create($user->stripe_customer_id, $input['stripe_token']);
                $result->success = true;
                $result->card = $card;
            } else if(empty($input['stripe_token'])) {
                $result->error = "We did not receive your card information correctly.  Please verify your card info.";
            } else if(empty($user->stripe_customer_id)) {
                $result->error = "We do not have a customer id for our payment processor for you.  Please contact tech support.";
            } else {
                $result->error = "Unknown error.  Please try again momentarily.";
            }
        } catch (\Guzzle\Service\Exception\ValidationException $e){
            $result->success = false;
            $result->error = $e->getMessage();
        } catch (\Cartalyst\Stripe\Exception\CardErrorException $e){
            $result->success = false;
            $result->error = $e->getMessage();
        } catch (\Cartalyst\Stripe\Exception\BadRequestException $e){
            $result->success = false;
            $result->error = $e->getMessage();
        } catch (\Cartalyst\Stripe\Exception\UnauthorizedException $e){
            $result->success = false;
            $result->error = $e->getMessage();
        } catch (\Cartalyst\Stripe\Exception\RequestFailedException $e){
            $result->success = false;
            $result->error = $e->getMessage();
        } catch (\Cartalyst\Stripe\Exception\NotFoundException $e){
            $result->success = false;
            $result->error = $e->getMessage();
        } catch (\Cartalyst\Stripe\Exception\ServerErrorException $e){
            $result->success = false;
            $result->error = $e->getMessage();
        }  catch (Exception $e) {
            $result->success = false;
            $result->error = $e->getMessage();
        }

        if(!empty($input['ajaxPost']) && $input['ajaxPost']){
            return json_encode($result);
        }

        if((int) $user->type === 1){ // USER IS VENDOR
            $vendor = Vendors::where('user_id', $user->id)->first();
            return redirect()->action('VendorsController@edit', ['id'=>$vendor->id]);
        } else {
            $planner = Planners::where('user_id', $user->id)->first();
            return redirect()->action('PlannersController@edit', ['id'=>$planner->id]);
        }

    }

    public function delete_card(){

        $user = Sentinel::getUser();
        if(empty($user)){
            return redirect()->action('UsersController@get_login');
        }

        $input = Input::all();

        if(!empty($input['card_id']) && !empty($user->stripe_customer_id)){
            $stripe = new Stripe(env('STRIPE_SECRET', 'sk_test_95SgVueeUDeTZIPc5YnSaSMB'));
            $card = $stripe->cards()->delete($user->stripe_customer_id, $input['card_id']);
        }

        if((int) $user->type === 1){ // USER IS VENDOR
            $vendor = Vendors::where('user_id', $user->id)->first();
            return redirect()->action('VendorsController@edit', ['id'=>$vendor->id]);
        } else {
            $planner = Planners::where('user_id', $user->id)->first();
            return redirect()->action('PlannersController@edit', ['id'=>$planner->id]);
        }

    }

    public function reindex(){
        Vendors::clearIndices();
        Services::clearIndices();
        Vendors::reindex();
        Services::reindex();
    }

    public function test_mail(){
        return "nope";
        if(Sentinel::guest()) return "Log in to test";
        $user = Sentinel::getUser();

        // \Mail::to($user->email)->send(new ActivateAccount($user));

        // $credentials = [ 'login' => 'jllnmelton1@gmail.com' ];
        // $user = Sentinel::findByCredentials($credentials);
        // \Mail::to($user->email)->send(new ActivateAccount($user));

        // $credentials = [ 'login' => 'turner_bob@hotmail.com' ];
        // $user = Sentinel::findByCredentials($credentials);
        // \Mail::to($user->email)->send(new ActivateAccount($user));

        // $credentials = [ 'login' => 'courtneyjcomstock@yahoo.com' ];
        // $user = Sentinel::findByCredentials($credentials);
        // \Mail::to($user->email)->send(new ActivateAccount($user));

        return "result: " . $user->email;
    }

    public function reset_password($email){
        $result = (object) array('success'=>false);
        $user = Sentinel::findByCredentials(['login'=>$email]);
        if(empty($user)) return $result;
        $newPass = bin2hex(openssl_random_pseudo_bytes(4));
        $credentials = [
            'password' => $newPass,
        ];
        Sentinel::update($user, $credentials);
        \Mail::to($user->email)->send(new NewPassword($user, $newPass));
        $result->success = true;
        return json_encode($result);
    }

}
