<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Requests as EvpReq;
use App\RequestNotes;
use App\Events;
use App\Services;
use App\Planners;
use App\Vendors;
use App\Mail\NewRequestNote;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Support\Facades\Input;

use Helpers\AccountHelper as AcctHelper;

class RequestNotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $request_id)
    {
        if(Sentinel::guest()) return json_encode( (object) array( 'success'=>false, 'guest'=>true ) );
        $user = Sentinel::getUser();
        $req = EvpReq::find($request_id);

        if(empty($req->id)) return json_encode( (object) array( 'success'=>false, 'noreq'=>true ) );

        $event = Events::find($req->event_id);
        $service = Services::find($req->service_id);
        if( (int) $event->user_id !== (int) $user->id && (int) $service->vendor_id !== (int) $user->id && (int) $user->is_admin === 0){
            return json_encode( (object) array( 'success'=>false, 'idcheck'=>true ) );
        }

        $input = Input::all();

        $note = new RequestNotes;
        $note->author = $user->id;
        $note->note = $input['note'];
        $note->request_id = $request_id;
        $note->event_id = $req->event_id;


        if($note->save()){
            $request = $req;

            $request->name = $service->name;
            $request->note = $note->note;
            if( (int) $user->type === 0 ){
                $other = Vendors::where('user_id', $service->vendor_id)->first();
                $other_user = Sentinel::findUserById($service->vendor_id);
                $recipient = Planners::where('user_id', $user->id)->first();
                $request->recipient_name = !empty($other->name) ? $other->name : $other_user->email;
                $request->other_name = !empty($recipient->name) ? $recipient->name : "Planner";
                if(empty($other_user)) return json_encode( (object) array( 'success'=>true ) );
                $email = $request->recipient_email = $other_user->email;
            } else {
                $other = Planners::where('user_id', $event->user_id)->first();
                $other_user = Sentinel::findUserById($event->user_id);
                $recipient = Vendors::where('user_id', $service->vendor_id)->first();
                $request->recipient_name = !empty($other->name) ? $other->name : $other_user->email;
                $request->other_name = !empty($recipient->name) ? $recipient->name : "Vendor";
                if(empty($other_user)) return json_encode( (object) array( 'success'=>true ) );
                $email = $request->recipient_email = $other_user->email;
            }

            \Mail::to($email)->send(new NewRequestNote($request));
            return json_encode( (object) array( 'success'=>true ) );
        }
        return json_encode( (object) array( 'success'=>false, 'save'=>true ) );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user = Sentinel::getUser();
        $notes = (int) $id !== 0 ? RequestNotes::where('request_id', $id)->orderBy('id', 'DESC')->get() : [];
        $authors = [];
            for($a = 0; $a<count($notes); $a++){
                if(!empty($notes[$a]->author)){
                $author = !empty($authors[$notes[$a]->author]) ? $authors[$notes[$a]->author] : Sentinel::findUserById($notes[$a]->author);
                if(empty($author)) continue;
                if(empty($authors[$notes[$a]->author])) $authors[$notes[$a]->author] = $author;
                if((int) $user->id !== (int) $author->id && empty($acct)){
                    $acct = (int) $author->type === 1 ? Vendors::where('user_id', $author->id)->first() : Planners::where('user_id', $author->id)->first();
                }
                $notes[$a]->author = (object) array('id'=>$author->id, 'name'=>(int) $user->id === (int) $author->id ? 'You ' : $acct->name);
            }
        }
        return json_encode($notes);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
