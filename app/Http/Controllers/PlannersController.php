<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Planners as Planners;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Support\Facades\Input;
use AccountHelper as AcctHelper;
use Cartalyst\Stripe\Stripe;

class PlannersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('events.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->action('VendorsController@edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user = Sentinel::getUser();
        if(empty($user) || (int) $user->is_admin !== 1 && (int) $user->id !== (int) $id){
            abort(404); // If the user isn't an admin, they don't need to be here.
        }

        $planners = Planners::find($id);

        return view('planners.profile', compact('user', 'planners'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $input = Input::all();
        $user = Sentinel::getUser();
        $planners = Planners::find($id);
        if(empty($user) || (int) $user->id !== (int) $planners->user_id)  return redirect()->action('UsersController@get_login');

        $stripe = new Stripe(env('STRIPE_SECRET', 'sk_test_95SgVueeUDeTZIPc5YnSaSMB'));
        $cards = new \stdClass;
        if(!empty($user->stripe_customer_id)){
            $cards = json_decode(json_encode(($stripe->cards()->all($user->stripe_customer_id))));
        }
        return view('planners.edit', compact('planners', 'cards'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = Input::all();
        $user = Sentinel::getUser();
        $planner = Planners::find($id);
        if(empty($user) || (int) $user->id !== (int) $planner->user_id)  return redirect()->action('UsersController@get_login');

        $password = 1;
        if(!empty($input['newPassword'])){
            if($input['newPassword'] === $input['newPassword2']){
                $credentials = [
                    'password' => $input['newPassword'],
                ];
                Sentinel::update($user, $credentials);
                $password = 2;
            } else {
                $password = 3;
            }
        }

        if($planner->update(array(
            "name"=>$input['plannersName'],
            "address"=>$input['plannersAddress'],
            "city"=>$input['plannersCity'],
            "state"=>$input['plannersState'],
            "zip"=>$input['plannersZip'],
            "phone"=>$input['plannersPhone'],
            "website"=>$input['plannersWebsite'],
            )
            )){
            $result = (object) array('success'=>true);
        }

        return redirect()->action('PlannersController@edit', ['id'=>$id])->with(compact('result', 'password'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function show_profile()
    {
        $user = Sentinel::getUser();
        if(empty($user)){
            return redirect()->action('UsersController@get_login');
        }
        $planners = Planners::where('user_id', $user->id)->first();
        if(empty($planners)){
            return redirect()->action('UsersController@get_login');
        }
        return view('planners.profile', compact('user', 'planners'));
    }
}
