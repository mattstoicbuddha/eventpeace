<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use AlgoliaSearch\Laravel\AlgoliaEloquentTrait;

class Services extends Model
{
	use AlgoliaEloquentTrait;
    protected $table = 'services';

    protected $fillable = ['name', 'description', 'price', 'price_details', 'category', 'subcategory', 'delivery_time', 'delivery_charge', 'terms_conditions', 'is_custom', 'extras', 'vendor_id', 'image', '_tags', '_geoloc', 'price_type', 'auto_accept', 'calendar_code'];

    protected $casts = [
        '_geoloc' => 'array',
        '_tags' => 'array',
        'category' => 'integer',
        'subcategory' => 'integer',
        'vendor_id' => 'integer',
        'price' => 'integer',
        'delivery_charge' => 'integer',

    ];

    public $algoliaSettings = [
        'attributesToIndex' => [
            'id', 'name', 'description', 'price', 'price_details', 'category', 'subcategory', 'delivery_time', 'delivery_charge', 'terms_conditions', 'extras', 'vendor_id', 'image', '_tags', '_geoloc', 'price_type', 'auto_accept'
        ],
    ];
}
