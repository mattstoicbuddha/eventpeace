<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedEvents extends Model
{
    protected $table = 'saved_events';

    protected $fillable = ['email', 'event_title', 'event_content'];
}
