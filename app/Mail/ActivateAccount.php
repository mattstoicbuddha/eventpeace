<?php

namespace App\Mail;

use Cartalyst\Sentinel\Users\EloquentUser as User;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ActivateAccount extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;

    public function __construct(User $user, $password = "")
    {
        $this->user = $user;
        $this->user->activation_code = (new \Cartalyst\Sentinel\Activations\IlluminateActivationRepository)->create($user)->code;
        $this->user->the_password = $password;
        \Log::info('Sending the following password to the blade file: '.$this->user->the_passwordpassword);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@eventpeace.com')
                    ->subject('EventPeace Account Activation')
                    ->view('mail.activation');
    }
}
