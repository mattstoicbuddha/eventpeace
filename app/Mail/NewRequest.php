<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;
    public $planner;
    public $vendor;

    public function __construct($request, $vendor, $planner)
    {
        $this->request = $request;
        $this->planner = $planner;
        $this->vendor = $vendor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@eventpeace.com')
                    ->subject('New Request on EventPeace')
                    ->view('mail.new-request');
    }
}
