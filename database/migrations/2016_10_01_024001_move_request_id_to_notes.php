<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveRequestIdToNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('requests', function (Blueprint $table) {
            $table->dropColumn('request_id');
        });
        Schema::table('requestnotes', function (Blueprint $table) {
            $table->integer('request_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requestnotes', function (Blueprint $table) {
            //
        });
    }
}
