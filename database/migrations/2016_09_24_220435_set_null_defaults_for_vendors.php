<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullDefaultsForVendors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors', function (Blueprint $table) {

            $table->string('name')->nullable()->change();
            $table->string('address')->nullable()->change();
            $table->string('city')->nullable()->change();
            $table->string('state')->nullable()->change();
            $table->string('zip')->nullable()->change();
            $table->longText('description')->nullable()->change();
            $table->string('phone')->nullable()->change();
            $table->string('website')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->string('business_contact')->nullable()->change();
            $table->string('business_contact_title')->nullable()->change();
            $table->string('stripe_customer_id')->nullable()->change();
            $table->integer('price_range_low')->nullable()->change();
            $table->integer('price_range_high')->nullable()->change();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
