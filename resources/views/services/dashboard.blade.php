

@extends ('layout')

@section ('title')
  Vendor Dashbard
@stop

@section ('content')

<div class="">
	<div class="container-fluid">
		<div class="row">

			<div class="col-lg-9"><!--mainarea-->
				<h3>Requests for service
					@if(!$expired)
						<a href='/services/dashboard/expired' class='btn btn-default '>View Expired/Completed Requests</a>
					@else
						<a href='/services/dashboard' class='btn btn-default '>View Current Requests</a>
					@endif</h3>
					@foreach ( $all_requests as $r )
					<div class="serviceReqItem">
						<div class="row">
							<div class="col-sm-4 leftSideReqForService">

								<h3>{{ !empty($r->service->name) ? $r->service->name : '' }}</h3>
								<h4>Event ID: {{ !empty($r->request->event_id) ? $r->request->event_id : '' }}</h4><!--shows event id-->
								<p>This Requests ID: {{ !empty($r->request->id) ? $r->request->id : '' }}</p><!--shows req id-->
								<p>Service Location: @if( !empty($r->request->address) ) <a href='https://www.google.com/maps/place/{{  str_replace(' ', '+', $r->request->address . " " . $r->request->city . " " . $r->request->state . " " . $r->request->zip) }}' target='_blank'>{{ $r->request->address . ", " . $r->request->city . ", " . $r->request->state . " " . $r->request->zip }}</a> @else <div class="alert alert-danger">No Location Given</div> @endif </p>
								<p class='{{request_text($r->request->status) }}'>Service Status: {{ !empty($r->request->status) ? request_text($r->request->status) : '' }}</p>
								<p class='{{ (int)$r->request->paid === 1 || (int) $r->request->status === 10 || (int) $r->request->status === 17  ? 'Paid' : 'Unpaid' }}'>Planner Payment Status: {{ (int)$r->request->paid === 1 || (int) $r->request->status === 10 || (int) $r->request->status === 17 ? 'Paid' : 'Unpaid' }}</p>
								@if(!empty($r->request->price_diff))
									<div class='panel'>
										<div class="panel-heading">
											Price:
										</div>
										<div class="panel-body text-right">
											<p><strong>Total:</strong> ${{ !empty($r->request->total) ? $r->request->total/100 : '' }}</p>
											<p><strong>Price Adjustment:</strong> {!! $r->request->price_diff < 0 ? '<span class="alert-danger">-$' . ($r->request->price_diff * -1)/100 . "</span>" : '<span class="alert-success">+$' . $r->request->price_diff/100 . "</span>" !!}</p>
										</div>
										<div class="panel-footer text-right">
											<p><strong> Final Total:</strong> ${{ !empty($r->request->total) ? ($r->request->total + $r->request->price_diff)/100 : '' }}</p>
										</div>
									</div>
								@else
									<p>Total: ${{ !empty($r->request->total) ? $r->request->total/100 : '' }}</p>
								@endif

								<p>This Service Start Time: {{ !empty($r->request->time_start) ? date("F j, Y, g:i a", $r->request->time_start) : '' }}</p>
								<p>This Service End Time: {{ !empty($r->request->time_stop) ? date("F j, Y, g:i a", $r->request->time_stop) : '' }}</p>
							</div>

							<div class="col-sm-4">
								<!--bof notes -->
							<div class=" serviceRequestNotesList">


								@if(!empty($r->request->notes))

								<h4>Notes:</h4>
									<div class='requestNotes'>
										@foreach($r->request->notes as $note)
											<div class="notesArea">
												<div>
													<strong>{{$note->author_name}}:</strong>
												</div>
												<div>
													{{$note->note}}
												</div>

											</div>
										@endforeach
									</div>

								@endif
								<div class="form-group">
								<textarea placeholder="Reply" rows="6" class='form-control serviceRequestNotesAdd' data-id="{{$r->request->id}}"></textarea>
								</div>
								<button class='btn btn-block serviceRequestNotesAddButton' data-id="{{$r->request->id}}">Add Note</button>
							</div>
							<!--eof notes-->
							</div>
							<div class="col-sm-4 rightSideReqForService">
								<h4>Extras:</h4>
								@if(!empty($r->request->extras))



									@foreach($r->request->extras as $ex)
										<div class="extraWell">
											<div>
												<strong>{{ !empty($ex->name) ? $ex->name : ""}}</strong> - ${{ !empty($ex->price) ? $ex->price/100 : ""}}
											</div>
											<div>
												{{ !empty($ex->description) ? $ex->description : ""}}
											</div>

										</div>
									@endforeach

								@endif

								<!-- the modify request output -->

								@if(!empty($r->request->price_diff))
									<div class="extraWell">
										<p><strong>Modification to Original Price: </strong> <span class='h3'>{{ $r->request->price_diff < 0 ? '-$' . ($r->request->price_diff * -1)/100 : '$' . $r->request->price_diff/100 }}</span></p>
										<p><strong>Modification Description: </strong> <p>{{$r->request->price_diff_description}}</p></p>
									</div>
								@endif


							</div>




						</div><!--row-->

						<div class="row">


						@if( (int) $r->request->status === 1 || (int) $r->request->status === 3 || (int) $r->request->status === 4 || (int) $r->request->status === 6 || (int) $r->request->status === 7 || (int) $r->request->status === 8)

						<ul class="serviceReqButtons">
						<li>
						<form action="/requests/{{$r->request->id}}" method="POST">
							<input type="hidden" name="status" value="{{ request_status_encoder($user, 9) }}" />
							<input type="hidden" name="_token" value="{{ csrf_token() }}" >
							<button class='btn btn-success btn-xs loaderOnClick'>Accept</button>
						</form>
						</li>
						<li>
						<form action="/requests/{{$r->request->id}}" method="POST">
							<input type="hidden" name="status" value="{{ request_status_encoder($user, 2) }}" />
							<input type="hidden" name="_token" value="{{ csrf_token() }}" >
							<button class='btn btn-danger btn-xs loaderOnClick'>Decline</button>
						</form>
						</li>
						<li>
							<input type="hidden" name="status" value="{{ request_status_encoder($user, 3) }}" />
							<button class='btn btn-info btn-xs vendorRequestUpdate' data-id="{{$r->request->id}}">Modify Original Price</button>
						</li>
						<!-- <li>
						<form action="/requests/{{$r->request->id}}" method="POST">
							<input type="hidden" name="status" value="{{ request_status_encoder($user, 6) }}" />
						</form>
						</li> -->
						</ul>
						@elseif(array_search( $r->request->status, [2, 5, 11, 12, 13, 14, 15, 16]) === false)
						<ul class="serviceReqButtons">
						<li>
						<form action="/requests/{{$r->request->id}}" method="POST">
							<input type="hidden" name="status" value="{{ request_status_encoder($user, 13) }}" />
							<input type="hidden" name="_token" value="{{ csrf_token() }}" >
							<button class='btn btn-danger loaderOnClick'>Cancel</button>
						</form>
						</li>
						</ul>
						@else
						<!-- <ul class="serviceReqButtons">
						<li>
						<form action="/requests/{{$r->request->id}}/destroy" method="POST">
							<input type="hidden" name="status" value="{{ request_status_encoder($user, 13) }}" />
							<button class='btn btn-danger vendorRequestUpdate'>Cancel</button>
						</form>
						</li>
						</ul> -->
						@endif

						</div><!--row-->
					</div><!--sidebar-->
					@endforeach

				</div><!--main area-->
			<div class="col-lg-3 "><!--right sidebar-->

				@include('partials.vendor-services-list')


			</div>

		</div><!--row-->
	</div><!--container-->
</div><!--wrapper-->

<div id="priceDiffModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">

        <h2 class="modal-title  text-center">Modify Original Price</h2>

      </div>
      <div class="modal-body">
      	<h3 class='modal-price-diff-message'></h3>
      	<div class="form-group">
	      	<label>To reduce original price, use a negative number</label>
	      	<input type='text' class='form-control modal-price-diff' placeholder="$">
      	</div>
        <div class="form-group">
	        <label>Reason for modifying:</label>
	        <textarea type='text' class='form-control modal-price-diff-description' placeholder="Why this added cost or discount"></textarea>
        </div>
      </div>
  		<div class="modal-footer">
  			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  			<button class='btn btn-success priceDiffSave'>Save</a>
		</div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

@stop

