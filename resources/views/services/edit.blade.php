@extends ('layout')
@section ('title')
Edit Service Form
@stop
@section ('content')
@if(!empty($result))
@if($result->success)
<div class='alert alert-success'>Your service was saved!  <a href='/services/{{ $service->id }}'>View it here.</a></div>
@else
<div class='alert alert-danger'>Your service was not saved.  We have encountered an error. You may want to try again.</div>
@endif
@endif
<form id="editServices" method="POST" action="/services/{{$services->id}}" enctype="multipart/form-data">
  <input name="_method" type="hidden" value="PUT">
  <div class="row">
    <div class="col-sm-6 col-sm-offset-3">
      <div class="">
        <h3>Edit a Service</h3>
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label>Give your Service a Title</label>
              <input name="editServiceTitle" type="text" class="form-control" id="editServiceTitle" value="{{ !empty($services->name) ? $services->name : '' }}" required>
            </div>
          </div>
        </div>
      </div>
      <div clas="row">
        <div class="form-group col-sm-6">
          <label>Pick a Service Category</label>
          <select name ="editSelectServiceCategory" id="EventServiceCat" class="mainCategoriesSelect selectpicker form-control" required>
            <option value="" disabled {{ empty($services->category) ? 'selected' : '' }}>Select a Category</option>
            @foreach($categories as $category)
            <option value="{{$category->id}}" {{ (int) $category->id === (int) $services->category ? 'selected' : '' }}>{{$category->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group col-sm-6">
          <label>Pick a Service Sub-category</label>
          <select name ="editSelectServiceSubcat" id="EventServiceSubcat" class="subCategoriesSelect selectpicker form-control" required>
            @if(count($subcategories) > 0)
            <option value="" disabled {{ empty($services->subcategory) ? 'selected' : '' }}>Select a Sub Category</option>
            @foreach($subcategories as $subcategory)
            <option value="{{$subcategory->id}}" {{ (int) $subcategory->id === (int) $services->subcategory ? 'selected' : '' }}>{{$subcategory->name }}</option>
            @endforeach
            @else
            <option value="-1" selected>None</option>
            @endif
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8">
          <div class="form-group">
            <label>Describe this service listing in detail</label>
            <textarea name="editServiceDescription" id="editServiceDescription" class="form-control" rows="10" required>{{ !empty($services->description) ? $services->description : '' }}</textarea>
          </div>
        </div>
        <div class="form-group">
                <label>Keywords</label>
                <input type='text' id='serviceTags' name='serviceTags' />
                <script>
                  (function($){
                    window.inputAddTag = function(e){
                      var currentTags = window.formatTags($('#serviceTags').val());
                      $('#serviceTags').importTags(currentTags);
                      var tags = currentTags.split(',');
                      if(tags.length > 4){
                        $('#serviceTags').importTags('');
                        $('#serviceTags').importTags([tags[0],tags[1],tags[2],tags[3]].join());
                        $("#serviceTags_tag").focus();
                      }
                    };
                    $('#serviceTags').tagsInput({
                      'height':'100%',
                      'width':'100%',
                      'onAddTag':
                        window.inputAddTag,
                    });
                  @if(!empty($services->_tags))
                    $('#serviceTags').importTags('{{ implode( ",", $services->_tags ) }}');
                  @endif
                  })(jQuery);
                </script>
        </div>
        <div class="col-sm-4">
          <div class="form-group well">
            <label>Image</label>
            <div class="uploadThumbnail">{!! !empty($services->image) ? "<img src='" . env('CDN_URL', 'http://cdn.todcandev.com/eventpeace') . "/services/".$services->image."' />" : '' !!}
            </div>
            <input name="servicePic" type="file" class="" id="servicePic" placeholder="Service Image">
          </div>
        </div>
      </div>
      <!--end well 1-->
      <hr>
      <div class="">
        <h3>Scope & Price</h3>
        <div class="col-sm-3">
          <div class="form-group">
            <label>Allow Autoaccepted Booking</label>
            <input name="auto_accept" type="checkbox" class="form-control" id="auto_accept" value='1' {{ !empty($services->auto_accept) ? 'checked' : '' }}>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label>Price for this service</label>
              <input name="editServicePrice" type="text" class="form-control" id="editServicePrice" value="{{ !empty($services->price) ? $services->price/100 : '' }}" required>
            </div>
            <div class="form-group">
              <label>Price details</label>
              <input name="editServicePriceDetails" type="text" class="form-control" id="editServicePriceDetails" value="{{ !empty($services->price_details) ? $services->price_details : '' }}" required>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="well">
              <div class="form-group">
                <label>Pricing Type</label>
                <br><input name="servicePriceType" type="radio"  value='0' {{ !empty($services->price_type) && (int) $services->price_type === 0 ? 'checked' : '' }} required> Allow Buyer to select Quantity
                <br><input name="servicePriceType" type="radio"  value='1' {{ !empty($services->price_type) && (int) $services->price_type === 1 || empty($services->price_type) ? 'checked' : '' }} required> Offer Only One
              </div>
            </div>
          </div>
        </div>
        <div id="services-add-extras-box" class="form-group">
          @foreach($extras as $extra)
          <div class="greenBox form-group services-extras">
            <a class='deleteThis deleteExtra' data-id='{{$extra->id}}'></a>
            <h2 class="pull-right">{{$extra->name}}</h2>
            <h3 class="pull-left">Extra</h3>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-sm-4">
                <label>Name of this Extra</label>
                <input type="text" class="form-control services-extras-name" name="extraname[]" placeholder="Name" value="{{$extra->name}}" required />
              </div>
              <div class="col-sm-4">
                <label>List the additional price</label>
                <input type="text" class="form-control services-extras-price" name="extraprice[]" placeholder="Extra Price" value="{{$extra->price/100}}" required />
              </div>
              <div class="col-sm-4">
                <label>Additional details</label>
                <input type="text" class="form-control services-extras-details" name="extradetails[]" placeholder="Details" value="{{$extra->description}}" required />
              </div>
            </div>
            <input type="hidden" name="extraid[]" value="{{$extra->id}}" />
            <input type='hidden' name='extrafield[]' value="true" />
          </div>
          @endforeach
        </div>
        <button id="services-add-extras-button" type="button" class="btn btn-success btn-round">+</button> Add Extra
        </div><!-- eof well 2-->
        <hr>

          <h3>Shipping & Delivery</h3>
          <div class="form-group">
            <label>Shipping or delivery cost ($)</label>
            <input name="editDeliveryCharge" type="number" class="form-control" id="editServiceDeliveryCharge" value="{{ !empty($services->delivery_charge) ? $services->delivery_charge/100 : 0 }}" required>
          </div>
          <div class="form-group">
            <label>Shipping or delivery time frame</label>
            <input name="editDeliveryTime" type="text" class="form-control" id="editServicedeliveryTime" value="{{ !empty($services->delivery_time) ? $services->delivery_time : '' }}" required>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label>Google Calendar Code</label>
                <textarea name="calendarCode" class="form-control" id="inputCalendarCode" placeholder="Input Google Calendar code here..." >{!! !empty($services->calendar_code) ? $services->calendar_code : '' !!}</textarea>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Your detailed Terms and Conditions</label>
            <textarea name="editTermsAndConditions" id="editTermsAndConditions" class="form-control" rows="10" required>{{ !empty($services->terms_conditions) ? $services->terms_conditions : '' }}</textarea>
          </div>

          <input type="submit" class="btn btn-ltGreen btn-block" value="Submit" >

          <input type="hidden" name="_token" value="{{ csrf_token() }}">

        </form>

          <form method='POST' action='/services/{{ !empty($services->id) ? $services->id : 0 }}'>
          {{ method_field('DELETE') }}
          <input type="hidden" name="_token" value="{{ csrf_token() }}" >
          <button style="margin-top:15px" class='btn btn-danger btn-block'>Delete This Listing</button>
          </form>
        </div><!--eof well 3-->
        </div><!--eof col-sm-6 col-sm-offset-3-->
        </div><!--eof row-->
        @stop