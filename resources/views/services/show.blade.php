<!-- Shows the Vendor Service Listing-->

@extends ('layout')

@section ('title')
  {{ !empty($service->name) ? $service->name : '' }} Service
@stop
<div class="container">
@section ('content')

@include ('services.show_backbone')
</div>
@stop