<div class="container-fluid">

	<div class="showBlock">
		<h2 class="vendorIcon serviceTitle">{{ !empty($service->name) ? $service->name : '' }}
		@if(!empty($user) && $user->id === $service->vendor_id)
		<a class="editThis" href="/services/{{$service->id}}/edit"></a></h2>
		@endif
		<h5 class="text-right">Category: {{ !empty($category->name) ? $category->name : '' }} > {{ !empty($subcategory->name) ? $subcategory->name : '' }}</h5>
		<hr>
		<div class="row">
			<p class="displayLabel">Price: ${{ !empty($service->price) ? $service->price/100 : '' }} - {{ !empty($service->price_details) ? $service->price_details : '' }}</p>
		</div>

		<div class="row">
			<p class="displayLabel">Delivery: ${{ !empty($service->delivery_charge) ? $service->delivery_charge/100 : '' }} - {{ !empty($service->delivery_time) ? $service->delivery_time : '' }}</p>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-4">
				<div class="showBlock">


						 {!! !empty($service->image) ? "<img class='img-responsive' src='" . env('CDN_URL', 'http://cdn.todcandev.com/eventpeace') . "/services/".$service->image."' />" : "<img class='img-responsive' src='http://cdn.todcandev.com/eventpeace/noimage.jpg' />" !!}

				</div>
			</div>

			<div class="col-sm-8">
				<div class="">
					<p class="displayLabel">Service Description:</p>
					<div class="serviceDescription">
					{{ !empty($service->description) ? $service->description : '' }}
					</div>
				</div>

			</div>

		</div>



	<div class="calendarSection">
		<div class="col-xs-12">
			@if(!empty($calendar))
				<?php $morerand = rand(0, 9999999); ?>

					<button type="button" data-target="#availabilityButton{{$service->id.$morerand}}" data-toggle="collapse" class="btn btn-primary btn-lg calAvailability" >
					  View availability
					</button>
					<div class="collapse" id="availabilityButton{{$service->id.$morerand}}">
						<div class="cal-wrapper">
							<iframe src='https://calendar.google.com/calendar/embed?{{$calendar}}' style='border: 0'  frameborder='0' scrolling='no'></iframe>
						</div>
					</div>
			@endif
		</div>
	</div>





	<div class="showBlock">
		<p class="displayLabel">Extras:</p>
		<div class="">
		@if(!empty($extras))

			@foreach($extras as $extra)

				<div class="extras col-sm-4">
					<div class="extraWrapper">
						<div class="displayLabel">
							<div class="row">
								<div class="col-sm-7">
									<h4>{{$extra->name}}</h4>
								</div>
								<div class="col-sm-5">
									<h2 class="priceBlock">${{$extra->price/100}}</h2>
								</div>
							</div>
						</div>
						<hr>

						<div class="displayLabel"><strong>Extra Details:</strong><br> {{$extra->description}}</div>
					</div>
				</div>

			@endforeach

		@endif
		</div>
		<div class="clearfix"></div>
		<hr>

		<div class="showBlock">
			<p class="displayLabel">Terms & Conditions:</p>

			{{ !empty($service->terms_conditions) ? $service->terms_conditions : '' }}

	</div>

	</div>



</div><!--eof container-->