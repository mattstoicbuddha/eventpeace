@extends ('layout')
@section ('title')
Create Service Form
@stop
@section ('content')
@if(!empty($result))
@if(session('failed'))
<div class='alert alert-danger'>Your service was not saved.  Our technicians are investigating the issue.</div>
@endif
@endif
<form id="createServices" method="POST" action="/services" enctype="multipart/form-data"> <!-- Save Service route, kinda weird that it has no tail -->
<div class="row">
  <div class="col-sm-6 col-sm-offset-3">
    <h3>List a Service <a class="btn btn-info btn-sm pull-right" href="/vendors/tips">Listing Tips</a></h3>
    <hr>
    <div class="row">
      <div class="col-sm-8">
        <div class="form-group">
          <label>Give your Service a Title</label>
          <input name="serviceTitle" type="text" class="form-control" id="inputServiceTitle" placeholder="Service Title" required>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label>Image</label>
          <input name="servicePic" type="file" id="servicePic" placeholder="Service Image">
        </div>
      </div>
    </div>
    <div class="form-group col-sm-6">
      <label>Pick a Service Category</label>
      <select name ="selectServiceCategory" id="EventServiceCat" class="mainCategoriesSelect selectpicker form-control" required>
        <option value="" disabled selected>Select a Category</option>
        @foreach($categories as $category)
        <option value= "{{$category->id}}">{{$category->name}}</option>
        @endforeach
      </select>
    </div>
    <div class="form-group col-sm-6">
      <label>Pick a Service Sub-category</label>
      <select name="selectServiceSubcat" id="EventServiceSubcat" class="subCategoriesSelect selectpicker form-control" required>
        <option value="" disabled selected>Select a Sub Category</option>
      </select>
    </div>
    <div class="form-group">
      <label>Describe this listing in detail<span class="pls-help" data-toggle="popover" data-placement="right" title="Write a complete and detailed description" data-content="A detailed description is important. Make sure you list as much information as is relevant so planners can make a sound decision. If you require a minimum or have any certain restrictions, they should be clearly listed here." data-placement="top"></span></label>
      <textarea name="serviceDescription" id="serviceDescription" placeholder="Description" class="form-control" rows="10" required></textarea>
    </div>
    <hr>
    <div class="form-group">
      <label>Keywords<span class="pls-help" data-toggle="popover" data-placement="right" title="Use up to 4 Keywords to add very specific terms to your listing" data-content="Keywords allow you to use specific terms like 'kids' or 'vegetarian' to your listing. Avoid general terms like 'holiday' or 'fun'. Also avoid terms already used by categories and sub-categories. Keywords can bring specialized searches directly to your listing. For example, if you offer spanish speaking bartenders, you may widh to use the Keywords 'espanol'. " data-placement="top"></span></label>
      <input type='text' id='serviceTags' name='serviceTags' />
      <script>
          (function($){
            window.inputAddTag = function(e){
              var currentTags = window.formatTags($('#serviceTags').val());
              $('#serviceTags').importTags(currentTags);
              var tags = currentTags.split(',');
              if(tags.length > 4){
                $('#serviceTags').importTags('');
                $('#serviceTags').importTags([tags[0],tags[1],tags[2],tags[3]].join());
              }
              $("#serviceTags_tag").focus();
            };
            $('#serviceTags').tagsInput({
              'height':'100%',
              'width':'100%',
              'onAddTag':
                window.inputAddTag,
            });
          })(jQuery);
        </script>
    </div>
    <div class="row">
      <hr>
      <div class="col-sm-4">
      <label>Service Price</label>
        <div class="input-group">
          <span class="input-group-addon">$</span>
            <input name="servicePrice" type="text" class="form-control" id="inputServicePrice" placeholder="Service Price" required>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label>Price Unit <span class="pls-help" data-toggle="popover" data-placement="right" title="The unit your price refers to" data-content="The Pricing unit may be expressed as 'Hours' or 'servings' or 'pieces' or any term you want to use to express the unit your price refers to." data-placement="left"></span></label>
          <input name="servicePriceDetails" type="text" class="form-control" id="inputServicePriceDetails" placeholder="eg: 'per Hour', 'per Item'" required>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label>Pricing Type<span class="pls-help" data-toggle="popover" data-placement="right" title="Sell by Quantity?" data-content="You can choose to allow buyers to select a quantity for this listing. If you are offering a product such as 'Candles', you may wish to allow planners to buy a quantity they choose. If you have a listing with only one item you will likley want to select 'sell only one' and not allow buyes to select a quantity." data-placement="top"></span></label><br>
          <input name="servicePriceType" type="radio"  value='0' required> Buyer can select Quantity<br>
          <input name="servicePriceType" type="radio"  value='1' required> Sell Only One
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-sm-8">
        <div class="form-group">
          <label>Add an Extra <span class="pls-help" data-toggle="popover" data-placement="right" title="Extras are used for listing options" data-content="If you offer 'BAGELS' you may wish to offer some extras such as 'CREAM CHEESE' or 'BUTTER'. If you provide a venue, you may wish to add a 'PROJECTOR' or 'PA SYSTEM' as an extra. Extras go along with the listing, and have an additional cost. Extras are not items that are sold by themselves." data-placement="left"></span></label>
          <div id="services-add-extras-box" class="form-group">
            <!--from main.js-->
          </div>
          <button id="services-add-extras-button" type="button" class="btn btn-info btn-round">ADD</button>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label>Auto accept Booking <span class="pls-help" data-toggle="popover" data-placement="left" title="Do not check this option unless you are certain!" data-content="If this box is checked it means you will not have the opportunity to respond to a booking request before it is booked. Checking this box means you will allow planners to book your service without your prior approval. If you arent sure you should leave this box un-checked so you can approve all booking requests."></span></label>
          <input name="auto_accept" type="checkbox" class="" id="auto_accept" value='1'>
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <label>Shipping or delivery cost</label>
          <input name="deliveryCharge" type="text" class="form-control" id="inputServiceDeliveryCharge" placeholder="Delivery Charge" required>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <label>Shipping or delivery time frame</label>
          <input name="deliveryTime" type="text" class="form-control" id="inputServicedeliveryTime" placeholder="Delivery Time" required>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <label>Google Calendar Code</label>
          <textarea name="calendarCode" class="form-control" id="inputCalendarCode" placeholder="Input Google Calendar code here..." ></textarea>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label>Your detailed Terms and Conditions<span class="pls-help" data-toggle="popover" data-placement="right" title="Your Terms" data-content="This area can be used to describe any additional terms or conditions you may have for this listing." data-placement="left"></span></label>
      <textarea name="termsAndConditions" placeholder="Terms & Conditions" class="form-control" rows="10" required></textarea>
    </div>
    <input type="submit" class="btn btn-success btn-lg" value="submit" >
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </div><!--eof col-sm-6 col-sm-offset-3-->
    </div><!--eof row-->
  </form>
  <script type="text/javascript">
  $(function () {
  $('[data-toggle="popover"]').popover()
  })
  </script>
  @stop