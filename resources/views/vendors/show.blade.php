@extends ('layout')

@section ('title')
  Vendor Show
@stop

@section ('content')
<!-- $vendor = Vendor object
     $user = User object -->

<div class="container">

	<h1>{{ !empty($vendor->name) ? $vendor->name : '' }}</h1>
	<div class="row">

		<div class="col-sm-6 leftCol"><!--the left col-->

			<div class="row"><!--pic and map-->
				<div class="col-sm-6">
				 	<a href="#"><img class="img-responsive" src="{{ !empty($vendor->profile_pic) ? env('CDN_URL', 'http://cdn.todcandev.com/eventpeace') . '/'.$vendor->profile_pic : env('CDN_URL', 'http://cdn.todcandev.com/eventpeace') . '/noimage.jpg' }}"></a>
				</div>
				<div class="col-sm-6">
					<div id="map" class="map-wrapper"><!--js puts the map in here--></div>

					    <script>
					      var map;
					      function initMap() {
					        map = new google.maps.Map(document.getElementById('map'), {
					          center: {lat: {{ !empty($geoloc->lat) ? $geoloc->lat : '' }}, lng: {{ !empty($geoloc->lng) ? $geoloc->lng : ''}} },
					          zoom: 12
					        });

					        var marker = new google.maps.Marker({
							    position: {lat: {{ !empty($geoloc->lat) ? $geoloc->lat : '' }}, lng: {{ !empty($geoloc->lng) ? $geoloc->lng : ''}} },
							    map: map,
							    title: "{{ !empty($vendor->name) ? $vendor->name : '' }}"
							  });
					      }
					    </script>
					    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASfFNUH52i82R6uReEsn4qF6RbRXx-oVQ&callback=initMap"
					    async defer></script>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4">
					<button>contact</button>
				</div>
				<div class="col-sm-8">
					{{ !empty($vendor->business_contact) ? $vendor->business_contact : '' }} - {{ !empty($vendor->business_contact_title) ? $vendor->business_contact_title : '' }} at {{ !empty($vendor->name) ? $vendor->name : '' }}
					<p>City: {{ !empty($vendor->city) ? $vendor->city : '' }}, State: {{ !empty($vendor->state) ? $vendor->state : '' }}</p>

				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h3>About {{ !empty($vendor->name) ? $vendor->name : '' }}</h3>
					<p>{{ !empty($vendor->description) ? $vendor->description : '' }}</p>
				</div>
			</div>

		</div>

		<div class="col-sm-6 rightCol"><!--the right col-->
			<div class="well">
			<h3>Services</h3>
			@foreach($services as $service)
			<ul>
			<li class="verticalList vendorIcon"> {{$service->name}} </li>
			</ul>
			@endforeach
		</div>
		<div class="row">
			<div class="col-sm-8">

			</div>
			<div class="col-sm-4">
				<!-- favorite checkbox-->
			</div>
		</div>



		</div>

	</div>






@stop