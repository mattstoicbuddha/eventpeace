@extends ('layout')

@section ('title')
  Vendor Tips
@stop

@section ('content')


<div class="container">
<h3>A few things to think about when writing a vendor listing as it’s helpful to be as specific as possible</h3>
<ul>
<li>Is there a minimum number of guests required?  Maximum allowed?</li>

<li>Is there a minimum amount a host/hostess is required to spend?  If yes, please state the amount:</li>

<li>Do you require a deposit?</li>

<li>Is there a legal form to sign?  If yes, please attach below.</li>

<li>Are you licensed and insured (if applicable)?</li>

<li>Do you offer a private event room (venues only)?</li>

<li>Do you have an outside seating area?  Covered or not covered (venues only)?</li>

<li>If so, is it wheelchair accessible (if there are more than one rooms available, please specify which ones are accessible)?</li>

<li>Do you have photos of your venue/food/services that we can use?  If yes, please attach below.</li>

<li>Do you offer delivery services?</li>

<li>Any special packages you would like to offer?</li>
</ul>
<p><strong>Any restrictions we should know about?  For example:</strong></p>
<ul>
<li>No fire</li>
<li>No animals</li>
<li>No alcohol</li>
<li>No outside food, drinks, and/or decorations</li>
<li>No outside dj/band</li>
<li>Only specific food or drinks allowed</li>
<li>Only 18 and over allowed</li>
<li>Only 21 and over allowed</li>
<li>Minimum/maximum time limit</li>
</ul>

<p><strong>Any special accommodations we should know about for your venue?  For example:</strong></p>
<ul>
<li>Audio/visual equipment available</li>
<li>Wi-Fi available</li>
<li>Kid-friendly</li>
<li>Staff available</li>
<li>If a restaurant, bakery, catering, or food truck, do you offer:</li>
<li>Gluten free</li>
<li>Dairy free</li>
<li>Vegetarian</li>
<li>Vegan</li>
<li>Specialty items (please let us know):</li>
</ul>

<hr>
<p><strong>Adding Google Calendar Code to your listing</strong></p>
<ol>
<li>On a computer, open Google Calendar. You can only get the code to embed in your website from a computer, not the Google Calendar app.</li>
<li>In the top right, click Settings.</li>
<li>Click the Calendars tab.</li>
<li>Click the name of the calendar you want to embed.</li>
<li>In the "Embed This Calendar" section, copy the iframe code displayed.</li>
<li>When making a listing, paste this code in the section titled "Google Calendar Code" to allow your calendar to display on the listing.</li>
<li>Your embedded calendar will only be visible if you have it set to "public" in your Google calendar settings.</li>
</ol>


<hr>
<a class="btn btn-info" href="/services/create">Create a Listing</a>


</div>


@stop