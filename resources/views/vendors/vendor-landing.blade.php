@extends ('layout')

@section ('title')
  Become a Vendor
@stop

@section ('content')


<div class="container">
	<h3>All the reasons vendors and event planners should join EventPeace</h3>

	<div class="faqList">
		<ul>
		<li><strong>Why should I use EventPeace?</strong>  We’re here to save you time and money!  You will no longer have to answer multiple phone calls regarding availability, cost, range of services, etc.  This is also a great marketing tool as planners will view you on our website - whether they request a service now or for another event in the future.
</li>

		<li><strong>How much does it cost to use your services?</strong>  It is free to register, respond to requests for services, and communicate with planners. EventPeace only charges a small fee when a planner books you for an event, normally 15%.</li>

		<li><strong>How do I get paid?</strong>  The planner pays the full amount upfront once they have booked your services.  The money will be released directly to your bank account 24 hours after the event.
</li>

		<li><strong>How do I get a deposit?</strong>  Typically, security deposits are no longer necessary since the organizer pays the full amount upfront. If you require a deposit upfront, please state so in your detailed Terms and Conditions when listing a service.</li>

		<li><strong>How do I know when my services are requested?</strong>  You will receive an email notification letting you know that you have a new request.
</li>

		<li><strong>What happens if the event organizer cancels after accepting the offer?</strong>  Please state your cancellation terms in your detailed Terms and Conditions when listing a service. We will hold our planners accountable to those terms.</li>

		<li><strong>When should I respond to a new request for service?</strong>  You are required to either accept or decline within 24 hours after receiving a request.</li>

		<li><strong>How do I communicate with a planner?</strong>  On your Vendor Dashboard, you can message planners directly and vice versa.  You will receive an email notification letting you know you have a message from a planner.</li>

		<li><strong>Can I modify my prices if need be?</strong>  Yes, you can modify your original price on your Vendor Dashboard at any time, or modify an individual service request price before you accept an offer if needed.
</li>

		<li><strong>Can I offer more than one service?</strong>  Yes, you can list as many services as you offer as long as they’re relevant to events.</li>

		<li><strong>Who should I contact if a problem should arise?</strong>  You can contact EventPeace at support@eventpeace.com.</li>
		</ul>
	</div>
	<a class="btn btn-success" href="/users/create">Register now</a>
	<a class="btn btn-info" href="/vendors/tips">Listing Tips</a>

</div>
@stop