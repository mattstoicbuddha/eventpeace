<!-- This will be used for the vendor profile page-->

@extends ('layout')

@section ('title')
  Create Service Form
@stop

@section ('content')
<div class="row">
  <div class="col-sm-6 col-sm-offset-3">

	<form name="vendorProfile" id="vendorProfile">
	  <div class="form-group">
	    <input name="vendorName" type="text" class="form-control" id="vendorName" placeholder="Vendor Name">
	  </div>

	  <div class="form-group">
	    <input name="vendorContactPerson" type="text" class="form-control" id="vendorContactPerson" placeholder="Vendor contact person">
	  </div>

	  <div class="form-group">
	    <input name="vendorLocation" type="text" class="form-control" id="vendorLocation" placeholder="Vendor location">
	  </div>

	  <div class="form-group">
        <textarea name="vendorDescription" id="vendorDescription" placeholder="Vendor Description" class="form-control" rows="10"></textarea>
      </div>

	  <div class="form-group"><!--image upload for 1 image only-->
	    <input name="vendorImage" type="file" class="form-control" id="vendorImage" placeholder="Vendor location">
	  </div>

	  <input type="submit" class="btn btn-ltGreen btn-block" value="Submit">

	</form>

  </div><!--eof col-sm-6 col-sm-offset-3-->
</div><!--eof row-->

@stop