<!-- a blank page layout-->

@extends ('layout')

@section ('title')
  Edit Vendors Profile<!-- change this-->
@stop

@section ('content')
<div class="container-fluid">
	<div class="row">
  		<div class="col-sm-8">
			<h3>Your Vendor Profile</h3>
			<form id="vendorsEdit" method="POST" action="/vendors/{{$vendors->id}}" enctype="multipart/form-data">

				<div class="row">
					<div class="col-sm-6">
					  <div class="form-group">
					  <label>Company Name</label>
					    <input value="{{ !empty($vendors->name) ? $vendors->name : '' }}" name="vendorsName" type="text" class="form-control" id="vendorsName" >
					  </div>
					</div>
					<div class="col-sm-6">
					  <div class="form-group">
					  <label>Street Address</label>
					    <input value="{{ !empty($vendors->address) ? $vendors->address : '' }}" name="vendorsAddress" type="text" class="form-control" id="vendorsAddress" >
					  </div>
					</div>
				</div>

				<div class="row">

					<div class="col-sm-6">
						<div class="form-group">
							<label>City</label>
					    	<input value="{{ !empty($vendors->city) ? $vendors->city : '' }}" name="vendorsCity" type="text" class="form-control" id="vendorsCity" >
						</div>
					</div>

					<div class="col-sm-2">
						<div class="form-group">
							<label>State</label>
						    <input value="{{ !empty($vendors->state) ? $vendors->state : '' }}" name="vendorsState" type="text" class="form-control" id="vendorsState" >
						</div>
					 </div>

					<div class="col-sm-4">
						<div class="form-group">
						  	<label>Zip Code</label>
						    <input value="{{ !empty($vendors->zip) ? $vendors->zip : '' }}" name="vendorsZip" type="text" class="form-control" id="vendorsZip" >
						</div>
					</div>

				</div>


				<div class="row">

					<div class="col-sm-4">
						<div class="form-group">
			  				<label>Phone Number</label>
			   				<input value="{{ !empty($vendors->phone) ? $vendors->phone : '' }}" name="vendorsPhone" type="text" class="form-control" id="vendorsPhone" >
			  			</div>
					</div>

					<div class="col-sm-4">
					  	<div class="form-group">
						  	<label>Website</label>
						    <input value="{{ !empty($vendors->website) ? $vendors->website : '' }}" name="vendorsWebsite" type="text" class="form-control" id="vendorsWebsite" >
					  	</div>
					</div>

					<div class="col-sm-4">
					  	<div class="form-group">
					  		<label>Contact Email</label>
					    	<input value="{{ !empty($vendors->email) ? $vendors->email : '' }}" name="vendorsEmail" type="text" class="form-control" id="vendorsEmail" >
					  	</div>
					</div>

				</div>

				<div class="row">

					<div class="col-sm-6">
					  	<div class="form-group">
					  		<label>Contact Person</label>
				    		<input value="{{ !empty($vendors->business_contact) ? $vendors->business_contact : '' }}" name="vendorsBusinessContact" type="text" class="form-control" id="vendorsBusinessContact" >
					  	</div>
					</div>

					<div class="col-sm-6">
			  			<div class="form-group">
						 	 <label>Contact Title</label>
						  	  <input value="{{ !empty($vendors->business_contact_title) ? $vendors->business_contact_title : '' }}" name="vendorsBusinessContactTitle" type="text" class="form-control" id="vendorsBusinessContactTitle" >
						</div>
					</div>

				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
			  				<label>Vendor Description</label>
			    			<textarea value="" name="vendorsDescription" type="text" class="form-control" id="vendorsDescription" rows="10">{{ !empty($vendors->description) ? $vendors->description : '' }}</textarea>
			  			</div>
		  			</div>
				</div>

				<div class="row">
					<div class="col-lg-4 col-md-6">
						<div class="form-group well">
							<label>Upload your Company image or logo</label>
							<input name="_method" type="hidden" value="PUT">
							<input name="profilePic" type="file" id="profilePic" >
						    {!! !empty($vendors->profile_pic) ? "<img class='img-responsive' src='" . env('CDN_URL', 'http://cdn.todcandev.com/eventpeace') . "/vendors/profile/".$vendors->profile_pic."?_ga=1.75009877.612256312.1460416098' />" : "<img class='img-responsive' src='" . env('CDN_URL', 'http://cdn.todcandev.com/eventpeace') . "/vendors/profile/noimage.jpg' " !!}>
						</div>
					</div>
				</div>

				<div class="row">

					<div class="col-sm-6">
					  	<div class="form-group">
					  		<label>New Password</label>
					  		{!!  session('result') && !session('password') ? "<div class='alert alert-danger'>Your passwords did not match.</div>" : session('result') && session('password') && (int)session('password') === 2 ? "<div class='alert alert-success'>Password successfully changed!</div>" : "" !!}
				    		<input name="newPassword" type="password" class="form-control" placeholder="New Password">
				    		<input name="newPassword2" type="password" class="form-control" placeholder="New Password Again">
					  	</div>
					</div>

				</div>

					<!-- 	<div class="col-sm-4">
						  <div class="form-group">
						  <label>Lowest Priced Listing</label>
						    <input value="{{ !empty($vendors->price_range_low) ? $vendors->price_range_low : '' }}" name="priceRangeLow" type="number" min="0" step="1" class="form-control" id="priceRangeLow" >
						  </div>
						</div>

						<div class="col-sm-4">
						  <div class="form-group">
						  <label>Highest price Listing</label>
						    <input value="{{ !empty($vendors->price_range_high) ? $vendors->price_range_high : '' }}" name="priceRangeHigh" type="number" min="0" step="1" class="form-control" id="priceRangeHigh" >
						  </div>
						</div> -->


	  			<input type="hidden" name="_token" value="{{ csrf_token() }}">
	  			<button class="btn btn-info">Update</button>
			</form>
		</div><!--eof left col-->
		<div class="col-sm-4">
			<div class="vendorCardWrapper">
				<div class="panel panel-default">
					<div class="panel-heading">Your cards</div>
					<div class="panel-body">

						<div id='thisholdscards' class="thisholdscards">
							<div class="row">
							@if(!empty($cards) && !empty($cards->data))

								@foreach($cards->data as $card)

									<form method="POST" action="/users/deletecard">
									<div class="eachCard">
										<div class='cardBrand col-xs-3 text-left'>{{ $card->brand }}</div>
										<div class='cardLast4  col-xs-6 text-right'>{{ $card->last4 }}</div>
										<div  class="col-xs-3">
											<button class='pull-right'>x</button>
										</div>
										<div class="clearfix"></div>
										<input type="hidden" name="card_id" value="{{ $card->id }}">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
									</div>
									</form>

								@endforeach

							@endif
							</div>
						</div>

					</div>

				</div>


				<form action="/users/addcard" method="POST" id="payment-form">
				  	<span class="payment-errors"></span>

				  	<div class="form-group">
				   		<label>Card Number</label>
				      	<input type="text" size="20" data-stripe="number" class="form-control">
				    </div>

				    <div class="row">

				    	<div class="col-xs-6">
						  	<div class="form-group">
							    <label>Expiration (MM)</label>
						     	<input type="text" size="2" data-stripe="exp_month" class="form-control">
						   	</div>
						</div>

						<div class="col-xs-6">
							<div class="form-group">
								<label>Expiration (YY)</label>
					    		<input type="text" size="2" data-stripe="exp_year" class="form-control">
			  				</div>
			  			</div>

			  		</div>

					<div class="row">

						<div class="col-xs-6">
							<div class="form-group">
							    <label>CVC</label>
						    	<input type="text" size="4" data-stripe="cvc" class="form-control">
							</div>
						</div>

						<div class="col-xs-6">
							<div class="form-group">
								<label> </label>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="submit" class="submit btn btn-success btn-block" value="Add Card">
							</div>
						</div>

					</div>

				</form>
			</div>
		</div><!--eof right col-->
	</div>
</div>


@include('stripe')

@stop