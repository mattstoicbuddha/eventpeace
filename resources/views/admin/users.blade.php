@extends ('layout')

@section ('title')
  Users
@stop

@section ('content')
<div class="container">
	@include ('partials.admin-nav')
		<div class="adminPayments panel panel-default">
			<div class="panel-heading">Manage Users</div>
			<a href="/admin/users/csv" class="btn pull-right" download>Export as CSV</a>
			<div class="panel-body">
				<table class="table table-hover table-responsive">

					<tr>
						<th>User id</th>
						<th>User email</th>
						<th class="text-center">User type</th>
						<th class="text-center">Action</th>
					</tr>

					@foreach($users as $user)
						<tr>
							<td>{{ $user->id }}</td>
							<td>{{ $user->email }}</td>
							<td class='{{ (int) $user->type === 1 ? "Vendor" : "Planner" }}'>{{ (int) $user->type === 1 ? "Vendor" : "Planner" }}</td>
							<td class="text-center">
								@if((int) $user->suspended === 0)
									<button class='btn btn-xs btn-warning suspendUser' data-id='{{ $user->id }}'>Suspend</button>
								@else
									<button class='btn btn-xs btn-danger unsuspendUser' data-id='{{ $user->id }}'>Unsuspend</button>
								@endif
							</td>
						</tr>
					@endforeach
				</table>

@stop