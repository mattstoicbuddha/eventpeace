<!--
Payments: Payments that are already paid out
Pending: Requests that have been paid and are waiting to be paid out
-->

@extends ('layout')

@section ('title')
  Payments
@stop

@section ('content')
	<div class="container">
 	@include ('partials.admin-nav')


		<div class="adminPending panel panel-warning">
			<div class="panel-heading">Pending Payments</div>
	 		<div class="panel-body">
				<table class="table table-striped table-hover table-responsive">

						<tr>
							<th>Request ID</th>
							<th>event_id</th>
							<th>service_id</th>
							<th>planner_id</th>
							<th>vendor_id</th>
							<th>payment total</th>
							<th>EP Amt.</th>
							<th>Vendor Amt.</th>
							<th>time_start</th>
							<th>time_stop</th>
							<th>Action</th>
						</tr>
						@foreach($pending as $pend)
						<tr>
							<td>{{ $pend->id }}</td>
							<td><a href='/events/{{ $pend->event_id }}'>{{ $pend->event_id }}</a></td>
							<td><a href='/services/{{ $pend->service_id }}'>{{ $pend->service_id }}</a></td>
							<td><a href='/planners/{{ $pend->planner_id }}'>{{ $pend->planner_id }}</a></td>
							<td><a href='/vendors/{{ $pend->vendor_id }}'>{{ $pend->vendor_id }}</a></td>
							<td>${{ $pend->total/100 }}</td>
							<td>${{ ($pend->total/100) * .1 }} </td><!-- EventPeace commisssion of 10%, may change?  -->
							<td>${{ ($pend->total/100) * .9 }} </td><!-- Vendor pay out 90%, may change?  -->
							<td>{{ date( 'm/d/Y h:m A', $pend->time_start ) }}</td>
							<td>{{ date( 'm/d/Y h:m A', $pend->time_stop ) }}</td>
							<td><button class='btn btn-xs btn-success btn-outline payoutPending' data-id='{{ $pend->id }}'>Mark Paid Out</button></td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>

		<div class="adminPayments panel panel-success">
			<div class="panel-heading">Completed Payments</div>
			<div class="panel-body">
				<table class="table table-striped table-hover table-responsive">

						<tr>
							<th>Request ID</th>
							<th>event_id</th>
							<th>service_id</th>
							<th>planner_id</th>
							<th>vendor_id</th>
							<th>payment total</th>
							<th>EP Amt.</th>
							<th>Vendor Amt.</th>
							<th>time_start</th>
							<th>time_stop</th>
						</tr>
						@foreach($payments as $payment)
						<tr>
							<td>{{ $payment->id }}</td>
							<td><a href='/events/{{ $payment->event_id }}'>{{ $payment->event_id }}</a></td>
							<td><a href='/services/{{ $payment->service_id }}'>{{ $payment->service_id }}</a></td>
							<td><a href='/planners/{{ $payment->planner_id }}'>{{ $payment->planner_id }}</a></td>
							<td><a href='/vendors/{{ $payment->vendor_id }}'>{{ $payment->vendor_id }}</a></td>
							<td>${{ $payment->total/100 }}</td>
							<td>${{ ($payment->total/100) * .1 }} </td><!-- EventPeace commisssion of 10%, may change?  -->
							<td>${{ ($payment->total/100) * .9 }} </td><!-- Vendor pay out 90%, may change?  -->
							<td>{{ date( 'm/d/Y h:m A', $payment->time_start ) }}</td>
							<td>{{ date( 'm/d/Y h:m A', $payment->time_stop ) }}</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>

	</div>

@stop