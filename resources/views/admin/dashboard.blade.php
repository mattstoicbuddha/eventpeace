

@extends ('layout')

@section ('title')
  Admin Dashbard
@stop

@section ('content')



	<div class="container">
		<h1 class="text-center">You are logged in as Admin</h1>
		<div class="row">
			<div class="col-sm-4 col-sm-offset-4">
					@include ('partials.admin-nav')
			</div>
		</div>
	</div>

@stop

