
@extends ('layout')

@section ('title')
  Suspended Account
@stop

@section ('content')

	<h2> Your account has been suspended!!! </h2>
	<h5> If you feel you are receiving this notice in error, contact us. </h5>

@stop