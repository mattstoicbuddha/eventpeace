

@extends ('layout')

@section ('title')
  403 Page
@stop

@section ('content')
<style>
body{
 	background: url(http://lorempixel.com/g/1200/800/) no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;

</style>
<div class="errorPage">
	<div class="row text-center">
	  <div class="col-sm-6 col-sm-offset-3">

	           <h1>403 Forbidden</h1>


				<h4> If you like, you can click the buton below to go to the main page</h4>
				<a href="http://eventpeace.dev" class="btn btn-lg btn-info">Take Me To the Main Page.</a>

		</div>
	</div>
</div>

@stop