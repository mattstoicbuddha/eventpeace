@extends ('layout')

@section ('title')
  Vendor Address Needed
@stop

@section ('content')

<div class="container text-center">

<h2>Please complete your Vendor Profile first.</h2>
<h3>Your Profile information will be used in any services you will list.</h3>
<a class="btn btn-info btn-lg" href="/vendors/{{$vendor->id}}/edit">Edit My Vendor Profile</a>



</div>


@stop