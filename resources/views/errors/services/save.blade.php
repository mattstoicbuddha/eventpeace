

@extends ('layout')

@section ('title')
  Save Problem
@stop

@section ('content')
<div class="saveProblem">
	<div class="row text-center">
	  <div class="col-sm-6 col-sm-offset-3">
		
	           <h1>We have encountered a problem</h1>
	           <h3>Sorry but you will have to go back and try resubmitting</h3>
				
				<a href="http://eventpeace.dev" class="btn btn-lg btn-warning">Take Me Back</a>
			
		</div>
	</div>
</div>

@stop