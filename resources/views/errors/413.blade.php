

@extends ('layout')

@section ('title')
  File Too Big
@stop

@section ('content')
<style>
body{
 	background: url(http://lorempixel.com/g/1200/800/) no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;

</style>
<div class="errorPage">
	<div class="row text-center">
	  <div class="col-sm-6 col-sm-offset-3">

	           <h1>413 Request Entity Too Large</h1>


				<h4>The file you are trying to upload is too large for the server to accept.  Please go back and try to upload a smaller file.  Thank you.</h4>

		</div>
	</div>
</div>

@stop