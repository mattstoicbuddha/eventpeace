@extends ('layout')

@section ('title')
  FAQ By Planners
@stop

@section ('content')


<div class="container">
	<h3>FAQ By Planners</h3>

	<div class="faqList">
		<ul>
		<li><strong>Why should I use your site when I can look up various services on my own?</strong>  EventPeace is here to save you time!  We have all of your event planning needs in one spot so you don’t have to view multiple websites or make several phone calls just to see what’s available, how much the service costs, etc.</li>

		<li><strong>How much does it cost to use your services?</strong>  EventPeace is free for planners.
		What kinds of services can I request?  EventPeace has everything you need to help make your event a success — bounce houses, face painters, caterers, venues, photographers, etc. </li>

		<li><strong>How do I book a service?</strong>  All you need to do is create a new event and either search by category or just start typing in the search field.  Click “Add to Event” to include that service.  You then go to your Event Dashboard to create a request for that service.  The vendor will then review your request and either accept or decline.  You can find the status of any request on your Event Dashboard.  On that page, you will also be able to message vendors, add service extras (when offered), and view the status of each service.</li>

		<li><strong>When can I expect to hear back from vendors?</strong>  We have asked our vendors to reply to service requests within 24 hours as much as possible.  If you do not hear back within 48 hours, please feel free to email support@eventpeace.com. </li>

		<li><strong>What if vendors have questions about my request?</strong>  Vendors can message you and vice versa.  You will receive an email notification letting you know you have a message from a vendor.</li>

		<li><strong>Can I negotiate prices with vendors?</strong>  Yes, our system allows vendors to make modifications to their prices as they see fit.</li>

		<li><strong>How does EventPeace guarantee the quality of work?</strong>  We have screened all of our vendors for quality and experience prior to adding them to our website.  If you are not satisfied with a service provided at an event, please email support@eventpeace.com and we will work with you to make things right.</li>

		<li><strong>What happens after I accept an offer?</strong>  Once you provide your credit card information, we charge the full amount of the offer total and hold that money in escrow until 24 hours after the event, at which point we release the money to the vendor.</li>

		<li><strong>What if I have a same day event?</strong>  You can still try to book a service but we cannot guarantee that vendors will respond in time.  </li>

		<li><strong>How does EventPeace protect my privacy?</strong>  We only share your contact information with vendors you whose services you have selected for events.</li>
		</ul>
	</div>


</div>
@stop