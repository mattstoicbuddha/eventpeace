@extends ('layout')

@section ('title')
	Home Page
@stop


@section ('content')

<section class="homeBannerTop">
	<div class="homeButtonsAction container">
		<div class="row text-center">
			<div class="col-xs-6">
				<a href="/events/create" class="btn  btn-white btn-xl">I'm a Planner</a>
			</div>
			<div class="col-xs-6">
				<a href="/vendors/landing" class="btn  btn-white btn-xl">I'm a Vendor</a>
			</div>
		</div>
	</div>
</section>

<section class="secondSection">
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
			<img class="center-block sectionTwoIcon" src="/images/icons/arrows_circle_check.svg">
			<h3 class="sectionTwoFeature">Sign Up<br/>For Free</h3>
			</div>
			<div class="col-sm-4">
			<img class="center-block sectionTwoIcon" src="/images/icons/basic_compass.svg">
			<h3 class="sectionTwoFeature">Find Everything<br/>You Need</h3>
			</div>
			<div class="col-sm-4">
			<img class="center-block sectionTwoIcon" src="/images/icons/basic_cloud.svg">
			<h3 class="sectionTwoFeature">All Managed in<br/>One Place</h3>
			</div>
		</div>
	</div>
</section><!--eof second section-->

<section class="thirdSection">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="startSelling"><a href="/vendors/landing" class="btn btn-lg btn-white btn-outline">Start Selling</a></div>
		</div>
	</div>
</section><!--eof third section-->

<section class="fouthSection">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="fouthSectionTagline">EventPeace will change the way you do business!</h2>
			</div>
		</div>
	</div>
</section><!--eof fourth section-->

<!--for future

<section class="fifthSection">
	<h2 class="text-center">Featured Vendors</h2>
</section>

-->


@stop
