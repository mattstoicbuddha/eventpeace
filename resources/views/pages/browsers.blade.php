
@extends ('layout')

@section ('title')
  Out of Date Browser
@stop

@section ('content')
<style>
body{
 	background: url(http://lorempixel.com/g/1200/800/) no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;

</style>
<div class="container">
	<div class="panel">
		<h2 class="text-center">Your web browser is not up to date.</h2>
		<h3  class="text-center">Please use the links below to upgrade your browser or use a different one.</h3>
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
			<ul>
			<li><strong>Google Chrome:</strong> <a href='https://www.google.com/chrome/' target='_blank'>https://www.google.com/chrome/</a></li>

			<li><strong>Mozilla Firefox:</strong> <a href='https://www.mozilla.org/en-US/firefox/new/' target='_blank'>https://www.mozilla.org/en-US/firefox/new/</a></li>

			<li><strong>Apple Safari:</strong> May require update at least OSX version is 10.10 (Yosemite).</li>
			</ul>
			</div>
		</div>
	</div>
</div>
@stop