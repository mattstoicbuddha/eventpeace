
@extends ('layout')

@section ('title')
  Edit Planners Profile
@stop

@section ('content')

<div class="row">
  <div class="col-sm-6 col-sm-offset-3">
	<div class="">
		<form id="plannersEdit" method="POST" action="/planners/{{ $planners->id }}" enctype="multipart/form-data">
			<input name="_method" type="hidden" value="PUT">
			<div class="row">
				<div class="col-sm-6">
				  	<div class="form-group">
					  	<label>Planner Name</label>
					    <input value="{{ !empty($planners->name) ? $planners->name.'' : '' }}" name="plannersName" type="text" class="form-control" id="plannersName" >
				  	</div>
				</div>
				<div class="col-sm-6">
				  	<div class="form-group">
					  	<label>Street Address</label>
					    <input value="{{ !empty($planners->address) ? $planners->address : '' }}" name="plannersAddress" type="text" class="form-control" id="plannersAddress" >
				  	</div>
			  	</div>
		  	</div>

			<div class="row">
				<div class="col-sm-6">
				  <div class="form-group">
				  	<label>City</label>
				    <input value="{{ !empty($planners->city) ? $planners->city : '' }}" name="plannersCity" type="text" class="form-control" id="plannersCity" >
				  </div>
				</div>

				<div class="col-sm-2">
					<div class="form-group">
					  	<label>State</label>
					    <input value="{{ !empty($planners->state) ? $planners->state : '' }}" name="plannersState" type="text" class="form-control" id="plannersState" >
					</div>
				</div>

				<div class="col-sm-4">
				  	<div class="form-group">
					  	<label>Zip</label>
					    <input value="{{ !empty($planners->zip) ? $planners->zip : '' }}" name="plannersZip" type="text" class="form-control" id="plannersZip" >
				  	</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4">
				  	<div class="form-group">
					  	<label>Phone</label>
					    <input value="{{ !empty($planners->phone) ? $planners->phone : '' }}" name="plannersPhone" type="text" class="form-control" id="plannersPhone" >
				  	</div>
				</div>

				<div class="col-sm-4">
				  	<div class="form-group">
					  	<label>Website</label>
					    <input value="{{ !empty($planners->website) ? $planners->website : '' }}" name="plannersWebsite" type="text" class="form-control" id="plannersWebsite" >
				  	</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
					  	<label>Email</label>
					    <input value="{{ !empty($planners->email) ? $planners->email : '' }}" name="plannersEmail" type="text" class="form-control" id="plannersEmail" >
				  	</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
				  		<label>New Password</label>
				  		@if(session()->has('result') && session()->has('password') && (int)session('password') === 3)
				  			<div class='alert alert-danger'>Your passwords did not match.</div>
				  		@elseif(session()->has('result') && session()->has('password') && (int)session('password') === 2)
				  			<div class='alert alert-success'>Password successfully changed!</div>
				  		@endif
			    		<input name="newPassword" type="password" class="form-control" placeholder="New Password">
			    		<input name="newPassword2" type="password" class="form-control" placeholder="New Password Again">
				  	</div>
			  	</div>

			</div>
		  <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  <button class="btn btn-default">Update</button>
		</form>

		<div id='thisholdscards'>

			<h3>Your cards</h3>

			@if(!empty($cards) && !empty($cards->data))

				@foreach($cards->data as $card)

					<form method="POST" action="/users/deletecard">
						<div class='cardBrand'>{{ $card->brand }}</div>
						<div class='cardLast4'>{{ $card->last4 }}</div>
						<button class='btn btn-danger'>Delete</button>
						<input class="form-control" type="hidden" name="card_id" value="{{ $card->id }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					</form>

				@endforeach

			@endif

		</div>

		<form action="/users/addcard" method="POST" id="payment-form">
		  <span class="payment-errors"></span>

		  <div class="form-row">
		    <label>
		      <span>Card Number</span>
		      <input type="text" size="20" data-stripe="number">
		    </label>
		  </div>

		  <div class="form-row">
		    <label>
		      <span>Expiration (MM/YY)</span>
		      <input type="text" size="2" data-stripe="exp_month">
		    </label>
		    <span> / </span>
		    <input type="text" size="2" data-stripe="exp_year">
		  </div>

		  <div class="form-row">
		    <label>
		      <span>CVC</span>
		      <input type="text" size="4" data-stripe="cvc">
		    </label>
		  </div>

		  <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  <input type="submit" class="submit" value="Submit Payment">
		</form>

		@include('stripe')

	</div>
  </div>
</div>



@stop