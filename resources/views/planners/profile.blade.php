
@extends ('layout')

@section ('title')
  Planners Profile
@stop

@section ('content')

<div class="row">
<a class="btn btn-sm btn-default" href="/planners/{{$planners->id}}/edit">Manage my Planner Profile</a>
  <div class="col-sm-6 col-sm-offset-3">
	  	<div class="row">
	  	
		  	<div class="col-sm-6">
	  
			  	<h2>{{ !empty($planners->name) ? $planners->name : '' }}</h2>
			  		<h4>
					 	{{ !empty($planners->address) ? $planners->address : '' }}<br>
					  	{{ !empty($planners->city) ? $planners->city : '' }} {{ !empty($planners->state) ? $planners->state : '' }} &nbsp;&nbsp;{{ !empty($planners->zip) ? $planners->zip : '' }}
				  	</h4>
			</div>

			<div class="col-sm-6">
				<h3>Contact</h3>
				<h4>
				  	<strong>Phone:</strong> {{ !empty($planners->phone) ? $planners->phone : '' }}<br>
				  	
				    <strong>Website:</strong> {{ !empty($planners->website) ? $planners->website : '' }}<br>
				  	
				    <strong>Email:</strong> {{ !empty($planners->email) ? $planners->email : '' }}
				</h4>

			</div>

			
		</div>

	
	
  </div>
</div>



@stop