<!DOCTYPE html>
<html lang="en">
  <head>
     @include('partials.mail-head')
  </head>

  <header>
      @include ('partials.mail-header')
  </header>

  <body>
    <div class="container">
      <div class="contentWrapper">
        @yield ('content')
      </div>
    </div>



    <footer>
          @include('partials.footer')
    </footer>

  </body>

</html>

