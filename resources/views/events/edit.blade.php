<!--this form will be used to edit an event-->

@extends ('layout')

@section ('title')
  Edit Service Form
@stop

@section ('content')
<div class="row">
  <div class="col-sm-6 col-sm-offset-3">

	<form data-name="createEvent" id="createEvent">
	  <div class="form-group">
	    <input name ="EventName" id="EventName" class="form-control" value=""/>
	  </div>
	  <div class="form-group">
	    <select name ="EventServiceCat" id="EventServiceCat" class="selectpicker form-control">
                <option value="" disabled selected>Select Event Category</option>
                @foreach($categories as $c)

                	<option value='{{$c->id}}'>{{$c->name}}</option>

                @endforeach
        </select>
	  </div>
	  <div class="form-group">
	     <select name ="EventServiceSubcat" id="EventServiceSubcat" class="selectpicker form-control">
                <option value="" disabled selected>Select Event Sub-Category</option>
         </select>
	  </div>
	  <div class="form-group">
	    <input name="eventLocation" type="text" class="form-control" id="eventLocation"  value=" ">
	  </div>

	  <button class='btn btn-success'>Find Matches</button>

	</form>

  </div><!--eof col-sm-6 col-sm-offset-3-->
</div><!--eof row-->

@stop