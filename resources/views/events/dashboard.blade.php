ex
@extends ('layout')

@section ('title')
  Event Dashboard
@stop

@section ('content')

<div class="">
	<div class="container">
	<h1>My Events
	@if (!$expired)
	<a class="btn btn-default btn-sm" href="/events/dashboard/expired">View Expired Events</a>
	@else
	<a class="btn btn-default btn-sm" href="/events/dashboard/">View Active Events</a>
	@endif
	</h1>
		<div class="row">

			<div class="col-sm-12"><!--mainarea-->

			@foreach ( $events as $e )
			<div class="eventWrapper {{ !empty($e->end) && (int) $e->end < time() ? 'eventExpired' : '' }}">
			<div class="row">
				<div class="col-sm-4">
					<h2>{{ !empty($e->name) ? $e->name : '' }}<!--Event Title-->
					@if(!empty($e->end) && (int) $e->end >= time())
						<a class="editThis" href="/events/{{ !empty($e->id) ? $e->id : '' }}/edit"></a>
					@else
						(Expired)
					@endif
					</h2>
				</div>
				<div class="col-sm-5">

					<ul class="startEndTimes">
						<li>Event ID: {{ !empty($e->id) ? $e->id : '' }}</li>
						<li>Event Start:<strong> {{ !empty($e->start) ? date("F j, Y, g:i a", $e->start) : '' }}</strong></li>
						<li>Event End:<strong> {{ !empty($e->end) ? date("F j, Y, g:i a", $e->end) : '' }}</strong></li>
						<li>Annual Event:<strong> {{ !empty($e->annual) && (int) $e->annual === 1 ? 'Yes' : 'No' }}</strong></li>
						<!--shows event id-->

					</ul>
				</div>
				<div class="col-sm-3">
					<p class="text-right eventTotalCost">
						<span class="eventTotalCostLabel">Total</span> ${{ number_format($e->total/100, 2, '.', '') }}
					</p>
				</div>
			</div>
				<!--Now, the services-->

				@foreach ( $e->services as $s )

					@if(empty($s->service->id))
						<?php continue; ?>
					@endif

						<?php $rand = rand(0, 9999999); ?>

						<div class="panel-group" id="accordion{{ $s->service->id . $rand }}" role="tablist" aria-multiselectable="false">
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="services{{ $s->service->id }}">
						       <a data-toggle="collapse" data-parent="#accordion{{ $s->service->id . $rand }}" href="#services{{ $rand }}" aria-expanded="false" aria-controls="servicesList">
						       	<div class="row">
							       	<div class="col-sm-6">
							       		<div class="servicesName">
											{{ !empty($s->service->name) ? $s->service->name : '' }}
										</div>
									</div>
									<div class="col-sm-2">
										<div class="servicesPrice">
											${{ !empty($s->service_total) ? number_format($s->service_total/100, 2, '.', '') : '' }}
										</div>
									</div>

									<div class="col-sm-3 {{ !empty($s->request->status->id) ? request_text_class($s->request->status->id) : 'text-info' }}">
									{!! !empty($s->request->status->id) ? request_icon($s->request->status->id) : request_icon(0)  !!}
										<span class="statusText">{{ !empty($s->request->status->text) ? $s->request->status->text : 'Needs Request' }}</span>
									</div>
									<div class="col-sm-1">
										<div class="servicesCarret">
											<img  src="/images/icons/arrows_circle_down.svg" width="24px" height="24px">
										</div>
									</div>

								</div>
								</a>
							</div>
								<!--inside collapse-->
								 <div id="services{{ $rand }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="servicesList">
      								<div class="panel-body">

      								<div class="row">
	      								<div class="col-sm-3">
											<img class="img-responsive" src="{{ !empty($s->service->image) ? env('CDN_URL', 'http://cdn.todcandev.com/eventpeace') . '/services/' . $s->service->image : env('CDN_URL', 'http://cdn.todcandev.com/eventpeace') . '/noimage.jpg'  }}">
	      								</div>
										<div class="col-sm-9">
										<h4>Service Description</h4>
										{{ !empty($s->service->description) ? $s->service->description : '' }}
										</div>


									</div>
									@if(!empty($s->calendar))

											<?php $morerand = rand(0, 9999999); ?>

		      								<button type="button" id="availabilityButton{{$s->service->id.$rand.$morerand}}" class="btn btn-primary btn-lg calAvailability">
											  View availability calendar
											</button>

											<script>
											$("#availabilityButton{{$s->service->id.$rand.$morerand}}").click(function(){
												$("#availability").find(".cal-wrapper").html("<iframe src='https://calendar.google.com/calendar/embed?{{$s->calendar}}' style='border: 0'  frameborder='0' scrolling='no'></iframe>");
												$("#availability").modal('show');
											});
											</script>


									@endif
									<hr>
									<div class="row">
										<div class="{{!empty($s->request->quantity) ? 'col-sm-3' : 'col-sm-4'}}">
										<h4>Price Details</h4>
										<h5>${{ !empty($s->service->price) ? number_format($s->service->price/100, 2, '.', '') : '' }}</h5>
										{{ !empty($s->service->price_details) ? $s->service->price_details : '' }}
										</div>

										@if(!empty($s->request->quantity))
										<div class="col-sm-3">
										<h4>Request Quantity</h4>
											{{ $s->request->quantity }}
										</div>
										@endif
										<div class="{{!empty($s->request->quantity) ? 'col-sm-3' : 'col-sm-4'}}">
										<h4>Delivery Charge</h4>
										${{ !empty($s->service->delivery_charge) ? $s->service->delivery_charge/100 : '0' }}
										</div>
										<div class="{{!empty($s->request->quantity) ? 'col-sm-3' : 'col-sm-4'}}">
										<h4>Delivery Time/Details</h4>
										{{ !empty($s->service->delivery_time) ? $s->service->delivery_time : '' }}
										</div>
									</div>
									<hr>

									<div class="row">

										<div class="{{ !empty($s->extras) ? 'col-sm-6' : 'col-sm-12' }}" id="termsAndConditions">
										<h4>Terms & Conditions</h4>
										{{ !empty($s->service->terms_conditions) ? $s->service->terms_conditions : '' }}
										</div>
										@if(!empty($s->extras))
										<div class="col-sm-6 well">
										<h4>Extras</h4>


												@foreach($s->extras as $ex)
												 <div id="serviceExtras" class="serviceExtras"><b>{{!empty($ex->name) ? $ex->name . ": " : ""}}</b>{{!empty($ex->price) ? "$" . $ex->price/100 . ", " : ""}} {{!empty($ex->description) ? $ex->description : ""}}</div>
												@endforeach

										</div>
										@endif

										@if(!empty($s->request->price_diff))
											<div class="col-sm-6 well">
												<h4>Modification to Original Price: </h4>


												<p><span class='h3'>{{ $s->request->price_diff < 0 ? '-$' . ($s->request->price_diff * -1)/100 : '+$' . $s->request->price_diff/100 }}</span></p>
												<p><strong>Modification Description: </strong> <p>{{$s->request->price_diff_description}}</p></p>
											</div>
										@endif
									</div>
										<div class="row">
											<div class="editCancelButtons">
												@if(empty($s->request->status->id) || !empty($s->request->status->id) && array_search( $s->request->status->id, [2, 5, 11, 12, 13, 14, 15, 16]) === false )
												<div class="btn-group" role="group" aria-label="...">
													@if(!empty($s->request->id) && (int) $s->request->status->id === 9)
													<button type='button' class='btn btn-success dashboardPlannerPayRequest'  data-id='{{$s->request->id}}' data-servicename="{{ !empty($s->service->name) ? $s->service->name : '' }}">Make Payment</button>
													@endif

													<button type='button' class='btn  btn-info dashboardPlannerEditRequest'  data-id="{{ !empty($s->request->id) ? $s->request->id : '0' }}" data-service="{{ !empty($s->service->id) ? $s->service->id : '0' }}" data-servicename="{{ !empty($s->service->name) ? $s->service->name : '' }}" data-serviceprice="{{ !empty($s->service->price) ? ((int) $s->service->price) : '0' }}" data-event="{{ !empty($e->id) ? $e->id : '0' }}" data-requeststart="{{ !empty($s->request->time_start) ? $s->request->time_start : !empty($e->start) ? $e->start :  time() }}" data-requestend="{{ !empty($s->request->time_stop) ? $s->request->time_stop : !empty($e->end) ? $e->end :  time() }}" data-requestquantity="{{ isset($s->service->price_type)  && (int) $s->service->price_type === 0 ? 'true' : 'false' }}" data-requestquantitynumber="{{ !empty($s->request->quantity) ? $s->request->quantity : 1 }}" data-address="{{ !empty($s->request->address) ? $s->request->address : '' }}" data-city="{{ !empty($s->request->city) ? $s->request->city : '' }}" data-state="{{ !empty($s->request->state) ? $s->request->state : '' }}" data-zip="{{ !empty($s->request->zip) ? $s->request->zip : '' }}" data-pricediff="{{ !empty($s->request->price_diff) ? $s->request->price_diff : 0 }}" data-pricediffdescription="{{ !empty($s->request->price_diff_description) ? $s->request->price_diff_description : '' }}" data-status="{{!empty($s->request->status->id) ? $s->request->status->id : ''}}" >{{ !empty($s->request->id) ? 'Edit Request' : 'Create Request' }}</button>

													@if(!empty($s->request->id))
													<button type='button' class='btn btn-danger dashboardPlannerEditRequestCancel'  data-id="{{ !empty($s->request->id) ? $s->request->id : '0' }}" data-status="{{request_status_encoder($user, 11)}}">Cancel Request</button>
													@endif
												</div>
												@endif
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>


				@endforeach
				</div>
			@endforeach

		</div>
	</div>
</div>

<div id="requestModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="arrows-circle-remove" aria-hidden="true"><img width="30px" height="30px" src="/images/icons/arrows_circle_remove.svg"></span></button>
        <h2 class="modal-title vendorIcon">Modal title</h2><span class='modal-price'></span>
      </div>
      <div class="modal-body">
      	<div class='text-success dashboardPlannerSaveRequestStatus'></div>
        <form  data-name="requestEvent" id="requestEvent">
				<div class="row">
					<div class="col-sm-6">
	        			<div class="form-group">
							<label>Start Date for this service</label>
							<div class='input-group date'  name="serviceStartDate" id="serviceStartDate">
								<input class="form-control" type="text">
								<span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
							</div>
						</div>
	        		</div>
	        		<div class="col-sm-6">
						<div class="form-group">
							<label>Start time for this service</label>
							<div class='input-group date'  name="serviceStartTime" id="serviceStartTime">
								<input class="form-control" type="text">
								<span class="input-group-addon">
			                        <span class="glyphicon glyphicon-time"></span>
			                    </span>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label>End Date for this service</label>
								<div  class='input-group date' name ="serviceEndDate" id="serviceEndDate"><input class="form-control" type="text">
									<span class="input-group-addon">
				                        <span class="glyphicon glyphicon-calendar"></span>
				                    </span>
			                    </div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label>End time for this service</label>
								<div  class='input-group date' name ="serviceEndTime" id="serviceEndTime"><input class="form-control" type="text">
									<span class="input-group-addon">
				                        <span class="glyphicon glyphicon-time"></span>
				                    </span>
			                    </div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label>Full address for this service</label>
					<input class="form-control address-for-service" type="text" name="address-for-service" placeholder="Address">
					<input class="form-control city-for-service" type="text" name="city-for-service" placeholder="City">
					<input class="form-control state-for-service" type="text" name="state-for-service" placeholder="State">
					<input class="form-control zip-for-service" type="text" name="zip-for-service" placeholder="Zip">
				</div>

					<!-- we'll keep custom extras for an additional future upgrade-->

					<div class='modal-price-diff'></div>
					<div class='modal-price-diff-description'></div>

					<div class='modal-price-type'></div>

					<div class="col-sm-12 addExtrasToRequest">
						<h3>Need to add extras?</h3>
						<div id="serviceRequestExtras" class="serviceRequestExtras">
							<!--from main-evp.js-->
						</div>
					</div>

					<div class="addExtrasToRequest">
						<h3>Send this Vendor a message</h3>
						<div id="serviceNotes"></div>
					</div>

			<input type='hidden' name='status' value='{{request_status_encoder($user, 8)}}' />

        </form>




      </div>
      <div class="modal-add-to-event-button"></div>
      <div class="modal-footer">
      	<button class="dashboardPlannerSaveRequest btn btn-success pull-left" type="button">Submit</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Payment Modal -->
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="paymentModalLabel">Process a Payment</h4>
      </div>
      <div class="modal-body">
      	<div class="requestServicePaymentError alert alert-danger" style='display: none;'></div>
      	<div id='paymentInfo'></div>
        <form action="/users/addcard" method="POST" id="payment-form" data-submit="false" style='display: none;'>
		  <span class="payment-errors"></span>

		  <div class="form-row">
		  <h3>Add a Card</h3>
		    <label>
		      <span>Card Number</span>
		      <input type="text" size="20" data-stripe="number">
		    </label>
		  </div>

		  <div class="form-row">
		    <label>
		      <span>Expiration (MM/YY)</span>
		      <input type="text" size="2" data-stripe="exp_month">
		    </label>
		    <span> / </span>
		    <input type="text" size="2" data-stripe="exp_year">
		  </div>

		  <div class="form-row">
		    <label>
		      <span>CVC</span>
		      <input type="text" size="4" data-stripe="cvc">
		    </label>
		  </div>
		  <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  <input type="submit" class="submit" value="Add Card">
		</form>
      </div>

    </div>
  </div>
</div>

<div id="availability" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><img width="30px" height="30px" src="/images/icons/arrows_circle_remove.svg"/></button>
				<h2>Availability Calendar</h2>
			</div>
			<div class="modal-body">
					<div class="cal-wrapper">

					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
	$("#serviceStartDate").datetimepicker({
	                    format: 'MM/DD/YYYY'
	                });
	$("#serviceStartTime").datetimepicker({
	                    format: 'LT',
						stepping: 30
	                });
	$("#serviceEndDate").datetimepicker({
	                    format: 'MM/DD/YYYY',
	                    useCurrent: false
	                });
	$("#serviceEndTime").datetimepicker({
	                    format: 'LT',
						stepping: 30
	                });
</script>

@include('stripe')
@stop