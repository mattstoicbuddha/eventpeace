<!--this form will be used to create an event-->
@extends ('layout')
@section ('title')
Create Service Form
@stop
@section ('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-3 col-lg-3 leftCol"><!--left col-->
		<div class="serviceSelectionWrapper">
			<form data-name="createEvent" id="createEvent">
				<div name ="EventServiceCat" id="EventServiceCat">
					<div class ="desktopEventCats form-group">
						<!-- <h4>Category</h4> -->
						<!-- <ul class="mainCategoryIcons " role="menu"> -->
							<select class='mainCategoriesSelect form-control' style='font-size: 20px; height: 100%'>
							<option value='-1' selected>Category</option>
							@foreach($categories as $c)
							<option value='{{$c->id}}'>{{$c->name}}</option>
							<!-- <li class='mainCategories category{{$c->id}}' data-id='{{$c->id}}' ><img class="EventServiceCatImg" src='http://cdn.todcandev.com/eventpeace/sidenav/icons/{{$c->name}}.png' alt="{{$c->name}}"><span>{{$c->name}}</span></li>
							<div class="clearfix"></div> -->
							@endforeach
							</select>
						<!-- </ul> -->
					</div>
				</div>
				<div id='subsMenuWrapper' class="form-group">
					<!-- <h4 id="subsMenuTitle">Sub-category:</h4> -->
					<!-- <a href="#" class="btn btn-block btn-default dropdown-toggle" data-toggle="dropdown">Sub Category <b class="caret"></b></a>-->
					<select class='subCategoriesSelect form-control' style='font-size: 20px; height: 100%'>
					<option value='-1' selected>Sub-Category</option>
					</select>
					<!--class="dropdown-menu navmenu-nav" role="menu"-->
				</div>
				<!-- <h4>Search Location</h4>
					<div class="form-group">
							<input name="eventLocation" type="text" class="form-control" id="eventLocation" placeholder="city & state or zip" value="{{ !empty($input->eventLocation) ? $input->eventLocation : "" }}">
					</div>
					<div class="form-group">
							<select name="eventDistance" type="text" class="form-control" id="eventDistance" placeholder="Miles">
									<option value="none">Select Distance</option>
									<option value="10">10 miles</option>
										<option value="25">25 miles</option>
										<option value="50">50 miles</option>
										<option value="100">100 miles</option>
								</select>
								@if(!empty($input->eventDistance))
					<script>$('#eventDistance').val({{ $input->eventDistance }});</script>
					@endif
				</div> -->
			</form>
			<div class="form-group">
				<!--  <label>Search using Tags<span class="pls-help" data-toggle="popover" data-placement="bottom" title="Use these Tags" data-content="You can use these tags to refine your results. You can also add some from the additioanl (yellow) tags section, remove some. You may even type in new ones all-together. Dont forget to hit the 'Submit' button to search again." data-placement="left"></span></label>
				<input class="form-control" type='text' id='serviceTags' name='serviceTags' />
			</div>
			<script>
				$('#serviceTags').tagsInput({
					'height':'100%',
				'width':'100%',
				'defaultText':'Add a tag',
				'onAddTag': addServiceTag,
				});
				window.st = $('#serviceTags');
				function addServiceTag(tag){
					window.st.importTags(window.formatTags($('#serviceTags').val()));
					$("#serviceTags_tag").focus();
				}
			@if(!empty($tag))
				$('#serviceTags').importTags('{{$tag}}');
				$(document).ready(function(){
							$(".eventSearchSubmit").trigger('click');
						});
					@endif
			</script>
			<div class="form-group">
				<div class="form-group">
					<label>Select more tags to search<span class="pls-help" data-toggle="popover" data-placement="bottom" title="Use additional Tags" data-content="You can these additional tags to refine your results. Click on them to add them or you may also add them by typing in tags directly. Dont forget to hit the 'Submit' button to search again." data-placement="left"></span></label>
					<div id="tagHolder" class='tagHolder col-sm-12'></div>
				</div>
			</div> -->
		</div>
		</div><!--eof left col -->
	</div>
	<div class="col-md-8 middleCol">
		<div>
			<div class="row">
				<div class="col-md-12">
					<div class='searchBar'>
					<div class="form-group has-feedback">
						<input type='text' class='searchText form-control' placeholder="Search" />
						 <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
					</div>
					</div>
				</div>
				<!-- <div class="col-md-3 col-lg-3">
					<button type='button' class='btn btn-block btn-ltGreen btn-lg eventSearchSubmit'><span class="search">Search</span></button>
				</div> -->
				<!-- <div class="col-md-5 col-lg-5">
					<img class="tipIcon" src="http://cdn.todcandev.com/eventpeace/flat-icons/idea.png">
					<p class="tipParagraph">You may just click a Category and hit the search button for a broad search.  If you would like to do a more detailed search, add a Sub-category, Location info, or Tags to get more specific results.</p>
				</div> -->
			</div>
		</div>
		<div class ="mobileEventCats">
		</div>
		<div class="row yourMatches">
			<h3>Your Matches<!-- <span class="pls-help" data-toggle="popover" data-placement="right" title="Below are the matches for your search" data-content="You can search again with tags to better filter your results. If you want to add a service to this event, you must first click the 'Details' button and review the vendor's listing details." data-placement="left"></span> --></h3>
			<div class="eventMatchesWrapper">
				<!--from search.evp.js-->
				<div id="eventMatches"></div>
			</div>
		</div>
		</div><!--eof middle col-->
		<div class="col-md-4 ">
			<div class="serviceSelectionWrapper">
			<form data-name="createEvent" id="createEvent">
				<div name ="EventServiceCat" id="EventServiceCat">
					<div class ="desktopEventCats form-group">

							<select class='mainCategoriesSelect form-control' style='font-size: 20px; height: 100%'>
							<option value='-1' selected>Category</option>
							@foreach($categories as $c)
							<option value='{{$c->id}}'>{{$c->name}}</option>

							@endforeach
							</select>

					</div>
				</div>
				<div id='subsMenuWrapper' class="form-group">
					<select class='subCategoriesSelect form-control' style='font-size: 20px; height: 100%'>
					<option value='-1' selected>Sub-Category</option>
					</select>

				</div>

			</form>
			<div class="form-group">

		</div>
			<div class="rightCol">
				<h3>Save Your Event Details</h3>
				<div class="form-group">
					@if( is_array($events) && count($events) > 0)
					<label>Choose an Event</label>
					<select id="eventNameSelect" class="form-control">
						<option value='none'>Choose an Event</option>
						@foreach($events as $e)
						<option value='{{$e->id}}'>{{$e->name}}</option>
						@endforeach
						<option value='new'>New Event</option>
					</select>
					<input name="EventName" id="EventName" class="form-control hide" placeholder="Event Name" />
					@elseif(!empty($edit))
					<label>Event Name</label>
					<input name="EventName" id="EventName" class="form-control" placeholder="Event Name" value="{{ $events->name }}" />
					@else
					<label>Give your event a unique name</label>
					<input name="EventName" id="EventName" class="form-control" placeholder="Event Name" />
					@if(empty($user))
					<label>Your Email Address</label>
					<input name="EventEmail" id="EventEmail" class="form-control" placeholder="Email Address" />
					@endif
					@endif
				</div>
				<!--bof date time picker-->
				<div class="row">
					<h4 class="text-center">When is this event?</h4>
					<div class="col-lg-6">
						<div class="form-group">
							<div  class='input-group date' name="EventStartDate" id="EventStartDate" >
								<input class="form-control" placeholder="Event Start Date" {!! !empty($edit) ?  'value="'.date('m/d/y', $events->start).'"' : '' !!} />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<div name="EventStartTime" id="EventStartTime" class='input-group date' name ="serviceEndDate" id="serviceEndDate">
								<input class="form-control" placeholder="Event Start Time" {!! !empty($edit) ?  'value="'.date('h:i A', $events->start).'"' : '' !!}  />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-time"></span>
								</span>
							</div>
						</div>
					</div>
					</div><!--row 1-->
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<div name="EventEndDate" id="EventEndDate" class='input-group date' name ="serviceEndDate" id="serviceEndDate">
									<input class="form-control" placeholder="Event End Date" {!! !empty($edit) ?  'value="'.date('m/d/y', $events->end).'"' : '' !!}  />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div name="EventEndTime" id="EventEndTime" class='input-group date' name ="serviceEndDate" id="serviceEndDate">
									<input class="form-control" placeholder="Event End Time" {!! !empty($edit) ?  'value="'.date('h:i A', $events->end).'"' : '' !!} />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-time"></span>
									</span>
								</div>
							</div>
						</div>
						</div><!--row 2-->
						<!--eof date time picker-->
						<div class="eventCreateBox">
							<div class="estimateTotal">
								<p>Estimate</p>
								<div id="saveEventTotal"></div>
							</div>
							<div class="annualEvent">
								<p><input type='checkbox' name='annualEvent' value='1' {{ !empty($edit) && (int) $events->annual === 1 ? 'checked' : '' }} /> This is an annual event.</p>
							</div>
							<div class="saveEvent">
								<button id="saveEventButton" class='btn btn-white btn-outline btn-lg'>Save Event</button>
							</div>
						</div>
						<!--left col to be used to display saved items-->
						<div class="clearfix"></div>
						<div class="eventMatchesWrapper">
							<h3>Your Selected Services<span class="pls-help" data-toggle="popover" data-placement="bottom" title="Add as many as you like" data-content="You can keep adding services until you have everything you need for this event. Make sure to hit 'Save Event' before you are done." data-placement="left"></span></h3>
							<hr>
							<div id="servicesDisplayBox"></div>
						</div>
					</div>
				</div>
				</div><!--eof row-->
			</div>
			<div id="loginModal" class="modal fade" role="dialog">
				<div class="modal-dialog modal-md" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="arrows-circle-remove" aria-hidden="true"><img width="30px" height="30px" src="/images/icons/arrows_circle_remove.svg"></span></button>
							<h2 class="modal-title vendorIcon">Log in to Continue</h2>
						</div>
						<div class="modal-body">
							<form id='loginModalLoginForm'>
								<div class='loginModalError'></div>
								<input type='text' name='userEmailAddress' placeholder="Email" />
								<input type='password' name='userPassword' placeholder="Password" />
								<button type='button' class='btn btn-success' id='loginModalLoginButton'>Login</button>
								<button type='button' class='btn btn-info' onclick="$('#loginModalLoginForm').fadeOut('slow', function(){$('#loginModalRegisterForm').fadeIn('slow')})">New? Click Here</button>
							</form>
							<form id='loginModalRegisterForm'>
								<input type='text' name='userEmailAddress' placeholder="Email" />
								<input type='text' name='userPassword' placeholder="Password" />
								<button type='button' class='btn btn-success'  id='loginModalRegisterButton'>Register</button>
								<button type='button' class='btn btn-info' onclick="$('#loginModalRegisterForm').fadeOut('slow', function(){$('#loginModalLoginForm').fadeIn('slow')})">Have an Account? Click Here</button>
							</form>
						</div>
						<div class="modal-footer">
							<div class="modal-add-to-event-button pull-left"></div>
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
						</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div>
					<div id="eventSavedDialogModal" class="modal fade" role="dialog">
						<div class="modal-dialog modal-md" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h2 class="modal-title vendorIcon text-center">Successfully Saved!</h2>
								</div>
								<div class="modal-body">
								<H3>Important</H3>
									<!--<p>One fine body&hellip;</p> -->
								</div>
								<div class="modal-footer">
									<a class='btn btn-success btn-block btn-lg' href= '#' data-dismiss="modal" >OK</a>
								</div>
								</div><!-- /.modal-content -->
								</div><!-- /.modal-dialog -->
							</div>
							<div id="searchModal" class="modal fade" tabindex="-1" role="dialog">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="arrows-circle-remove" aria-hidden="true"><img width="30px" height="30px" src="/images/icons/arrows_circle_remove.svg"></span></button>
										</div>
										<div class="modal-body">
											<p>One fine body&hellip;</p>
										</div>
										<div class="modal-footer">
											<div class="modal-add-to-event-button pull-left"></div>
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										</div>
										</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
										</div><!-- /.modal -->
										@foreach($subcategories as $v=>$sub)
										<div style='display:none' id='subs{{$v}}'>
											@foreach($sub as $s)
											<li class='subCategories category{{$s->id}}' data-id='{{$s->id}}' data-parent='{{$v}}' >{{$s->name}}</li>
											@endforeach
										</div>
										@endforeach
										<script>
											$("#EventStartDate").datetimepicker({
										format: 'MM/DD/YYYY'
										});
											$("#EventStartTime").datetimepicker({
										format: 'LT'
										});
											$("#EventEndDate").datetimepicker({
										format: 'MM/DD/YYYY'
										});
											$("#EventEndTime").datetimepicker({
										format: 'LT'
										});
											$("#EventStartDate").on("dp.change", function (e) {
												if(!window.startDateOnce || window.startDateTwice){
													window.startDateOnce = true;
													return;
												}
												$("#EventEndDate input").val($("#EventStartDate input").val());
												window.startDateTwice = true;
										});
										</script>
										@if(!empty($edit))
										<script>
											var searchObj = typeof window.classObj !== 'undefined' ? window.classObj : new evpSearch('services');
										searchObj.getFromEvent({{$edit}});
										</script>
										@endif
										@if(!empty($input->EventServiceCat))
										<script>
											$(document).ready(function(){
												$("#eventSearchSubmit").trigger('click');
											});
										</script>
										@endif
										<script type="text/javascript">
											$(function () {
										$('[data-toggle="popover"]').popover()
										})
										</script>
										@stop