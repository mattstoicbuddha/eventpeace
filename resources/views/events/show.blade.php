<!-- a blank page layout-->

@extends ('layout')

@section ('title')
  Show Event
@stop

@section ('content')
<div class="container">
	<div class="servicesName">
		{{ !empty($s->service->name) ? $s->service->name : '' }}
	</div>
	<div class="servicesPrice">
		${{ !empty($s->service->price) ? (int) $s->service->price/100 : '' }} {{ !empty($s->request->quantity) ? " x " . $s->request->quantity : '' }}
	</div>
		{!! !empty($s->request->status->id) ? request_icon($s->request->status->id) : request_icon(0)  !!}
		<span class="statusText">{{ !empty($s->request->status->text) ? $s->request->status->text : 'New/Untouched' }}</span>


		<div class="row">
			<div class="col-sm-4">
			<img class="img-responsive" src="{{ !empty($s->service->image) ? env('CDN_URL', 'http://cdn.todcandev.com/eventpeace') . '/services/' . $s->service->image : env('CDN_URL', 'http://cdn.todcandev.com/eventpeace') . '/noimage.jpg'  }}">
			</div>
		<div class="col-sm-8">
		<h4>Service Description</h4>
		{{ !empty($s->service->description) ? $s->service->description : '' }}
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-sm-4">
		<h4>Price</h4>
		{{ !empty($s->service->price_details) ? $s->service->price_details : '' }}
		</div>
		<div class="col-sm-4">
		<h4>Delivery Charge</h4>
		{{ !empty($s->service->delivery_charge) ? $s->service->delivery_charge : '' }}
		</div>
		<div class="col-sm-4">
		<h4>Delivery Time</h4>
		{{ !empty($s->service->delivery_time) ? $s->service->delivery_time : '' }}
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="{{ !empty($s->extras) ? 'col-sm-6' : 'col-sm-12' }}" id="termsAndConditions">
		<h4>Terms & Conditions</h4>
		{{ !empty($s->service->terms_conditions) ? $s->service->terms_conditions : '' }}
		</div>
		@if(!empty($s->extras))
		<div class="col-sm-6 well">
		<h4>Extras</h4>


				@foreach($s->extras as $ex)
				 <div id="serviceExtras" class="serviceExtras">{{!empty($ex->name) ? $ex->name : ""}} {{!empty($ex->price) ? $ex->price/100 : ""}} {{!empty($ex->description) ? $ex->description : ""}}</div>
				@endforeach

		</div>
		@endif
	</div>
</div>



@stop