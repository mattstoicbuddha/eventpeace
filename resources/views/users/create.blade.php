<!--this form will register all users with just email address and password.  
The registrant will select "planner" or "vendor" for the account type-->

@extends ('layout')

@section ('title')
  Create Service Form
@stop

@section ('content')

<div class="row">
  <div class="col-sm-6 col-sm-offset-3">
	<div class="panel panel-default">
    	<div class="panel-heading">
           <h3>One Step Registration</h3>
        </div>
        <div class="panel-body">
			<form id="userRegistration" method="POST" action="/users">
			  <div class="form-group">
			    <input name="userEmailAddress" type="text" class="form-control" id="userEmailAddress" placeholder="Email Address">
			  </div>

			  <div class="form-group input-group">
			    <input name="userPassword" type="password" class="form-control" id="userPassword" placeholder="Password">
				<span class="input-group-addon">
                    <span class="glyphicon glyphicon-eye-open showPass"></span>
                </span>
              </div>
      		<input type="hidden" name="_token" value="{{ csrf_token() }}">
			</form>
		
	    <div class="row">
	                 <!-- Controls -->
	        <div class="col-sm-5">
	        <!--submits user as a planner-->
	          <a class="btn btn-success btn-block user-registration-type" href="#" data-usertype="0" >I'm a Planner</a>
	        </div>

	        <div class="col-sm-2"><p class="text-center"> or </p></div>

	        <div class="col-sm-5">
	        <!--submits user as a vendor-->
	          <a class="btn  btn-info btn-block user-registration-type" href="#" data-usertype="1" >I'm a Vendor</a>
	        </div>

	    </div>
	    </div>
    </div>

  </div><!--eof col-sm-6 col-sm-offset-3-->
</div><!--eof row-->

@stop