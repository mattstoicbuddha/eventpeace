<!-- a blank page layout-->

@extends ('layout')

@section ('title')
  New User Welcome
@stop

@section ('content')

<div class="container">
	<div class="well">
		<h1 class="text-center"> Welcome to EventPeace!</h1>
		<h2 class="text-center">Your account has been activated</h2>

		<a class="btn btn-success text-center" href="https://eventpeace.com/users/login">login</a>
	</div>
</div>


@stop