
@extends ('layout')

@section ('title')
  Thanks for Registering
@stop

@section ('content')

<div class="row">
  <div class="col-sm-6 col-sm-offset-3">

		<h2>Please check your email for your activation link</h2>
		<p>You must activate your account before you can login or access your dashboard.</p>

	</div>
</div>
@stop