

@extends ('layout')

@section ('title')
  User Login Page
@stop

@section ('content')

<div class="row">
  <div class="col-sm-4 col-sm-offset-4">
	<div class="panel panel-default">
    <div class="panel-heading">
       <h3>EventPeace Login</h3>
    </div>
    <div class="panel-body">
      @if(!empty($result) && !$result->success || session('noauth'))
        <div class='alert alert-danger'>We were unable to log you in.  Please check your credentials and try again. <a href='#' class='password-reset' data-email='{{session("email")}}'>Reset Password</a></div>
      @endif

      @if(session('nouser'))
        <div class='alert alert-warning'>That user does not exist.  Would you like to <a href='/users/create'>register now</a>?</div>
      @endif

      @if(session('checkemail'))
        <div class='alert alert-info'>Your account is not yet activated.  Please check your email for the activation link.</div>
      @endif
			<form id="userLogin" method="POST" action="">
			  <div class="form-group">
			    <input name="userEmailAddress" type="text" class="form-control" id="userEmailAddress" placeholder="Email Address">
			  </div>

			  <div class="form-group input-group">
			    <input name="userPassword" type="password" class="form-control" id="userPassword" placeholder="Password">
				<span class="input-group-addon">
                    <span class="glyphicon glyphicon-eye-open showPass"></span>
                </span>
              </div>

      		<input type="hidden" name="_token" value="{{ csrf_token() }}">
      		<input class="btn btn-success" value="login" type="submit" name=" ">
			</form>
		</div>
	</div>
  </div>
</div>



@stop