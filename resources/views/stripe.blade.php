<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
  Stripe.setPublishableKey('{{ env('STRIPE_KEY', 'pk_test_oqKRdAiOlwgKQxwFuPJlVEMg') }}');
</script>
<script>
$(function() {
  var $form = $('#payment-form');
  $form.submit(function(event) {
    $form.find('.submit').prop('disabled', true);
    Stripe.card.createToken($form, stripeResponseHandler);
    return false;
  });
});

function stripeResponseHandler(status, response) {
  var $form = $('#payment-form');
  if (response.error) {
    $form.find('.payment-errors').text(response.error.message);
    $form.find('.submit').prop('disabled', false);
  } else {
    var token = response.id;
    $form.append($('<input type="hidden" name="stripe_token">').val(token));
    if(typeof $form.attr('data-submit') !== 'undefined' && $form.attr('data-submit') === 'false'){
      $(".requestServicePayWithThisCard button").prop('disabled', true);
      $.post('/users/addcard', {
        stripe_token: token,
        ajaxPost: true,
        _token: $("meta[name=csrf-token]").attr('content')
      }, function(res){
        var r = JSON.parse(res);
        if(r.success){
          div = "<div class='requestServiceCard'>";
              div = div + "<div class='requestServiceExtraBrand'>"+r.card.brand+"</div>";
              div = div + "<div class='requestServiceExtraLast4'>"+r.card.last4+"</div>";
              div = div + "<div class='requestServicePayWithThisCard newCard' data-id='"+$("#paymentModal").attr('data-id')+"' data-card='"+r.card.id+"'><button>Pay with "+r.card.last4+"</button></div>";
          div = div + "</div>";
          $("#payment-form").find('input').val('');
          $("#payment-form").find('input.submit').val('Submit');
          $(".requestServiceCards").append(div);
          window.setupPayWithCards($("#paymentModal").attr('data-id'));
          $(".requestServicePayWithThisCard.newCard button").trigger('click');
        } else {
          $(".requestServicePayWithThisCard button").prop('disabled', false);
          $form.find('.submit').prop('disabled', false);
          $(".requestServicePayWithThisCard button").prop('disabled', false);
          $(".requestServicePaymentError").css('display', 'none').fadeIn('fast').html(r.error);
        }
      });
    } else {
      $form.get(0).submit();
    }
  }
};


</script>