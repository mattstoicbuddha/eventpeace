<!DOCTYPE html>
<html lang="en">
  <head>
     @include('partials.head')
  </head>

  <header>

      @include ('partials.header')
  </header>

  <body>

    <div class="">
      <div class="contentWrapper">
        @yield ('content')
      </div>
    </div>



    <footer>
          @include('partials.footer')
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

      <script src="{{ url('js/bootstrap.min.js') }}"></script>

  </body>

</html>


