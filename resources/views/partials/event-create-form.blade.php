		 	<!--bof slider form-->
 		<form data-name="createEvent" id="createEvent" action="/events/create" method="POST">
			<div class="row">
			<div class="col-sm-12">
			<h1 class="text-center homeTitle">Start Planning Now</h1>
				<div id="CreateEventFormCarousel" class="carousel slide vertical" data-ride="carousel" data-interval="false">
			    	<div class="carousel-inner" role="listbox">

					    <div class="item active"><!--slide 1-->
							<div class="panel panel-home">
						        <div class="panel-heading">
						            <h3 class="text-center homeSubTitle">What services will you need?</h3>
						        </div>
					         	<div class="panel-body">
									<!--cat select-->
									<div class="form-group">
									  	<label></label>
									    <select name ="EventServiceCat" id="EventServiceCat" class="selectpicker form-control">
								                <option value="" disabled selected>Select a Category</option>
								                @foreach($categories as $c)

								                <option value='{{$c->id}}'>{{$c->name}}</option>

								                @endforeach
								        </select>
									</div>
									<!--cat select-->
						  		</div>
						  		<div class="panel-footer">
									
										<a class="btn btn-ltGreen btn-block btn-outline btn-big" href="#CreateEventFormCarousel" role="button" data-slide="next">Start</a>
							  		
							  	</div>
						  	</div>
					 	</div><!--slide 1-->

					 	<div class="item"><!--slide 2-->
							<div class="panel panel-home">
						        <div class="panel-heading">
						            <h3>Select a Sub-Category</h3>
						        </div>
					         	<div class="panel-body">
									<!--subcat select-->
									<div class="form-group">
									  <label></label>
									     <select name ="EventServiceSubcat" id="EventServiceSubcat" class="selectpicker form-control">
								         <option value="" disabled selected>Sub-Category</option>
								         </select>
									</div>
									<!--subcat select-->

						  		</div>
						  		<div class="panel-footer">
										

										<a class="btn btn-ltGreen btn-block btn-big btn-outline" href="#CreateEventFormCarousel" role="button" data-slide="next">Next</a>
							  		
							  	</div>
						  	</div>
						  	<a class="pull-left" href="#CreateEventFormCarousel" role="button" data-slide="prev">Back</a>
					 	</div><!--slide 2-->

					 	<div class="item"><!--slide 3-->
							<div class="panel panel-home">
						        <div class="panel-heading">
						            <h3>Where is your event?</h3>
						        </div>
					         	<div class="panel-body">
									<!--address-->
									<div class="form-group">
								  		<label>Enter a City & State or Zip</label>
								    	<input name="eventLocation" type="text" class="form-control" id="eventLocation" placeholder="Event Location">
								   	</div>
									<!--address-->
									
						  		</div>
						  		<div class="panel-footer">
										

										<a class="btn btn-ltGreen btn-block btn-big btn-outline" href="#CreateEventFormCarousel" role="button" data-slide="next">Next</a>
							  		
							  	</div>
						  	</div>
						  	<a class="pull-left" href="#CreateEventFormCarousel" role="button" data-slide="prev">Back</a>
					 	</div><!--slide 3-->

					 	<div class="item"><!--slide 4-->
							<div class="panel panel-home">
						        <div class="panel-heading">
						            <h3>How local do you want to be?</h3>
						        </div>
					         	<div class="panel-body">
									<!--distance-->
									<div class="form-group">
										<label>Service Distance</label>
								    	<select name="eventDistance" type="text" class="form-control" id="eventDistanceFront" placeholder="Miles">
								    		<option value="">Select</option>
								    		<option value="10">10 miles</option>
										  	<option value="25">25 miles</option>
										  	<option value="50">50 miles</option>
										  	<option value="100">100 miles</option>
										</select>
									</div>
									<!--distance-->
						  		</div>
								
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
		</form>
						  		<div class="panel-footer row">
									
							  	</div>
						  	</div>
						  	<a class="pull-left" href="#CreateEventFormCarousel" role="button" data-slide="prev">Back</a>
					 	</div><!--slide 4-->
					</div>
				</div>
			</div>
			</div>
		 	<!--eof slider form-->