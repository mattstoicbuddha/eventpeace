
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{{ Session::token() }}}">

<title>@yield ('title')</title>

<!-- Scripts -->

<script>
// When run, will let us know if ECMA 6 is available on the browser or not.
// This way, we know which browsers we want to avoid.
function check() {
    "use strict";

    try { eval("var foo = (x)=>x+1"); }
    catch (e) { return false; }
    return true;
}
if(!check()){
	// We don't want people with bad/old browsers using the site, because
	// they'll run into issues and have various things that don't
	// work for them.  So we tell them to get a better browser.
	if(window.location.href.indexOf('/browsers') < 1){
		alert("Your browser is not supported by EventPeace.  Please upgrade in order to fully experience everything EventPeace has to offer.");
		window.location.href = '/browsers';
	}
}
</script>
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>


<script src="{{ url('js/moment.js') }}"></script>
<script src="{{ url('js/bootstrap-collapse.js') }}"></script>
<!-- <script src="{{ url('js/bootstrap-transition.js') }}"></script> -->
<script src="{{ url('js/bootstrap-datetimepicker.js') }}"></script>
<!-- Latest compiled and minified JavaScript for Jansy Bootstrap -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>

<script src="{{ url('js/main.evp.js') }}"></script>
<script src="{{ url('js/search.evp.js') }}"></script>

<script src="{{ url('js/jquery-tag.js') }}"></script>

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
<!--stylesheets-->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
<!-- Latest compiled and minified CSS for Jansy Bootstrap-->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
<link rel="stylesheet" href="{{ url('css/style.css') }}" >
<link href="{{ url('css/jquery-tag.css') }}" rel="stylesheet">
