		 	<!--bof slider form-->
 			<div class="row">
			<div class="col-sm-12">
			<h1 class="text-center homeTitle">What type of event are you planning?</h1>
				<div class="homeAction">
					<div class="form-group">
						<input class="form-control" id='newEventTag' type='text' placeholder="Type something like 'Birthday' or 'Wedding'..." />
					</div>
					<button class="btn btn-ltGreen btn-block btn-outline btn-big" id='newEventTagButton'>Get Started!</button>
				</div>
			</div>
			</div>

			<script>

				$("#newEventTag").keypress(function(event){
				    var keycode = (event.keyCode ? event.keyCode : event.which);
				    if(keycode == '13'){
				        $("#newEventTagButton").trigger('click');
				    }
				});

			</script>
		