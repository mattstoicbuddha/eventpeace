<div class="well">
	<h3>Your Services</h3>
		@foreach($services as $service)
	<ul class="vertList">
		<li class="verticalList vendorIcon">{{$service->id}}: {{$service->name}} <a class="editThis" href="/services/{{$service->id}}/edit"></a> <a class="viewThis" href="/services/{{$service->id}}/"></a></li>
	</ul>
		@endforeach
</div>