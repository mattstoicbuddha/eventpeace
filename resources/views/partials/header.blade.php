<?php use \App\Vendors; use \App\Planners; use Cartalyst\Sentinel\Native\Facades\Sentinel; ?>
<div class="navWrapper">

  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-special">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button> -->
        <a class="navbar-brand" href="/"><img src=" {{ url(env('CDN_URL', 'http://cdn.todcandev.com/eventpeace') . '/green-logo.png') }} "></a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="mainNav" id="main-navbar-collapse-1">

        <ul class="nav navbar-nav navbar-right">
        <?php
         if(!Sentinel::guest()){
          $logged_user = Sentinel::getUser();
          $planner = Planners::where('user_id', $logged_user->id)->first();
          $vendor = Vendors::where('user_id', $logged_user->id)->first();
         ?>

            <?php if(!empty($planner->id)){ ?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle eventIcon" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Planner Actions <span class="caret"></span></a>
              <ul class="dropdown-menu mainNavDropdown">
                @if( (int) $logged_user->is_admin === 1 )
                <li class='admin text-center'><strong>Admin Options</strong></li>
                <li class="admin"><a href="/admin/users">Manage Users</a></li>
                <li class="admin"><a href="/admin/payments">Manage Payments</a></li>
                <hr>
                <li class='text-center'><strong>Planner Options</strong></li>
                @endif
                <li><a href="/events/dashboard">Event Dasboard</a></li>
                <li><a href="/events/create">Create an Event</a></li>
                <li><a href="/planners/profile">Planner Profile</a></li>
                <li><a href="/users/logout">Logout</a></li>

              </ul>
            </li>
            <?php } ?>
            <?php if(!empty($vendor->id)){ ?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle vendorIcon" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Vendor Actions <span class="caret"></span></a>
              <ul class="dropdown-menu">
                @if( (int) $logged_user->is_admin === 1 )
                <li class='text-center'><strong>Admin Options</strong></li>
                <li><a href="/admin/users">Manage Users</a></li>
                <li><a href="/admin/payments">Manage Payments</a></li>
                <li class='text-center'><strong>Vendor Options</strong></li>
                @endif
                <li><a href="/services/dashboard">Vendor Dashboard</a></li>
                <li><a href="/services/create">List a Service</a></li>
                <li><a href="/vendors/profile">Vendor Profile</a></li>
                <li><a href="/users/logout">Logout</a></li>

              </ul>
            </li>
            <?php } ?>
          <?php } else { ?>
          <li class="dropdown">
              <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">User<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/users/create">Register</a></li>
                <li><a href="/users/login">Login</a></li>
              </ul>
          </li>
          <?php } ?>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>

</div><!--eof navWrapper-->
