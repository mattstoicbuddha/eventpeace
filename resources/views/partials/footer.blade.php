<div class="footerContainer">
	<ul class="footerNav">
		<li><a href="/vendorfaq">Vendor FAQ</a></li>
		<li><a href="/plannerfaq">Planner FAQ</a></li>
		<li><a href="/terms">Terms</a></li>
		<li><a href="/privacy">Privacy</a></li>
	</ul>
	<div class="container">
	    <p class="copyrightFooter">&copy; {{ date('Y') }} EventPeace.com</p>
	</div>
</div>