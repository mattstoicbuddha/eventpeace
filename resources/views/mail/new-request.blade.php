
@extends ('mail-layout')

@section ('title')
  New Request
@stop

@section ('content')

<h3>Hello <strong>{{$vendor->name ?: 'Eventpeace Vendor'}}</strong>,</h3>

<h4>You've received a request from {{ $planner->name ?: "an Eventpeace user." }}.</h4>

<p></p>
<p><strong>To view this request, please <a href="{{env('APP_URL', 'https://eventpeace.com')}}/users/login">Log In</a> or paste the following into your browser's URL field: {{env('APP_URL', 'https://eventpeace.com')}}/users/login</strong></p>

@stop