
@extends ('mail-layout')

@section ('title')
  New Request Note
@stop

@section ('content')

<h3>Hello <strong>{{$request->recipient_name}}</strong>,</h3>

<h4>You've received a note from {{ $request->other_name }} for request ID {{$request->id}} - {{$request->name}}:</h4>

<p></p>
<p><strong>{{$request->note}}</strong></p>

@stop