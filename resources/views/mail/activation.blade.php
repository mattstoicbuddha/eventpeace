
@extends ('mail-layout')

@section ('title')
  Account Activation
@stop

@section ('content')

<h3>Hello <strong>{{$user->email}}</strong>,</h3>

<h4>You've successfully registered an account at EventPeace.  However, your new account requires activation, so we know you're a human.  <a href='{{ url("/") }}/users/activate/{{$user->id}}/{{$user->activation_code}}'>Click here</a> to activate your account or paste the following into your address bar: {{ url("/") }}/users/activate/{{$user->id}}/{{$user->activation_code}}</h4>

@if(!empty($user->the_password))
	<p></p>
	<h4>By the way, we set your password for you.  Your password is: {{$user->the_password}} <br/><br/>
	Please remember to change it after you log in.  You can change it by going to your profile page and clicking "Edit".</h4>
@endif

@stop