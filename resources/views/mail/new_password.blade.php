
@extends ('mail-layout')

@section ('title')
  New Eventpeace Password
@stop

@section ('content')

<h3>Hello <strong>{{$user->email}}</strong>,</h3>

<h4>You've requested a new password from Eventpeace.  Your new password is:</h4>
<h3>{{$password}}</h3>
<h4>Please change your password once you log in.</h4>

@stop