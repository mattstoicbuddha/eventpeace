
@extends ('mail-layout')

@section ('title')
  Your Annual Event
@stop

@section ('content')

<h3>Hello <strong>{{$event->recipient_name}}</strong>,</h3>

<h4>Last year around this time, you created an event called {{$event->name}} and told us it happens every year.  Why don't you <a href='{{ url() }}'>come on back to EventPeace</a> and plan this event with us again?</h4>


@stop