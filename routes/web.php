<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get('/', 'PagesController@index');

Route::get('/admin', 'AdminController@dashboard');

Route::get('/admin/users', 'AdminController@users');

Route::get('/admin/users/suspend/{user}', 'AdminController@suspend_user');

Route::get('/admin/users/unsuspend/{user}', 'AdminController@unsuspend_user');

Route::get('/admin/payments', 'AdminController@payments');

Route::get('/admin/payout/{request_id}', 'AdminController@pay_out');

Route::get('/admin/users/csv', 'AdminController@export_users_csv');

Route::get('/browsers', 'PagesController@browsers');

Route::get('/categories/top', 'CategoriesController@get_top');

Route::get('/categories/category/{category}', 'CategoriesController@get_category');

Route::get('/categories/children/{parent}', 'CategoriesController@get_children');

Route::get('/events/dashboard', 'EventsController@show_dashboard');

Route::post('/events/dashboard', 'EventsController@save_dashboard');

Route::get('/events/dashboard/expired', 'EventsController@show_dashboard');

Route::get('/events/add/{id}', 'EventsController@add_service_to_event');

Route::get('/events/remove/{id}', 'EventsController@remove_service_from_event');

Route::post('/events/save', 'EventsController@save_event');

Route::get('/events/get', 'EventsController@get_services_from_event');

Route::get('/events/get/{id}', 'EventsController@get_services_from_event');

Route::post('/events/create', 'EventsController@create');

Route::get('/events/create/search/{search}', 'EventsController@create');

Route::get('/services/dashboard', 'ServicesController@show_dashboard');

Route::get('/services/dashboard/{expired}', 'ServicesController@show_dashboard');

Route::post('/services/dashboard', 'ServicesController@save_dashboard');

Route::get('/vendors/profile', 'VendorsController@show_profile');

Route::get('/vendors/landing', 'VendorsController@landing');

Route::get('/vendors/tips', 'VendorsController@tips');

Route::get('/terms', 'PagesController@terms');

Route::get('/privacy', 'PagesController@privacy');

Route::get('/vendorfaq', 'PagesController@vendorfaq');

Route::get('/plannerfaq', 'PagesController@plannerfaq');

Route::get('/users/testmail', 'UsersController@test_mail');

Route::get('/users/login', 'UsersController@get_login');

Route::post('/users/login', 'UsersController@post_login');

Route::get('/users/logout', 'UsersController@logout');

Route::get('/users/profile', 'UsersController@show_profile');

Route::get('/users/resetpass/{email}', 'UsersController@reset_password');

Route::get('/users/activate/{id}/{code}', 'UsersController@activate');

Route::post('/users/addcard', 'UsersController@add_card');

Route::post('/users/deletecard', 'UsersController@delete_card');

Route::get('/planners/profile', 'PlannersController@show_profile');

Route::get('/reindex', 'UsersController@reindex');

Route::get('/requests/modify/{id}', 'RequestsController@get_modify');

Route::post('/requests/modify/{id}', 'RequestsController@post_modify');

Route::get('/requests/notes/{id}', 'RequestNotesController@show');

Route::post('/requests/notes/{id}', 'RequestNotesController@store');

Route::get('/requests/create/{id}', 'RequestsController@create');

Route::post('/requests', 'RequestsController@store');

Route::get('/requests/{id}/edit', 'RequestsController@edit');

Route::post('/requests/{id}', 'RequestsController@update');

Route::post('/requests/{id}/destroy', 'RequestsController@destroy');

Route::get('/requests/{id}/payment', 'RequestsController@payment_info');

Route::post('/requests/{id}/payment', 'RequestsController@create_charge');

Route::get('/services/tags/autocomplete', 'ServicesController@tags_autocomplete');

Route::get('/services/extras/{event_id}/{service_id}', 'ServicesController@get_extras');

Route::get('/services/modal/{service_id}', 'ServicesController@show_modal');

Route::get('/services/calendar_source/{service_id}', 'ServicesController@get_calendar_sources');

Route::post('/webhook/update', 'WebhookController@index');

// MUST BE AT THE BOTTOM OF ALL THE ROUTES
Route::resource('vendors', 'VendorsController');
Route::resource('events', 'EventsController');
Route::resource('planners', 'PlannersController');
Route::resource('services', 'ServicesController');
Route::resource('users', 'UsersController');