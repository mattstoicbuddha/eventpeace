// This class exists to deal with the various
// search functions necessary for this site.
// Algolia is kind of a bitch to deal with
// if you don't encapsulate things.
class evpSearch {

	constructor(ind) {
		this.client = algoliasearch('BPOM9ORZSO', '43c369d1a0ae389679b9170ee4bb0434');
		this.index = this.client.initIndex(ind);
		this.results = [];
		this.hiddenFields = [];
		this.hiddenFieldsTR = [];
		this.tableID = Math.random().toString(36).substring(10);
		window.classObj = this;
		this.cdn = () => {
			if(window.location.href.indexOf('eventpeace.com') > -1){
				return 'https://storage.googleapis.com/eventpeace/eventpeace/';
			} else {
				return 'https://storage.googleapis.com/eventpeace/eventpeace/';
			}
		};
	}

	search(searchable, cat, subcat, attr, hidden, table, location, distance) {
		// var results = this.results;
		var classObj = this;
		var subcategory = "";
		var category = "";
		var cdn = this.cdn();
		// var alltags;
		// var alltagsstring = "";

		// Disable just in case they get uncommented.
		location = undefined;
		distance = undefined;
		var noresults = "<div class='alert alert-warning'><span>No results found for that search.</span> <strong>Try broadening your search by removing some tags or searching only by Category.</strong></div>";
		// TODO: Clean  this up so it doesn't look like a giant blocky mess
		// if (typeof cat === 'undefined' || isNaN(cat) || !cat ) if(table){ $(table).html(noresults); return; }
		// if (typeof subcat === 'undefined' ||  !subcat ) if(table){ $(table).html(noresults); return; };
		if (typeof searchable === 'undefined') {
			console.log("No searchable passed");
			if (table) {
				$(table).html(noresults);
				return;
			}
		}
		if (typeof attr === 'undefined') {
			console.log("No attributes passed");
			if (table) {
				$(table).html(noresults);
				return;
			}
		}
		if (typeof table === 'undefined') table = false;
		if (typeof hidden === 'undefined') hidden = [];
		if (typeof distance === 'undefined' || isNaN(distance)) distance = 2500;
		if (typeof cat !== 'undefined' && cat.length > 0 && cat > 0) {
			category = 'category=' + cat;
		}
		if (typeof subcat !== 'undefined' && subcat.length > 0 && subcat > 0) {
			subcategory = ' AND subcategory=' + subcat;
		}
		// if (typeof tags !== 'undefined' && tags.length > 0) {
		// 	alltagsstring = typeof category !== 'undefined' && category.length > 0 ? " AND ( " : "( ";
		// 	alltags = tags.split(',');
		// 	for (var i = 0; i < alltags.length; i++) {
		// 		if (i > 0) alltagsstring = alltagsstring + " OR ";
		// 		var tagMutations = " _tags:" + alltags[i] + " OR " + "_tags:" + alltags[i] + "es OR " + " _tags:" + alltags[i] + "s " + " OR " + " _tags:" + alltags[i].slice(0, -1) + " OR  " + " _tags:" + alltags[i].slice(0, -2) + " ";
		// 		alltagsstring = alltagsstring + tagMutations;
		// 	}

		// 	alltagsstring = alltagsstring + " )";

		// }
		var filter = (category + subcategory).length > 0 ? category + subcategory + ' AND is_custom=0' : 'is_custom=0';
		if ((category + subcategory + searchable).length < 1) {
			$(table).html("<div class='alert alert-warning'><span>You didn't put in anything to search.  Please select a category or enter a search word (i.e. birthday, wedding) and try again.</strong></div>");
			return;
		}

		this.returnFields = attr;
		this.hideFields = hidden;
		var searchAttr = {
			attributesToRetrieve: attr,
			hitsPerPage: 50,
			filters: filter,
		};

		if (location && typeof location !== 'undefined') {
			var locationResults;
			var settings = {
				"async": false,
				"url": "https://maps.googleapis.com/maps/api/geocode/json?address=" + location + "&key=AIzaSyASfFNUH52i82R6uReEsn4qF6RbRXx-oVQ",
				"method": "GET",
			};

			$.ajax(settings).done(function(response) {
				if (response.status === "ZERO_RESULTS") return;
				locationResults = response.results[0];
				searchAttr.aroundLatLng = locationResults.geometry.location.lat + ", " + locationResults.geometry.location.lng;
				searchAttr.aroundRadius = distance.length > 0 && distance > 0 ? classObj.getMeters(distance) : 2500;
			});
		}


		this.index.search(searchable, searchAttr, function(err, r) {
			if (err) {
				throw err;
			}

			if (r.hits.length > 0) {
				classObj.results = r.hits;
			}

			if (table) {
				$(table).html(classObj.buildTable());
				$("#" + classObj.tableID).ready(function() {
					window.processedAttrs = [];
					window.all_tags = [];
					for (var i = 0; i < classObj.hiddenFieldsTR.length; i++) {
						var div = $(".evpTableRow").eq(i);
						$(div).prepend('<div class="dismissThis" onclick="$(this).parent().fadeOut(\'slow\');"></div>');
						var infoAttrs = "<div class='attr-description-container'></div><div class='attr-price-container'></div>";

						for (var name in classObj.hiddenFieldsTR[i]) {
							if (name === '_tags') {
								if (typeof classObj.hiddenFieldsTR[i][name] === 'undefined' || classObj.hiddenFieldsTR[i][name] === null || classObj.hiddenFieldsTR[i][name].length < 1) continue;
								for (var tct = 0; tct < classObj.hiddenFieldsTR[i][name].length; tct++) {
									window.all_tags.push(classObj.hiddenFieldsTR[i][name][tct]);
								};
								var tagsnow = [];
								$.each(window.all_tags, function(i, el) {
									if ($.inArray(el, tagsnow) === -1) tagsnow.push(el);
								});
								window.all_tags = tagsnow;
								continue;
							}
							$(div).attr('data-' + name, classObj.hiddenFieldsTR[i][name]);

							/*** Make Uppercase ***/
							var label = name;
							if (label.indexOf("_") > -1) {
								label = label.replace("_", " ");
							}
							var str = label.toLowerCase().split(' ');

							for (var b = 0; b < str.length; b++) {
								str[b] = str[b].split('');
								str[b][0] = str[b][0].toUpperCase();
								str[b] = str[b].join('');
							}
							label = str.join(' ');
							/*** /Uppercase ***/

							if (name === 'id') {
								continue;
							}

							infoAttrs = infoAttrs + "<div id='attr-" + name + "-container' class='infoAttrsContainer'>" + "<div id='attr-" + name + "-label' class='infoAttrsLabel'>" + label + "</div>" + "<div id='attr-" + name + "-value' class='infoAttrsValue'>" + classObj.hiddenFieldsTR[i][name] + "</div></div>";
						}

						window.processedAttrs[$(div).data('id')] = infoAttrs;

						$(div).find('.moreInfobutton').on('click', function() {

							var id = $(this).parent().parent().data('id');

							// var name = $(this).parent().parent().find('#attr-name').length ? $(this).parent().parent().find('#attr-name').html() : $(this).parent().parent().find('#attr-title').length ? $(this).parent().parent().find('#attr-name').html() : '';

							// var price = $(this).parent().parent().find('#attr-price').length ? $(this).parent().parent().find('#attr-price').html() : '';

							// var description = $(this).parent().parent().find('#attr-description').length ? $(this).parent().parent().find('#attr-description').html() : '';

							// $("#searchModal").find('.modal-title').html("");

							// $("#searchModal").find('.modal-body').html(window.processedAttrs[id]);

							// if(price.length > 0){
							//     $('.attr-price-container').html("<div id='attr--price--label' class='infoAttrsLabel'>Price</div>" + "<div id='attr-price-value' class='infoAttrsValue'>" +price+"</div>");
							// }

							// if(description.length > 0){
							//     $('.attr-description-container').html("<div id='attr-description-label' class='infoAttrsLabel'>Description</div>" + "<div class='infoAttrsValue' id='attr-description-value'>" +description+"</div>");
							// }

							$("#searchModal").find('.modal-body').html("<div id='searchModalLoadingMessage' class='text-center'><h3>Loading...</h3><div id='searchModalLoadingImage'><img class='img-responsive center-block' src='"+cdn+"gears.gif' /></div></div>");
							$('#searchModal').modal('show');

							$.get('/services/modal/' + id, function(r) {

								$("#searchModal").find('.modal-body').html(r);
								if ($(".add-to-event-button").length > 0) $(".add-to-event-button").remove();
								$("#searchModal").find('.modal-add-to-event-button').html("<button class='btn btn-success add-to-event-button'>Add To Event</div>");
								$(".add-to-event-button").on('click', function() {

									$("#searchModal").modal('hide');
									classObj.addToEvent(id);

								});

							})

						});


					}


				});
				var table_created = new Event('table_created');

				// Dispatch the event.
				window.dispatchEvent(table_created);
			}

		});



	}

	getColumns() {
		if (!this.results || this.results.length < 1) {
			return [];
		}
		var columns = [];
		for (var propertyName in this.results[0]) {
			if (this.hideFields.indexOf(propertyName) > -1 || propertyName === 'objectID' || propertyName === '_highlightResult') continue;
			columns.push(propertyName);
		}
		columns = this.getSorted(columns, this.returnFields);
		return columns;
	}

	getSorted(obj, sortArr) {
		if (obj.constructor === Array) {
			var objlen = obj;
			var result = [];
		} else {
			var objlen = Object.keys(obj);
			var result = {};
		}

		if (objlen.length < sortArr.length) {
			objlen = sortArr;
		}
		this.hiddenFields = [];
		for (var i = 0; i < objlen.length; i++) {
			if (typeof sortArr[i] === 'undefined') continue;
			if (this.hideFields.indexOf(sortArr[i]) > -1 && this.hiddenFields.indexOf(sortArr[i]) < 0 && typeof obj[sortArr[i]] !== 'undefined') {
				this.hiddenFields[sortArr[i]] = obj[sortArr[i]];
				continue;
			}
			if (obj.constructor === Array) {
				result[sortArr[i]] = sortArr[i];
				continue;
			}
			result[sortArr[i]] = obj[sortArr[i]];
		}
		return result;
	}

	getMeters(i) {
		return Math.ceil(i * 1609.344);
	}

	buildTable() {
		if (!this.results || this.results.length < 1) {
			return "<div class='alert alert-warning'><span>No results found for that search.</span>  <strong>Try broadening your search by removing some tags or searching only by Category.</strong></div>";
		}

		var cdn = this.cdn();

		var rowcount = 0;

		var columns = this.getColumns();

		var table = '<div class="evpTable">';

		/**** Build Table Meat ****/
		for (var i = 0; i < this.results.length; i++) {

			var sorted = this.getSorted(this.results[i], this.returnFields);
			table = table + '<div class="evpTableRow col-sm-6 col-md-4 col-lg-3 tableRowBox">';
			this.hiddenFieldsTR[rowcount] = this.hiddenFields;
			rowcount++;
			for (var propertyName in sorted) {
				if (propertyName === '_tags') continue;
				var fortable = sorted[propertyName];
				if (propertyName.toLowerCase().indexOf('charge') > -1 && !isNaN(fortable) || propertyName.toLowerCase().indexOf('price') > -1 && !isNaN(fortable)) {
					fortable = "$" + parseInt(fortable / 100).toFixed(2);
				}
				if (typeof sorted[propertyName] === 'object') {
					continue;
				}
				if (typeof sorted[propertyName] === 'string') {
					try {
						var json = JSON.parse(sorted[propertyName]);
						if (json.constructor === Array) {
							fortable = '';
							if (propertyName.toLowerCase().indexOf('image') > -1) {
								fortable = fortable + 'aaa' + json[a].name + '<br/>';
							} else {
								for (var a = 0; a < json.length; a++) {
									if (typeof json[a].name === 'undefined') continue;
									fortable = fortable + json[a].name + '<br/>';
								}
							}
						} else {
							continue;
						}

					} catch (e) {

						fortable = sorted[propertyName];

						if (propertyName.toLowerCase().indexOf('image') > -1) {
							if (sorted[propertyName].length < 1) {
								fortable = "<img class='img-responsive' src='"+cdn+"noimage.jpg' />";
							} else {
								fortable = "<img class='img-responsive' src='"+cdn+"services/" + sorted[propertyName] + "' />";
							}
						}
					}
				}

				table = table + '<div id="attr-' + propertyName + '" class="evpTableCell">' + fortable + "</div>";
			}
			table = table + '<div class="evpTableCell"><button class="btn btn-sm btn-ltGreen moreInfobutton">Details</button></div>';
			table = table + '</div>';
		}
		/**** /Table Meat ****/

		table = table + '</div>';

		return table;

	}

	addToEvent(id) {
		var classObj = this;
		$.get("/events/add/" + id)
			.done(function(data) {
				try {
					var services = JSON.parse(data);
					var display = classObj.displayEventServices(services);
					$("#servicesDisplayBox").html(display);
				} catch (e) {
					console.log("Couldn't parse: ", data);
				}
			});
	}

	removeFromEvent(id) {
		$.get("/events/remove/" + id)
			.done(function(data) {
				try {
					var services = JSON.parse(data);
					var display = classObj.displayEventServices(services);
					$("#servicesDisplayBox").html(display);
				} catch (e) {
					console.log("Couldn't parse: ", data);
				}
			});
	}

	getFromEvent(id) {
		var classObj = this;
		if (typeof id === 'undefined' || id.length < 1) {
			id = ""
		} else {
			id = "/" + id
		};
		$.get("/events/get" + id)
			.done(function(data) {
				try {
					var services = JSON.parse(data);
					var display = classObj.displayEventServices(services);
					$("#servicesDisplayBox").html(display);
				} catch (e) {
					console.log("Couldn't parse: ", e, data);
				}
			});
	}

	displayEventServices(json) {
		var classObj = this;
		var display = "";
		var price = 0;
		for (var i = 0; i < json.length; i++) {
			price = price + parseInt(json[i].price) / 100;
			display = display + "<div class='eventServicesDisplay' data-id='" + json[i].id + "'>";
			display = display + "<div class='eventServicesDisplayName '><span>" + json[i].name + "</span></div>";
			display = display + "<div class='eventServicesDisplayPrice '><span>$" + parseInt(json[i].price) / 100 + "</span></div>";
			if([-1, 5, 11, 12, 13, 14, 15, 16].indexOf(json[i].status) > -1 || !json[i].status){
				display = display + "<div class='eventServicesDisplayRemove '><span class='text-right deleteThis'> </span></div>";
			}
			display = display + "</div>";
		}
		display = display + "<script>setEventServiceRemoveButtons();</script>";
		$("#saveEventTotal").html(price);
		return display;
	}



	saveEvent(id, obj) {
		if ($("#EventName").val().length < 1 && $("#eventNameSelect").length < 1) {
			$("#EventName").addClass('form-error');
			return;
		}
		if ($("#EventStartTime input").val().length < 1) {
			$("#EventStartTime input").addClass('form-error');
			return;
		}
		if ($("#EventEndTime input").val().length < 1) {
			$("#EventEndTime input").addClass('form-error');
			return;
		}
		if ($("#EventStartDate input").val().length < 1) {
			$("#EventStartDate input").addClass('form-error');
			return;
		}
		if ($("#EventEndDate input").val().length < 1) {
			$("#EventEndDate input").addClass('form-error');
			return;
		}
		if ($("#EventEmail").length > 0 && $("#EventEmail").val().length < 1) {
			$("#EventEmail").addClass('form-error');
			return;
		}

		// Remove all event form error classes if we've gotten this far.
		$("#EventName, #EventStartTime input, #EventEndTime input, #EventStartDate input, #EventEndDate input, #EventEmail").removeClass('form-error');

		var time = {};
		time.start_time = $("#EventStartTime input").val();
		time.end_time = $("#EventEndTime input").val();
		time.start_date = $("#EventStartDate input").val();
		time.end_date = $("#EventEndDate input").val();

		var email = $("#EventEmail").length > 0 ? $("#EventEmail").val() : '';

		var classObj = this;

		var annual = $('input[name=annualEvent]').is(':checked') ? 1 : 0;

		$("#eventSearchSubmit").prop('disabled', true);

		$.post("/events/save", {
				id: id,
				email: email,
				time: JSON.stringify(time),
				annual: annual,
				_token: $("meta[name=csrf-token]").attr('content')
			})
			.done(function(data) {
				try {
					var results = JSON.parse(data);
					if (results.success) {
						window.eventSaved = true;
						if (results.email) {
							$("#eventSavedDialogModal").find('.modal-body').html("<h2>Your event has successfully saved!</h2> <h3>Before logging in, you must first activate your account</h3><ul class='modalMessageList'><li>Check your email for the activation link</li> <li>Your temporary password is included in the same email</li><li>Once you login you may change your password in your Profile settings</li></ul><p class='text-center'><strong>You will not be able to login until you do these steps</strong></p>");
							// $("#saveEventTotal, #servicesDisplayBox, #EventName").html("");
							$("#eventSavedDialogModal").modal({
									  show: true,
									  backdrop: 'static',
									  keyboard: false
							});
							return;
						}
						$(obj).prop('disabled', false);
						$("#eventSavedDialogModal").find('.modal-body').html("<h2 class='text-center redTagLine'>(Nothing Has Been Booked Yet)</h2><h3 class='text-center'>You can manage services on your Event Dashboard</h3><ul class='modalMessageList'><li>Schedule Service Dates & Times</li><li>Book & Confirm Services</li><li>Edit or Modify Services</li><li>Message Service Vendors</li><li>Add Service Extras(When Offered)</li><li>View the Status of Each Service</li></ul>");
						// $("#saveEventTotal, #servicesDisplayBox, #EventName").html("");
						$("#eventSavedDialogModal").modal({
								  show: true,
								  backdrop: 'static',
								  keyboard: false
						});


					} else {
						if (results.login) {
							$("#loginModalLoginForm > .userEmailAddress").val(email);
							$("#loginModal").modal('show');
						} else if (results.events) {
							$("#eventSavedDialogModal").find('.modal-body').html("<p>We were unable to find services in this event.  If this is a new event, please make sure you have selected at least one service.  If this is an existing event, please make sure there is at least one service in it.  If you reached this page by hitting the back or forward buttons, please refresh and try again.</p><a class='btn btn-success btn-block btn-lg' href= '#' data-dismiss='modal'>OK</a>");
							$("#eventSearchSubmit").prop('disabled', false);
							$("#eventSavedDialogModal").modal('show');
						} else if (results.save) {
							$("#eventSavedDialogModal").find('.modal-body').html("<p>Your event did not save. Please try again momentarily.</p><a class='btn btn-success btn-block btn-lg' href= '#' data-dismiss='modal'>OK</a>");
							$("#eventSearchSubmit").prop('disabled', false);
							$("#eventSavedDialogModal").modal('show');
						} else {
							$("#eventSavedDialogModal").find('.modal-body').html("<p>An unknown error occured.  Please try again momentarily.</p><a class='btn btn-success btn-block btn-lg' href= '#' data-dismiss='modal'>OK</a>");
							$("#eventSearchSubmit").prop('disabled', false);
							$("#eventSavedDialogModal").modal('show');
						}

					}
				} catch (e) {
					console.log("Save error: ", e);
				}
			});
	}

}