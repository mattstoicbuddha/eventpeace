'use strict';

$(document).ready(function() {


	/****************/

	$('#services-add-extras-button').on('click', function() {

		$('#services-add-extras-box').append('<div class="greenBox form-group services-extras"><div class="row"><div class="col-sm-8"><label>Extra Name</label><input type="text" class="form-control services-extras-name" name="extraname[]" placeholder="Name" value="" required/></div><div class="col-sm-4"><label>Extra Price</label><input type="text" class="form-control services-extras-price" name="extraprice[]" placeholder="Price" value="" required/></div></div><label>Extra Details</label><input type="text" class="form-control services-extras-details" name="extradetails[]" placeholder="Details" value="" required/></div><input type="hidden" name="extrafield[]" value="true" />');

	});

	/****************/

	$(".user-registration-type").on('click', function() {

		if (validateEmail($("#userEmailAddress").val()) === false) {
			if ($("#emailErrorBlock").length > 0) return;
			$("#userEmailAddress").parent().addClass("has-error").append('<span id="emailErrorBlock" class="help-block">Your email is invalid.  Please check it and submit again.</span>');
			return;
		}

		var ud = $(this).data('usertype');

		$("#userRegistration").append("<input type='hidden' name='userType' value='" + ud + "' />");

		$("#userRegistration").submit();

	});

	/****************/

	$(".mainCategoriesSelect").on('change', function() {

		if ($(this).val() !== "none") {
			$(".subCategoriesSelect").prop('disabled', true).html('<option value="">Please wait...</option>');
			$.get('/categories/children/' + $(this).val(), function(r) {
				if (typeof r[0] === "undefined") {
					$(".subCategoriesSelect").prop('disabled', false).html("<option value='-1' selected>None</option>");
					return;
				}
				var children = "<option value='-1' selected>Select Event Sub-Category</option>";
				for (var a = 0; a < r.length; a++) {
					children = children + "<option value='" + r[a].id + "'>" + r[a].name + "</option>";
				}
				$(".subCategoriesSelect").prop('disabled', false).html(children);
			});
		}

	});

	/****************/

	function prepend_loader(elem, form) {
		$(elem).prepend("<div class='loader'></div>").prop('disabled', true);
	}

	$(".loaderOnClick").each(function() {
		$(this).click(function() {
			prepend_loader(this);
			var form = $(this).parent();
			$(form).submit();
		});
	});

	/****************/

	function validateEmail(email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	/****************/

	function sendSearch() {
		var searchObj = new evpSearch('services');

		var searchable = ['id', 'image', 'name', 'description', 'price'];

		var hideable = ['id'];

		searchObj.search($('.searchText').val(), $(".mainCategoriesSelect").val(), $(".subCategoriesSelect").val(), searchable, hideable, $("#eventMatches"), $("#eventLocation").val(), $("#eventDistance").val());
	}

	$('.searchText').keyup(function() {

		sendSearch();

	});

	$(".mainCategoriesSelect, .subCategoriesSelect").change(function() {

		sendSearch();

	});

	$(".eventSearchSubmit").click(function() {

		sendSearch();

	});

	$("#eventDistanceFront").on('change', function() {

		$("#createEvent").submit();

	});

	$("#saveEventButton").on('click', function() {

		var searchObj = typeof window.classObj !== 'undefined' ? window.classObj : new evpSearch('services');

		if ($("#eventNameSelect").length > 0 && $("#eventNameSelect").val() !== 'new') {
			var eventID = $("#eventNameSelect").val();
		} else if ($("#eventNameSelect").length > 0 && $("#eventNameSelect").val() === 'none') {
			$("#eventNameSelect").addClass('form-error');
			return;
		} else {
			var eventID = $("#EventName").val().length > 0 && !isNaN($("#EventName").val()) ? parseInt($("#EventName").val()) : $("#EventName").val();
		}

		searchObj.saveEvent(eventID, $("#saveEventButton"));


	});

	/****************/


	$(window).on('table_created', function() {
		window.tableCreatedTags();
	});

	/****************/


	$(".dashboardPlannerEditRequest").each(function() {

		var btn = this;

		$(this).click(function() {
			var id = parseInt($(this).attr('data-id'));
			var service = parseInt($(this).data('service'));
			var event = parseInt($(this).data('event'));
			var serviceName = $(this).data('servicename');
			var servicePrice = parseInt($(this).data('serviceprice'));
			var requestStartTime = $(this).data('requeststart');
			var requestEndTime = $(this).data('requestend');
			var requestQuantity = JSON.parse($(this).data('requestquantity'));
			var requestQuantityNumber = parseInt($(this).data('requestquantitynumber'));
			var requestAddress = $(this).data('address');
			var requestCity = $(this).data('city');
			var requestState = $(this).data('state');
			var requestZip = $(this).data('zip');
			var priceDiff = $(this).data('pricediff');
			var priceDiffDescription = $(this).data('pricediffdescription');
			var status = $(this).data('status');

			if (status >= 10) {
				$(".dashboardPlannerSaveRequest").css('display', 'none');
			} else {
				$(".dashboardPlannerSaveRequest").css('display', '');
			}
			if (!isNaN(requestStartTime)) {
				var date = timeConverter(requestStartTime);
				var requestStartDate = date.month + "/" + date.date + "/" + date.year;
				var hour = date.hour;
				var ampm;
				if (parseInt(hour) > 12) {
					ampm = "PM";
					hour = hour - 12;
				} else {
					ampm = "AM";
				}
				var requestStartTime = hour + ":" + date.min + " " + ampm;
			}

			if (!isNaN(requestEndTime)) {
				var date = timeConverter(requestEndTime);
				var requestEndDate = date.month + "/" + date.date + "/" + date.year;
				var hour = date.hour;
				var ampm;
				if (parseInt(hour) > 12) {
					ampm = "PM";
					hour = hour - 12;
				} else {
					ampm = "AM";
				}
				var requestEndTime = hour + ":" + date.min + " " + ampm;
			}

			$("#hiddenRequestInputs").remove();
			$("#requestModal").find('form').append("<div id='hiddenRequestInputs'><input type='hidden' name='event_id' value='" + event + "' /><input type='hidden' name='service_id' value='" + service + "' /></div>");

			$("#requestModal").find('form').attr('data-id', id);


			$("#requestModal").find('.modal-title').html(serviceName);
			$("#requestModal").find('.modal-price').html("$" + (servicePrice / 100).toFixed(2));

			$("#serviceStartDate input").val(requestStartDate);
			$("#serviceEndDate input").val(requestEndDate);
			$("#serviceStartTime input").val(requestStartTime);
			$("#serviceEndTime input").val(requestEndTime);
			$(".address-for-service").val(requestAddress);
			$(".city-for-service").val(requestCity);
			$(".state-for-service").val(requestState);
			$(".zip-for-service").val(requestZip);

			if (priceDiff !== 0) {
				$("#requestModal").find('.modal-price-diff').html("Price Modification: " + priceDiff / 100);
				$("#requestModal").find('.modal-price-diff-description').html("Reason: " + priceDiffDescription);

			}

			if (requestQuantity) {
				$("#requestModal").find('.modal-price-type').html("Quantity: <input type='number' name='requestQuantity' value='" + requestQuantityNumber + "' required />");
			}

			$("#serviceRequestExtras").html("<div id='searchModalLoadingMessage' class='text-center'><h3>Loading Extras...</h3><div id='searchModalLoadingImage'><img class='img-responsive center-block' src='http://cdn.todcandev.com/eventpeace/gears.gif' /></div></div>");

			$.get("/services/extras/" + event + "/" + service,
				function(r) {
					var res = JSON.parse(r);
					if (res === 'null' || res === null) {
						$("#serviceRequestExtras").html("<strong>There are no extras for this service.</strong>");
						return;
					}
					var newhtml = "";
					if (res.length < 1) newhtml = "<strong>There are no extras for this service.</strong>";
					for (var i = 0; i < res.length; i++) {
						newhtml = newhtml + "<div class='serviceRequestExtra' data-id='" + res[i].id + "'>";
						newhtml = newhtml + "<div class='serviceRequestExtraCheckbox '><input type='checkbox' value='" + res[i].id + "' name='extras[]' " + (typeof res[i].selected !== 'undefined' && res[i].selected ? "checked" : "") + " /></div>";
						newhtml = newhtml + "<div class='serviceRequestExtraName '><p>" + res[i].name + "</p></div>";
						newhtml = newhtml + "<div class='serviceRequestExtraPrice '><p>$" + parseInt(res[i].price) / 100 + "</p></div>";
						newhtml = newhtml + "<div class='serviceRequestExtraDescription '><p>" + res[i].description + "</p></div>";
						newhtml = newhtml + "</div>";
					}
					//lets keep this for future option
					//newhtml = newhtml + "<div class='serviceRequestAddCustom'><button type='button' class='serviceRequestAddCustomButton'>Request Custom Extra</button></div>";

					$("#serviceRequestExtras").html(newhtml);

					$(".serviceRequestAddCustomButton").click(function() {
						var newhtml = "";

						newhtml = newhtml + "<div class='serviceRequestExtra custom'>";
						newhtml = newhtml + "<div class='serviceRequestExtraCheckbox'><input type='checkbox' value='0' name='extras[]' checked /></div>";
						newhtml = newhtml + "<div class='serviceRequestExtraName'><input type='text' name='requestCustomExtrasName[]' />name</div>";
						newhtml = newhtml + "<div class='serviceRequestExtraPrice'>$<input type='text' name='requestCustomExtrasPrice[]' /></div>";
						newhtml = newhtml + "<div class='serviceRequestExtraDescription'><input type='text' name='requestCustomExtrasDescription[]' /></div>";
						newhtml = newhtml + "</div>";
						$(".serviceRequestAddCustom").before(newhtml);
					});
				});
			$("#requestModal").attr('data-id', id);
			$("#requestModal").modal('show');
			var req = id !== 0 && !isNaN(id) ? "/requests/notes/" + id : "/requests/notes/0";
			$("#serviceNotes").html("<div id='searchModalLoadingMessage' class='text-center'><h3>Loading Notes...</h3><div id='searchModalLoadingImage'><img class='img-responsive center-block' src='http://cdn.todcandev.com/eventpeace/gears.gif' /></div></div>");
			$.get(req,
				function(r) {
					var res = JSON.parse(r);
					if (res === 'null' || res === null) return;
					var newhtml = "";

					// Making sure we put the form for a new note before the other notes
					newhtml = newhtml + "<div class='serviceRequestExtraNoteNew'>";
					newhtml = newhtml + "<div class='serviceRequestExtraNoteText'><div id='requestNoteNewError'></div><textarea class='form-control' id='requestNoteNew'></textarea></div>";
					newhtml = newhtml + "<div class='serviceRequestAddNote'><button type='button' class='serviceRequestAddCustomNote btn btn-primary'>Add note</button></div>";
					newhtml = newhtml + "</div>";
					// End Form
					for (var i = 0; i < res.length; i++) {
						newhtml = newhtml + "<div class='serviceRequestExtraNote'>";
						newhtml = newhtml + "<div class='serviceRequestNoteAuthor' data-id='" + res[i].author.id + "'>" + res[i].author.name + "</div>";
						newhtml = newhtml + "<div class='serviceRequestExtraNoteText'><p>" + res[i].note + "</p></div>";
						newhtml = newhtml + "</div>";
					}

					$("#serviceNotes").html(newhtml);

					$(".serviceRequestAddCustomNote").click(function() {

						if ($('#requestNoteNew').val().length < 1) return;

						$(".serviceRequestAddCustomNote").prepend("<div class='loader'></div>").prop('disabled', true);

						if (id !== 0 && !isNaN(id)) {

							$.post("/requests/notes/" + id, {
								id: id,
								note: $('#requestNoteNew').val(),
								_token: $("meta[name=csrf-token]").attr('content')
							}, function(r) {

								var res = JSON.parse(r);

								if (res.success) {
									$(".serviceRequestAddCustomNote").prop('disabled', false);
									$(".serviceRequestAddCustomNote").find('.loader').remove();
									display_new_request_note(id);
									return;
								}

								$('#requestNoteNew').addClass('form-error');
								$('#requestNoteNewError').html('We could not save your note.');
								$(".serviceRequestAddCustomNote").prop('disabled', false);
								$(".serviceRequestAddCustomNote").find('.loader').remove();

							});


						} else {

							$(".serviceRequestAddCustomNote").prop('disabled', false);
							display_new_request_note(id);

						}

					});
				});
		});
	});

	/****************/

	function display_new_request_note(id) {
		if (id === 0 || isNaN(id)) {
			$("#requestEvent").append("<textarea style='display:none;' name='note[]'>" + $('#requestNoteNew').val() + "</textarea>");
		}
		$(".serviceRequestExtraNoteNew").after("<div class='serviceRequestExtraNote' style='display:none;'><div class='serviceRequestNoteAuthor'>You</div><div class='serviceRequestExtraNoteText'><p>" + $('#requestNoteNew').val() + "</p></div>");
		$('#requestNoteNew').val("");
		$("#serviceNotes .serviceRequestExtraNote").eq(0).fadeIn('slow');
		$(".serviceRequestAddCustomNote").prop('disabled', false);
		$(".serviceRequestAddCustomNote").find(".loader").remove();
	}

	/****************/

	$(".serviceRequestNotesAddButton").each(function() {
		$(this).on('click', function() {

			var button = $(this);

			var id = $(this).attr('data-id');

			$(button).prepend("<div class='loader'></div>").prop('disabled', true);

			$(button).parent().find('.serviceRequestNotesAdd').prop('disabled', true);

			$.post("/requests/notes/" + id, {
				id: id,
				note: $('.serviceRequestNotesAdd[data-id="' + id + '"]').val(),
				_token: $("meta[name=csrf-token]").attr('content')
			}, function(r) {

				var res = JSON.parse(r);

				if (res.success) {
					$(button).prop('disabled', false);
					$(button).find('.loader').remove();
					$(button).parent().find('.serviceRequestNotesAdd').prop('disabled', false);
					$($(button).parent().find('.requestNotes')).prepend('<div class="well"><div><strong>You:</strong></div><div>' + $('.serviceRequestNotesAdd[data-id="' + id + '"]').val() + '</div></div>');
					$('.serviceRequestNotesAdd[data-id="' + id + '"]').val("");
					return;
				} else {
					var message;
					if (res.guest) message = " You aren't logged in.";
					if (res.noreq) message = " The request ID wasn't set.";
					if (res.idcheck) message = " You don't have permission to save a note.";
					if (res.save) message = " The save failed.";

				}

				$('.note-error[data-id="' + id + '"]').remove();
				$('.serviceRequestNotesAdd[data-id="' + id + '"]').before('<div class="form-error note-error" data-id="' + id + '">We could not save your note: ' + message + '</div>');
				$(button).prop('disabled', false);

				$(button).parent().find('.serviceRequestNotesAdd').prop('disabled', false);

			});

		});

	});

	/****************/


	$(".dashboardPlannerSaveRequest").each(function() {

		$(this).click(function() {
			if ($("#serviceStartTime input").val().length < 1) {
				$("#serviceStartTime input").addClass('form-error');
				return;
			}
			if ($("#serviceEndTime input").val().length < 1) {
				$("#serviceEndTime input").addClass('form-error');
				return;
			}
			if ($("#serviceStartDate input").val().length < 1) {
				$("#serviceStartDate input").addClass('form-error');
				return;
			}
			if ($("#serviceEndDate input").val().length < 1) {
				$("#serviceEndDate input").addClass('form-error');
				return;
			}
			if ($('#requestNoteNew').val().length > 0) {
				var r = confirm("You added a note but didn't save it.  Press OK to ignore the note, Cancel to go back and save it.");
				if (!r) {
					return;
				}
				$('#requestNoteNew').val("");
			};
			if ($('.address-for-service').val().length < 1) {
				$('.address-for-service').addClass('form-error');
				return;
			}

			var pass = true;

			$(".serviceRequestExtra.custom").each(function() {

				if ($(this).find(".serviceRequestExtraCheckbox input").is(':checked')) {

					if ($(this).find(".serviceRequestExtraName input").val().length < 1) {
						pass = false;
						$(this).find(".serviceRequestExtraName input").addClass('form-error');
					}
					if ($(this).find(".serviceRequestExtraPrice input").val().length < 1) {
						pass = false;
						$(this).find(".serviceRequestExtraPrice input").addClass('form-error');
					}
					if ($(this).find(".serviceRequestExtraDescription input").val().length < 1) {
						pass = false;
						$(this).find(".serviceRequestExtraDescription input").addClass('form-error');
					}
				}

			});


			if (!pass) {
				return;
			}

			$(".dashboardPlannerSaveRequest").prepend("<div class='loader'></div>").prop('disabled', true);

			$(".serviceRequestExtraName input").removeClass('form-error');
			$(".serviceRequestExtraPrice input").removeClass('form-error');
			$(".serviceRequestExtraDescription input").removeClass('form-error');

			var id = parseInt($("#requestModal").find('form').attr('data-id'));
			var service = parseInt($(this).data('service'));
			var event = parseInt($(this).data('event'));
			var formdata = $("#requestModal").find('form').serialize();

			formdata = formdata + "&startDate=" + encodeURIComponent($("#serviceStartDate input").val());
			formdata = formdata + "&endDate=" + encodeURIComponent($("#serviceEndDate input").val());
			formdata = formdata + "&startTime=" + encodeURIComponent($("#serviceStartTime input").val());
			formdata = formdata + "&endTime=" + encodeURIComponent($("#serviceEndTime input").val());

			var req = id === 0 || isNaN(id) ? "/requests" : "/requests/" + id;

			$.post(req, {
					formdata: formdata,
					_token: $("meta[name=csrf-token]").attr('content')
				},
				function(r) {
					console.log(r);
					var res = JSON.parse(r);
					if (res.success) {
						$("#requestEvent").fadeOut('slow', function() {
							$(".dashboardPlannerSaveRequestStatus").removeClass('text-danger').addClass('text-success').html("Request saved! Your dashboard will refresh shortly.");
							setTimeout(function() {
								$("#requestModal").modal('hide');
								location.reload();
								$(".dashboardPlannerSaveRequestStatus").html("").removeClass('text-success');
								$(".dashboardPlannerSaveRequest").prop('disabled', false);
								$(".dashboardPlannerSaveRequest").find('.loader').remove();
							}, 3000);
						});
					} else {
						$(".dashboardPlannerSaveRequestStatus").addClass('text-danger').html("Your request could not be saved at this time.");
						$(".dashboardPlannerSaveRequest").prop('disabled', false);
						$(".dashboardPlannerSaveRequest").find('.loader').remove();
					}
				});

		});

	});

	/****************/


	$(".dashboardPlannerEditRequestCancel").each(function() {

		$(this).click(function() {

			var id = parseInt($(this).attr('data-id'));

			$(this).prop('disabled', true);

			var formdata = "status=" + $(this).attr('data-status');

			$.post('/requests/' + id, {
					formdata: formdata,
					_token: $("meta[name=csrf-token]").attr('content')
				},
				function(r) {
					var res = JSON.parse(r);
					if (res.success) {
						alert("Request Cancelled!");
						location.reload();
					} else {
						alert("We could not cancel your request at this time.");
					}
				});
		});
	});


	/****************/

	$('.deleteExtra').each(function() {

		var btn = this;

		$(btn).click(function() {

			if (parseInt($(btn).attr('data-id')) === 0) {
				$(btn).parent().fadeOut('slow', function() {
					$(btn).parent().remove();
				});
				return;
			}

			$(btn).prepend("<div class='loader'></div>").prop('disabled', true);

			$.get('/extras/' + $(btn).attr('data-id') + '/destroy', function(r) {

				var res = JSON.parse(r);

				if (!res.success) {

					$(btn).prop('disabled', false);
					$(btn).find('.loader').remove();
					alert("There was an error deleting your extra.  Please try again.");
					return;

				}

				$(btn).parent().fadeOut('slow', function() {
					$(btn).parent().remove();
				});

			});
		});

	});



	/****************/

	$("#loginModalLoginButton").on('click', function() {

		var formdata = $("#loginModalLoginForm").serialize();

		$("#loginModalLoginButton").prepend("<div class='loader'></div>").prop('disabled', true);

		$.post("/users/login", {
				formdata: formdata,
				onthefly: "true",
				_token: $("meta[name=csrf-token]").attr('content')
			},
			function(r) {
				var res = JSON.parse(r);
				if (res.success) {

					if (parseInt(res.acct) === 1) {
						$("#loginModalLoginForm .loginModalError").addClass('text-danger').html("You logged in from a vendor account.  You are logged in, but this event cannot be saved.");
						setTimeout(function() {
							$("#loginModal").modal('hide');
						}, 3000);
						return;
					}
					$("#loginModalLoginForm").html('<div class="modalLoginMessage">You have been successfully logged in.</div>');
					setTimeout(function() {
						$("#loginModal").modal('hide');
					}, 1500);
					var searchObj = typeof window.classObj !== 'undefined' ? window.classObj : new evpSearch('services');

					var eventID = $("#EventName").val().length > 0 && !isNaN($("#EventName").val()) ? parseInt($("#EventName").val()) : $("#EventName").val();
					searchObj.saveEvent(eventID, $("#saveEventButton"));

				} else {
					$("#loginModalLoginButton").prop('disabled', false);
					$("#loginModalLoginButton").find('.loader').remove();

					if (res.checkmail) {
						$("#loginModalLoginForm .loginModalError").addClass('text-danger').html("You need to activate your account.  An activation email has been resent to the email address on your account.");
						return;
					} else if (res.nouser) {
						$("#loginModalLoginForm .loginModalError").addClass('text-danger').html("The user you're trying to log in with does not exist.  Please click the button below to register.");
						return;
					} else if (res.noauth) {
						$("#loginModalLoginForm input").addClass('form-error');
						$("#loginModalLoginForm .loginModalError").addClass('text-danger').html("We could not log you in.  Please check your credentials.");
						return;
					} else {
						$("#loginModalLoginForm input").addClass('form-error');
						$("#loginModalLoginForm .loginModalError").addClass('text-danger').html("We could not log you in.  Please check your credentials.");
					}
				}

			});

	});

	$("#loginModalRegisterButton").click(function() {

		var formdata = $("#loginModalRegisterForm").serialize();

		$.post("/users/store", {
				formdata: formdata,
				onthefly: "true",
				_token: $("meta[name=csrf-token]").attr('content')
			},
			function(r) {
				var res = JSON.parse(r);
				if (res.success) {
					$("#loginModalRegisterForm").html('<div class="modalLoginMessage">You have been successfully registered.  Your event has been saved, but you need to follow the link in your email and log in to continue.</div>');
				} else {
					$("#loginModalRegisterForm input").addClass('form-error');
					$("#loginModalRegisterForm .loginModalError").html("We could not register you.  Please check your credentials.");
				}

			});
	});

	/****************/

	$("#eventNameSelect").on('change', function() {

		console.log("change", $(this).val());
		if ($(this).val() === 'new') {
			$("#EventName").removeClass('hide');
		} else {
			$("#EventName").addClass('hide');
		}
		if ($(this).val() !== 'none' && $(this).val() !== 'new') {
			var searchObj = typeof window.classObj !== 'undefined' ? window.classObj : new evpSearch('services');
			searchObj.getFromEvent($(this).val());
		}

	});

	/****************/

	$(".vendorRequestUpdate").each(function() {
		$(this).click(function() {
			var btn = this;
			var id = $(btn).attr('data-id');
			prepend_loader(btn);
			$.get('/requests/modify/' + id,
				function(r) {
					var res = JSON.parse(r);
					if (res.success) {
						if (res.price_diff !== 0) $(".modal-price-diff").val(res.price_diff);
						if (res.price_diff_description.length > 0) $(".modal-price-diff-description").val(res.price_diff_description);
						$(btn).find(".loader").remove();
						$(btn).prop('disabled', false);
						$("#priceDiffModal").attr("data-id", id);
						$(".modal-price-diff-message").removeClass('alert alert-success text-center').html("");
						$("#priceDiffModal").modal('show');
					} else {
						alert("We could not process your request at this time.");
						$(btn).find(".loader").remove();
						$(btn).prop('disabled', false);
					}
				});
		});
	});

	/****************/

	$(".priceDiffSave").click(function() {
		var mods = {};
		var id = $("#priceDiffModal").attr("data-id");
		mods.price_diff = $(".modal-price-diff").val();
		mods.price_diff_description = $(".modal-price-diff-description").val();
		if (typeof mods.price_diff === 'undefined' || Number(mods.price_diff) === 0 || mods.price_diff.length < 1) {
			$(".modal-price-diff").addClass('form-error');
			return;
		}
		$(".modal-price-diff").removeClass('form-error');
		if (typeof mods.price_diff_description === 'undefined' || Number(mods.price_diff) !== 0 && mods.price_diff_description.length < 1) {
			$(".modal-price-diff-description").addClass('form-error');
			return;
		}
		$(".modal-price-diff-description").removeClass('form-error');
		var btn = this;
		$(btn).prepend("<div class='loader'></div>").prop('disabled', true);
		$.post('/requests/modify/' + id, {
			price_diff: mods.price_diff,
			price_diff_description: mods.price_diff_description,
			_token: $("meta[name=csrf-token]").attr('content')
		}, function(r) {
			var res = JSON.parse(r);
			if (res.success) {
				$(btn).find(".loader").remove();
				$(btn).prop('disabled', false);
				$(".modal-price-diff-message").addClass('alert alert-success text-center').html("Modification saved successfully! Refreshing...");
				setTimeout(function() {
					location.reload();
				}, 2000);
			} else {
				alert("We could not process your request at this time.");
				$(btn).find(".loader").remove();
				$(btn).prop('disabled', false);
			}
		});
	});

	/****************/

	$(".showPass").click(function() {
		if ($("#userPassword").attr('type') === 'password') {
			$("#userPassword").attr('type', 'text');
			$(".showPass").removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');
		} else {
			$("#userPassword").attr('type', 'password');
			$(".showPass").removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
		}
	});

	/****************/

	$("#newEventTagButton").click(function() {

		if (typeof $("#newEventTag").val() === 'undefined' || $("#newEventTag").val().length < 1) return;

		$(this).prop('disabled', true);

		var tag = $("#newEventTag").val().toLowerCase();

		window.location.href = "/events/create/search/" + formatTags(tag);

	});


	/****************/

	window.formatTags = function(tag, isCreate) {

		if (typeof tag === 'undefined' || tag.length < 1) return;

		tag = tag.toLowerCase();

		// if(tag.indexOf('a ') > -1){
		//     tag = tag.replace('a ', '');
		// }

		if (tag.indexOf(',') > -1) {
			var tags = tag.split(',');
			tag = "";

			for (var i = 0; i < tags.length; i++) {

				if (tags[i].trim() == 'bar mitzvah' || tags[i].trim() == 'bat mitzvah') {

					tag = tag + (i === tags.length - 1 ? tags[i].trim() : tags[i].trim() + ",");
					continue;
				}

				if (tags[i].trim().indexOf(' ') > -1) {
					var newtags = tags[i].trim().split(' ');
					for (var a = 0; a < newtags.length; a++) {
						tag = tag + newtags[a] + ",";
					}
					continue;
				}

				tag = tag + (i === tags.length - 1 ? tags[i].trim() : tags[i].trim() + ",");

			}
		} else {
			if (tag.trim().indexOf(' ') > -1) {
				var newtags = tag.trim().split(' ');
				tag = "";
				for (var a = 0; a < newtags.length; a++) {
					tag = tag + (a === newtags.length - 1 ? newtags[a].trim() : newtags[a].trim() + ",");
				}
			}
		}

		if (isCreate) {
			$(window.st).importTags(tag);
			console.log(tag);
			return;
		}

		return tag;
	}

	/****************/

	$(".mainCategories").each(function() {
		$(this).click(function() {
			var currentid = $(".mainCategories.selected").length > 0 ? parseInt($(".mainCategories.selected").attr('data-id')) : 0;
			var id = parseInt($(this).attr('data-id'));
			$(".mainCategories").removeClass("selected");
			$("#subsMenuTitle").addClass('hidden');
			$("#subsMenu").html("");
			if (id === currentid || id < 1) return;
			$(this).addClass("selected");
			console.log($("#subs" + id).html());
			$("#subsMenu").html($("#subs" + id).html());
			if ($("#subsMenu").find('li').length > 0) $("#subsMenuTitle").removeClass('hidden');
			$(".subCategories").each(function() {
				$(this).click(function() {
					var currentid = $(".subCategories.selected").length > 0 ? parseInt($(".subCategories.selected").attr('data-id')) : 0;
					var id = parseInt($(this).attr('data-id'));
					$(".subCategories").removeClass("selected");
					if (id === currentid || id < 1) return;
					$(this).addClass("selected");
				});
			});
		});
	});

	/****************/

	if ($('.dashboardPlannerPayRequest').length > 0) {

		$('.dashboardPlannerPayRequest').each(function() {

			$(this).click(function() {

				$("#paymentModal").find('#paymentInfo').html("<div id='searchModalLoadingMessage' class='text-center'><h3>Loading Payment Info...</h3><div id='searchModalLoadingImage'><img class='img-responsive center-block' src='https://storage.googleapis.com/eventpeace/eventpeace/gears.gif' /></div></div>");
				$("#paymentModal").modal('show');

				var id = $(this).attr('data-id');

				$("#paymentModal").attr('data-id', id);

				$.get('/requests/' + id + '/payment', function(res) {

					var r = JSON.parse(res);

					var div = "<div class='paymentSummary'><div class='requestServiceName'>" + r.service_name + "</div>";
					div = div + "<div class='requestServicePrice'>Price: $" + r.price / 100 + " <strong>x " + r.quantity + "</strong></div>";
					div = div + "<div class='requestServiceDeliveryCharge'>Delivery Charge: $" + r.delivery_charge / 100 + "</div>";
					for (var a = 0; a < r.extras.length; a++) {
						div = div + "<div class='requestServiceExtra clearfix'>";
						div = div + "<div class='requestServiceExtraName'>Extra: " + r.extras[a].name + "</div>";
						div = div + "<div class='requestServiceExtraPrice'>Extra Price: $" + r.extras[a].price / 100 + "</div>";
						div = div + "</div>";
					}
					if (r.price_diff > 0) {
						div = div + "<div class='requestServicePriceModification'>Original Price Modification: $" + Math.ceil((r.price_diff / 100)) + "</div>";
						div = div + "<div class='requestServicePriceModificationDescription'>Modification Description: " + r.price_diff_description + "</div>";
					}
					div = div + "<div class='requestServiceProcessingFee bold'>Processing Fee: $" + Math.ceil((r.total / 100) * 0.03) + "</div>";
					div = div + "<div class='requestServiceTotal'>Total: $" + ((r.total / 100) + Math.ceil((r.total / 100) * 0.03)) + "</div></div>";

					if (typeof r.cards.data !== 'undefined') {

						div = div + "<div class='requestServiceCards'>";
						for (a = 0; a < r.cards.data.length; a++) {
							div = div + "<div class='requestServiceCard'>";
							div = div + "<div class='requestServiceExtraBrand'>" + r.cards.data[a].brand + "</div>";
							div = div + "<div class='requestServiceExtraLast4'>" + r.cards.data[a].last4 + "</div>";
							div = div + "<div class='requestServicePayWithThisCard' data-id='" + id + "' data-card='" + r.cards.data[a].id + "'><button>Pay with " + r.cards.data[a].last4 + "</button></div>";
							div = div + "</div>";
						}
						div = div + "</div>";
					}

					$("#paymentModal").find('#paymentInfo').html(div);
					$("#payment-form").css('display', '');
					window.setupPayWithCards(id);

				});

			});

		});

	}

	/****************/

	$(".payoutPending").each(function() {
		$(this).on('click', function() {
			var button = this;
			var id = $(this).attr('data-id');
			if (!id || id.length < 1) return;
			$(this).attr('disabled', true);
			$.get('/admin/payout/' + id, function(res) {
				var r = JSON.parse(res);
				if (r.success) {
					$(button).parent().fadeOut('slow');
				} else {
					// TODO: Create an error box for this failure
					$(this).attr('disabled', false);
				}
			});
		});
	});

	/****************/

	$(".suspendUser").each(function() {
		var button = this;
		var id = $(this).attr('data-id');
		if (id.length < 1) return;
		$(this).click(function() {
			$(this).attr('disabled', true);
			$.get('/admin/users/suspend/' + id, function(res) {
				var r = JSON.parse(res);
				if (r.success) {
					$(button).after('<div class="userSuspendedSuccess">User Suspended</div>');
					$(button).remove();
				} else {
					alert("This user could not be suspended at this time.");
					$(this).attr('disabled', false);
				}
			});
		});
	});

	/****************/

	$(".unsuspendUser").each(function() {
		var button = this;
		var id = $(this).attr('data-id');
		if (id.length < 1) return;
		$(this).click(function() {
			$(this).attr('disabled', true);
			$.get('/admin/users/unsuspend/' + id, function(res) {
				var r = JSON.parse(res);
				if (r.success) {
					$(button).after('<div class="userUnsuspendedSuccess">User Unsuspended</div>');
					$(button).remove();
				} else {
					alert("This user could not be suspended at this time.");
					$(this).attr('disabled', false);
				}
			});
		});
	});

	/****************/

	$(".password-reset").click(function() {
		var email = $(this).attr('data-email');
		$(this).attr('disabled', true);
		$.get('/users/resetpass/' + email, function(res) {
			var r = JSON.parse(res);
			if (r.success) {
				alert("Your password has been reset.  Please check your email.");
			} else {
				alert("We could not reset your password at this time.");
				$(this).attr('disabled', false);
			}
		});
	});

	/****************/

	// To prevent popups from sticking around
	jQuery(function() {
	    var isVisible = false;
	    var hideAllPopovers = function() {
	       $('.popup-marker').each(function() {
	            $(this).popover('hide');
	        });
	    };
	    $('.popup-marker').popover({
	        html: true,
	        trigger: 'manual'
	    }).on('click', function(e) {
	        // if any other popovers are visible, hide them
	        if(isVisible) {
	            hideAllPopovers();
	        }
	        $(this).popover('show');
	        // handle clicking on the popover itself
	        $('.popover').off('click').on('click', function(e) {
	            e.stopPropagation(); // prevent event for bubbling up => will not get caught with document.onclick
	        });
	        isVisible = true;
	        e.stopPropagation();
	    });
	    $(document).on('click', function(e) {
	        hideAllPopovers();
	        isVisible = false;
	    });
	});
	$('#servicePic,#profilePic').bind('change', function() {
      if(this.files[0].size > 1024000){
      	alert("This image is too big(" + this.files[0].size/1000 + "MB).  Please choose a different image that is 1MB or less in size.");
      	$('#servicePic,#profilePic').val('');
      }
    });

});

/****************/

window.setupPayWithCards = function(id) {
	$(".requestServicePayWithThisCard button").each(function() {
		$(this).off("click");
		$(this).click(function() {
			$(".requestServicePayWithThisCard button").prepend("<div class='loader'></div>").prop('disabled', true);
			var card = $(this).parent().attr('data-card');
			var card_html = $(this).html();
			$(this).html("Processing...");
			var button = $(this);
			$.post('/requests/' + id + '/payment', {
					card_id: card,
					_token: $("meta[name=csrf-token]").attr('content')
				},
				function(res) {
					var r = JSON.parse(res);
					if (r.success) {
						$("#paymentModal").find('.modal-body').html("Your payment was successfully processed!");
						setTimeout(function() {
							window.location.reload();
						}, 1000);
					} else {
						$(button).html(card_html);
						$(".requestServicePayWithThisCard button").prop('disabled', false);
						$(".requestServicePayWithThisCard button").find('.loader').remove();

						$(".requestServicePaymentError").css('display', 'none').fadeIn('fast').html(r.error);
					}
				}).fail(function() {
				$(button).html(card_html);
				$(".requestServicePayWithThisCard button").prop('disabled', false);
				$(".requestServicePayWithThisCard button").find('.loader').remove();
				$(".requestServicePaymentError").css('display', 'none').fadeIn('fast').html("There was a problem processing your payment.  Please use another card or try again.");
			});

		});

	});
};

/****************/

function setEventServiceRemoveButtons() {
	$('.eventServicesDisplayRemove').each(function() {
		$(this).click(function() {
			window.classObj.removeFromEvent($(this).parent().attr('data-id'));
		});

	});
}

/****************/

function timeConverter(UNIX_timestamp) {
	var a = new Date(UNIX_timestamp * 1000);
	var time = {};
	time.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	time.year = a.getFullYear();
	time.month = a.getMonth() + 1;
	time.date = a.getDate();
	time.hour = a.getHours() < 10 ? '0' + a.getHours() : a.getHours();
	time.min = a.getMinutes() < 10 ? '0' + a.getMinutes() : a.getMinutes();
	time.sec = a.getSeconds() < 10 ? '0' + a.getSeconds() : a.getSeconds();
	return time;
}

/****************/

if(window.location.href.indexOf("events/create") > -1 || window.location.href.indexOf("events") > -1 && window.location.href.indexOf("/edit") > -1){
	window.addEventListener("beforeunload", function (e) {
	  if(window.eventSaved) return;
	  var confirmationMessage = "\o/";

	  e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
	  return confirmationMessage;              // Gecko, WebKit, Chrome <34
	});
}

/****************/

// Adds tags to the search area in events/create
(function($) {
	window.tableCreatedTags = function() {
		if (typeof window.all_tags !== 'undefined' && window.all_tags.length > 0) {
			var tags = "";
			var length = window.all_tags.length;
			if (window.all_tags.length > 10) length = 10;
			for (var i = 0; i < length; i++) {
				tags = tags + "<div class='tags tags-table btn btn-warning' style='margin-right: 10px' data-tag='" + window.all_tags[i] + "'>" + window.all_tags[i] + "</div>";
			}
			$("#tagHolder").html(tags);
			$(".tags.tags-table").each(function() {
				$(this).click(function() {
					var tags = $('#serviceTags').val().split(',');
					if (tags.indexOf($(this).attr('data-tag')) > -1) return;
					$('#serviceTags').addTag($(this).attr('data-tag'));
				});
			});
		}
	}

})(jQuery);